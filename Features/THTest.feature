Feature: CI/CC/PL Submit data

  @test-this
  Scenario: Car Insurance Run Submission
    Given Open landing page
    When Click on CI CTA button
    And Click on Get a free quote button on subsequent page
    And Verify the funnel Page appears
    And Enter your car's details with click the dropdown: "Toyota", "Camry", "2015" and "Sedan Camry G 6AT 2.0"
    And Enter your specify the nature of the car used by select "ส่วนบุคคล" as Car usage characteristic question and "1" as The type of car insurance you need
    And Click on To roceed button
    And Verify PI form appearance
    And Enter Personal Information with First Name "TestCi" and Surame "CITesting" and Email "testci@gmail.com" and Phone Number "(008) 654-321" and LineIDorRangeIncome "tesci"
    And Check I Agree with Terms and conditions and privacy policy
    And Click Submit button on PI then wait until PI form disappear
    And Click Apply Now button on any data in the given list
    Then the application success page will be displayed
#    And Close browser


  Scenario: Submission of Car Credit
    Given Open landing page
    When Click on CC CTA button
    And Verify the Credit Card Page appears
    And click Apply Now on any data in the given list
    And Verify PI form appearance
    And Enter Personal Information with First Name "TestCC" and Surame "CCTesting" and Email "testcc@gmail.com" and Phone Number "(008) 654-321" and LineIDorRangeIncome "22000"
    And Check I Agree with Terms and conditions and privacy policy
    And Click Submit button on PI then wait until PI form disappear
    Then verify redirect site
#    And Close browser


  Scenario: Create Personal Loans
    Given Open landing page
    When Click on PL CTA button
    And Click on Personal Loans Comparison button on subsequent page
    And Verify the Personal Loans funnel page appears
    And select "พนักงานบริษัทเอกชน" as career question with click the dropdown
    And select "มากกว่า 2 ปี" as Working age question
    And fill "20000" in Loan amount needed
    And select "12 เดือน" as Loan borrowing period with click the dropdown
    And fill "22000" in Monthly income field
    And Click on To Proceed button then wait for it to change to the Loan Result page
    And click Register Now on any data in the given list
    And Verify PI form appearance
    And Enter Personal Information with First Name "TestPL" and Surame "PLTesting" and Email "testpl@gmail.com" and Phone Number "(008) 654-321" and LineIDorRangeIncome "tespl"
    And Check I agree with Terms and conditions and privacy policy
    And Click Submit button on PI then wait until PI form disappear
    Then verify loan redirect site
#    And Close browser