Feature: PH General Regression Test

  #----------Non-Trailing slash url----------#
  @ignore-this
  Scenario Outline: Check the Non-Trailing Slash moneymax url
    Given Open the browser
    When visit "<trailing-url>" on the address bar
    Then Verify the url will display "<non-trailing-url>" on the address bar of the destination page
    Examples:
      | trailing-url                             | non-trailing-url                        |
      | https://moneymax.dev.cag.codes/          | https://moneymax.dev.cag.codes          |
      | https://moneymax.dev.cag.codes/articles/ | https://moneymax.dev.cag.codes/articles |

#  Scenario: Test
#    Given My test
#    When Do the test
#    Then Verify the test result