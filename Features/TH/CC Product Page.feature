Feature: moneyguru.co.th CC Product Page

  Scenario: Ensure user can load Credit Card Product Page Augmented
    Given Open moneyguru Credit Card page "https://www.moneyguru.co.th/credit-card/all"
    When Click on any Product Name
    Then Verify the augmented version shown in the url

  Scenario: Ensure user can load Credit Card Product Page Standalone
    Given Open Credit Card Partner page "https://www.moneyguru.co.th/credit-card/partners/citibank/"
    When Click on any Product Image
    Then Verify the standalone version shown in the url

  Scenario Outline: Ensure Apply button works on Product page
    Given Open the Credit Card Product page "<url>"
    When Click on Apply CTA
    Then Verify the Personal Information Form is displayed
    Examples:
      | url                                                                                         |
      | https://www.moneyguru.co.th/credit-card/product/citibank/citi-cash-back/?version=standalone |
      | https://www.moneyguru.co.th/credit-card/product/citibank/citi-cash-back/?version=augmented  |

  Scenario Outline: Ensure Apply button on the sticky bar works
    Given Open the Credit Card Product page "<url>"
    When Scroll down until sticky bar appears
    And Click on Apply CTA
    Then Verify the Personal Information Form is displayed
    Examples:
      | url                                                                                         |
      | https://www.moneyguru.co.th/credit-card/product/citibank/citi-cash-back/?version=standalone |
      | https://www.moneyguru.co.th/credit-card/product/citibank/citi-cash-back/?version=augmented  |

  Scenario Outline: Ensure and Verify the details of Special Promotion section displayed correctly
    Given Open the Credit Card Product page "<url>"
    When Scroll down into Special Promotion section
    Then Verify the following special promotion detail:
      | Headings: "Special Promotion" |
      | Title                         |
      | Promotion Banner              |
      | Descriptions                  |
    Examples:
      | url                                                                                                              |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=standalone |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=augmented  |

  Scenario Outline: Verify the Product Details of Credit Card Information is displayed correctly
    Given Open the Credit Card Product page "<url>"
    When Hover pointer into Credit Card Provider logo
    Then Verify The following Product Detail Credit card Information are displayed correctly:
      | Product description title     |
      | Product description paragraph |
      | Provider logo                 |
      | Minimum monthly income        |
      | Interest per year             |
      | Annual fee                    |
    Examples:
      | url                                                                                                              |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=standalone |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=augmented  |

  Scenario Outline: Verify the details on the sticky nav bar is displayed correctly
    Given Open the Credit Card Product page "<url>"
    When Scroll down until sticky bar appears
    Then Verify the Detail content on sticky bar is displayed
    And Verify The following Details sticky content are displayed correctly:
      | Credit Card Information tab Section |
      | Qualifications tab Section          |
      | Fees and expenses tab Section       |
      | FAQs tab Section                    |
      | Card Logo                           |
      | Apply Now CTA                       |
    Examples:
      | url                                                                                                              |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=standalone |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=augmented  |

  Scenario Outline: Verify the details of Credit Card Information Section
    Given Open the Credit Card Product page "<url>"
    When Scroll down until sticky bar appears
    And Click Credit Card Information tab
    Then Verify should be navigated to Credit Card Information section
    And Verify Credit Card Information as section titles is displayed
    And Verify The following Details Credit Card Information :
      | Information Title       |
      | Information Description |
    Examples:
      | url                                                                                                              |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=standalone |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=augmented  |

  Scenario: Verify the details of Credit Qualifications Section  in standalone version is displayed correctly
    Given Open the Credit Card Product page in standalone version "https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=standalone"
    When Scroll down until sticky bar appears
    And Click Credit Card applicant Qualifications tab
    Then Verify should be navigated to Credit Card applicant Qualifications section
    And Verify Property as section title is displayed
    And Verify The following Details Credit Card applicant Qualifications:
      | Minimum age requirement                      |
      | Minimum age requirement (supplementary card) |
      | Minimum monthly income                       |
      | Landline number                              |

  Scenario Outline: Verify the details of Fees and Expenses Section
    Given Open the Credit Card Product page "<url>"
    When Scroll down until sticky bar appears
    And Click Fees & Expenses tab
    Then Verify should be navigated to Fees & Expenses section
    And Verify Fees & Expenses as section titles is displayed
    And Verify The following Details Fees & Expenses :
      | Entrance fee                                  |
      | Annual fee                                    |
      | Interest (per year)                           |
      | Cash withdrawal fee                           |
      | Minimum payment                               |
      | Annual fee waived When having spending amount |
    Examples:
      | url                                                                                                              |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=standalone |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=augmented  |

  Scenario Outline: Verify the details of FAQ Section is displayed correctly
    Given Open the Credit Card Product page "<url>"
    When Scroll down until sticky bar appears
    And Click Frequently asked questions tab
    Then Verify should be navigated to Frequently asked questions section
    And Verify Frequently asked questions as section titles is displayed
    And Verify Question Title and Answer Description on Frequently asked questions
    Examples:
      | url                                                                                                              |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=standalone |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=augmented  |

  Scenario Outline: Verify the Related Product of Credit Card is displayed correctly
    Given Open the Credit Card Product page "<url>"
    When Scroll into Other credit cards you might be interested in
    Then Verify for more related product option are displayed:
      | Citi Grab Credit Card                        |
      | Citi Mercedes credit card                    |
      | KTC Cashback Platinum MasterCard credit card |
      | UOB Preferred Platinum credit card           |
    And Verify the details on each related product box Product title, Minimum monthly income, Annual Fee
    Examples:
      | url                                                                                                              |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=standalone |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=augmented  |

  Scenario Outline: Verify the Related Product are clickable
    Given Open the Credit Card Product page "<url>"
    When Scroll into Other credit cards you might be interested in
    And Clicks any related product box
    Then Verify it can navigate properly to the related product page when clicking on it
    Examples:
      | url                                                                                                              |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=standalone |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=augmented  |

  Scenario Outline: Subscribe with an invalid email
    Given Open the Credit Card Product page "<url>"
    When Scroll into Follow us section
    And Input email with the invalid email format "test_cc123@com"
    And Hover over the the Free to apply CTA
    Then Verify Please enter a valid email address message is displayed
    Examples:
      | url                                                                                                              |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=standalone |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=augmented  |

  Scenario Outline: Subscribe with a new valid email and existing email
    Given Open the Credit Card Product page "<url>"
    When Scroll into Follow us section
    And Input email with the valid email format "<email>"
    And Hover over the the Free to apply CTA
    And Verify "อีเมล์ถูกต้อง" message is displayed
    And Click on Free to apply CTA
    Then Verify "<verification_message>" message displayed after submit
    Examples:
      | url                                                                                                              | email               | verification_message           |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=standalone | testcc1241@mail.com | ขอบคุณที่สมัครรับข่าวสารกับเรา |
      | https://moneyguru.staging.cag.codes/credit-card/product/citibank/citi-makro-platinum-rewards/?version=augmented  | testcc123@mail.com  | คุณสมัครรับข่าวสารกับเราแล้ว   |
#ขอบคุณที่สมัครรับข่าวสารกับเรา = Thank you for subscribing to our newsletter.
#คุณสมัครรับข่าวสารกับเราแล้ว = You have subscribed to our newsletter.


