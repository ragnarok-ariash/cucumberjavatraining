Feature: CC/PL/TI/HSBC Submit data

  Scenario: CC Result Page - Filter by Bank and Apply SCB - IC Number
    Given Open landing page
    When Click on CC CTA button
    And Verify CC Result Page
    * Select Category "Cashback"
    * Select Filter By Checkbox "Standard Chartered"
    * Click Apply Now button on credit card with name "Standard Chartered JustOne Platinum Mastercard"
    And Verify CC Application Form Step 1
    * Enter CC Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Enter with IC Number "910123333333"
    * Select "Multinational corporation (MNC,Berhad,GLC)" for Employment Type as CC Info
    * Click Button Next on CC Application Form
    And Verify CC Application Form Step 2
    And Select "Yes" for Credit History (> 12 months)
    * Enter with Range Income "1000"
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on CC Submit button
    Then Verify success page

  Scenario: PL Results Page - Filter and Apply Alliance
    Given Open landing page
    When Click on PL CTA button
    And Verify PL Funnel Page
    And Enter with Loan Amount "50000"
    * Select "2 years / 24 months" for Loan Tenure
    * Click Get Result Button
    And Verify PL Result Page
    * Click Filter "Alliance Bank" checkbox
    * Click Apply Now button on loan with name "Alliance Medical Loan - Bariatric Surgery"
    And Verify PL Application Form Step 1
    * Enter PL Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Click Button Next on PL Application Form
    And Verify PL Application Form Step 2
    * Select "Below RM 2,000" for Monthly Base Salary
    * Select "Experienced (> 12 months)" for long have you been working
    * Select "Salaried Employment (MNC/GLC/Berhad)" for Employment Type as PL Info
    * Select "Yes" for do you have an existing Credit Card or Loan
    * Select "None" for Any overdue payment in the past 12 months
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on PL Submit button
    Then Verify Thank you Page PL
#    And Close browser

  Scenario: TI - Filter and Apply Berjaya Sompo - Myself
    Given Open landing page
    When Click on TI CTA button
    And Verify TI Funnel Page
    * Select "Mexico" for Travelling from Malaysia to
    * Select "20/07/2020" for Start Date
    * Select "27/07/2020" for End Date
    * Select "Myself" for This policy is to insure
    * Click Button Lest Go
    And Verify TI Result Page
    * Click TI Filter "Berjaya Sompo Insurance" checkbox
    * Click Apply Now button on TI Data "Travel Plus Elite Area 2"
    And Verify TI Application Form
    * Enter Personal Information with Full Name "test TI" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select I Agree to the Terms & Conditions and Privacy Policy
    * Click Button Go To Provider Page
    Then Verify redirect site
    And Select Tab on application success
    And Verify Thank you Page TI
#    And Close browser

  Scenario: CC Partners Page Submit - HSBC Lead
    Given Open landing page
    When Click on Partner CTA button
    And Verify Partner Page
    And Click Tab "cc" Partner in Partner Page
    And click "credit-card" "HSBC" Logo button
    And Verify "credit-card" "hsbc" Partner Page
    And Click Apply Now button on Partner "CC" with name "HSBC Amanah MPower Visa Platinum Credit Card-i"
    And Verify CC Application Form Step 1
    * Enter CC Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Enter with IC Number "910123333333"
    * Select "Multinational corporation (MNC,Berhad,GLC)" for Employment Type as CC Info
    * Click Button Next on CC Application Form
    And Verify CC Application Form Step 2
    And Select "Yes" for Credit History (> 12 months)
    * Enter with Range Income "1000"
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on CC Submit button
    Then Verify success page
#    And Close browser

  Scenario: PL Partners Page - Apply - CITI - DOB
    Given Open landing page
    When Click on Partner CTA button
    And Verify Partner Page
    And Click Tab "pl" Partner in Partner Page
    And click "personal-loan" "Citibank" Logo button
    And Verify "personal-loan" "citibank" Partner Page
    And Click Apply Now button on Partner "PL" with name "Personal Loan"
    And Verify PL Application Form Step 1
    * Enter PL Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Select "10/08/1991" for Date of Birth
    * Click Button Next on PL Application Form
    And Verify PL Application Form Step 2
    * Select "Below RM 2,000" for Monthly Base Salary
    * Select "Experienced (> 12 months)" for long have you been working
    * Select "Salaried Employment (MNC/GLC/Berhad)" for Employment Type as PL Info
    * Select "Yes" for do you have an existing Credit Card or Loan
    * Select "None" for Any overdue payment in the past 12 months
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on PL Submit button
    Then Verify Thank you Page PL Redirect
#    And Close browser

  Scenario: TI Partners - Filter and Apply AMGeneral - No Redirect
    Given Open landing page
    When Click on Partner CTA button
    And Verify Partner Page
    And Click Tab "ti" Partner in Partner Page
    And click "travel-insurance" "AmGeneral Insurance Berhad" Logo button
    And Verify "travel-insurance" "kurnia" Partner Page
    And Click Here To Apply on Partner TI
    And Verify TI Funnel Page
    * Select "Mexico" for Travelling from Malaysia to
    * Select "20/07/2020" for Start Date
    * Select "27/07/2020" for End Date
    * Select "Myself" for This policy is to insure
    * Click Button Lest Go
    And Verify TI Result Page
    * Click TI Filter "Berjaya Sompo Insurance" checkbox
    * Click Apply Now button on TI Data "Travel Plus Elite Area 2"
    And Verify TI Application Form
    * Enter Personal Information with Full Name "test TI" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select I Agree to the Terms & Conditions and Privacy Policy
    * Click Button Go To Provider Page
    Then Verify redirect site
    And Select Tab on application success
    And Verify Thank you Page TI
#    And Close browser