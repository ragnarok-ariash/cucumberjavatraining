Feature: CC Regression Test

  Scenario: CC Result Page - Filter by Bank and Apply SCB - IC Number
    Given Open landing page
    When Click on CC CTA button
    And Verify CC Result Page
    * Select Category "Cashback"
    * Select Filter By Checkbox "Standard Chartered"
    * Click Apply Now button on credit card with name "Standard Chartered JustOne Platinum Mastercard"
    And Verify CC Application Form Step 1
    * Enter CC Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Enter with IC Number "910123333333"
    * Select "Multinational corporation (MNC,Berhad,GLC)" for Employment Type as CC Info
    * Click Button Next on CC Application Form
    And Verify CC Application Form Step 2
    And Select "Yes" for Credit History (> 12 months)
    * Enter with Range Income "1000"
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on CC Submit button
    Then Verify success page
#    And Close browser

  Scenario: CC Result Page - Filter by Bank and Apply Maybank - Redirect
    Given Open landing page
    When Click on CC CTA button
    And Verify CC Result Page
    * Select Category "Islamic"
    * Select Filter By Checkbox "Maybank"
    * Click Apply Now button on credit card with name "Maybank Islamic PETRONAS Ikhwan Visa Gold Card-i"
    And Verify CC Application Form Step 1
    * Enter CC Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Click Button Next on CC Application Form
    And Verify CC Application Form Step 2
    And Select "Yes" for Credit History (> 12 months)
    * Enter with Range Income "1000"
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on CC Submit button
    Then Verify Thank you Page Redirect
#    And Close browser

  Scenario: CC Partners Page Submit - HSBC Lead
    Given Open landing page
    When Click on Partner CTA button
    And Verify Partner Page
    And Click Tab "cc" Partner in Partner Page
    And click "credit-card" "HSBC" Logo button
    And Verify "credit-card" "hsbc" Partner Page
    And Click Apply Now button on Partner "CC" with name "HSBC Amanah MPower Visa Platinum Credit Card-i"
    And Verify CC Application Form Step 1
    * Enter CC Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Enter with IC Number "910123333333"
    * Select "Multinational corporation (MNC,Berhad,GLC)" for Employment Type as CC Info
    * Click Button Next on CC Application Form
    And Verify CC Application Form Step 2
    And Select "Yes" for Credit History (> 12 months)
    * Enter with Range Income "1000"
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on CC Submit button
    Then Verify success page
#    And Close browser

  Scenario: CC Partners Page Submit - Citibank Lead
    Given Open landing page
    When Click on Partner CTA button
    And Verify Partner Page
    And Click Tab "cc" Partner in Partner Page
    And click "credit-card" "Citibank" Logo button
    And Verify "credit-card" "citibank" Partner Page
    And Click Apply Now button on Partner "CC" with name "Citi PremierMiles Mastercard"
    And Verify CC Application Form Step 1
    * Enter CC Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Click Button Next on CC Application Form
    And Verify CC Application Form Step 2
    And Select "Yes" for Credit History (> 12 months)
    * Enter with Range Income "1000"
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on CC Submit button
    Then Verify Thank you Page Redirect
#    And Close browser

  Scenario: CC Result Page - Apply SCB Liverpool FC Cashback - Click Text
    Given Open landing page
    When Click on CC CTA button
    And Verify CC Result Page
    * Select Category "Cashback"
    * Select Filter By Checkbox "Standard Chartered"
    * Click "text" Product name "Standard Chartered Liverpool FC Cashback"
    * Click Apply Now button on credit card Product Page with name "standard-chartered-liverpool-fc-cashback"
    And Verify CC Application Form Step 1
    * Enter CC Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Enter with IC Number "910123333333"
    * Select "Multinational corporation (MNC,Berhad,GLC)" for Employment Type as CC Info
    * Click Button Next on CC Application Form
    And Verify CC Application Form Step 2
    And Select "Yes" for Credit History (> 12 months)
    * Enter with Range Income "1000"
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on CC Submit button
    Then Verify success page
#    And Close browser

  Scenario: CC Result Page - Apply SCB Liverpool FC Cashback - Click Image
    Given Open landing page
    When Click on CC CTA button
    And Verify CC Result Page
    * Select Category "Cashback"
    * Select Filter By Checkbox "Standard Chartered"
    * Click "logo" Product name "Standard Chartered Liverpool FC Cashback"
    * Click Apply Now button on credit card Product Page with name "standard-chartered-liverpool-fc-cashback"
    And Verify CC Application Form Step 1
    * Enter CC Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Enter with IC Number "910123333333"
    * Select "Multinational corporation (MNC,Berhad,GLC)" for Employment Type as CC Info
    * Click Button Next on CC Application Form
    And Verify CC Application Form Step 2
    And Select "Yes" for Credit History (> 12 months)
    * Enter with Range Income "1000"
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on CC Submit button
    Then Verify success page
#    And Close browser

  Scenario: CC Partners Product Page Submit - Citibank - Click Image
    Given Open landing page
    When Click on Partner CTA button
    And Verify Partner Page
    And Click Tab "cc" Partner in Partner Page
    And click "credit-card" "Citibank" Logo button
    And Click "text" Partner Product name "Citi PremierMiles Mastercard"
    And Click Apply Now button on credit card Product Page with name "citi-premiermiles-card"
    And Verify CC Application Form Step 1
    * Enter CC Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Click Button Next on CC Application Form
    And Verify CC Application Form Step 2
    And Select "Yes" for Credit History (> 12 months)
    * Enter with Range Income "1000"
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on CC Submit button
    Then Verify Thank you Page Redirect
#    And Close browser