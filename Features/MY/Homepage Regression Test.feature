Feature: Homepage Regression Test

  Scenario: CC Navigation bar redirection
    Given Open landing page
    When Hover pointer over "Credit Cards" menu in the navigation bar
    And Click on "All Credit Cards" dropdown option
    Then Verify url "/credit-card/all" page is displayed

  Scenario: PL Navigation bar redirection
    Given Open landing page
    When Hover pointer over "Personal Loans" menu in the navigation bar
    And Click on "All Personal Loans" dropdown option
    Then Verify url "/personal-loan" page is displayed

  Scenario: TI Navigation bar redirection
    Given Open landing page
    When Hover pointer over "Insurance" menu in the navigation bar
    And Click on "Travel Insurance" dropdown option
    Then Verify url "/travel-insurance" page is displayed

  Scenario: Articles Navigation bar redirection
    Given Open landing page
    When Hover pointer over "Articles" menu in the navigation bar
    And Click on "Articles" dropdown option
    Then Verify url "/articles" page is displayed

  Scenario: About Us Navigation bar redirection
    Given Open landing page
    When Click on "About Us" navigation bar menu
    Then Verify url "/about-us" page is displayed

  Scenario: Contact Us Navigation bar
    Given Open landing page
    When Click on "TALK TO US" navigation bar menu
    Then Verify popup TALK TO US page is displayed

    #----------Article Category drop down redirection----------#

  Scenario: CC Article Category drop down redirection
    Given Open landing page
    When Hover pointer over "Articles" menu in the navigation bar
    And Click on "Credit Card" article category drop down option
    Then Verify url "/articles/credit-card" page is displayed

  Scenario: PL Article Category drop down redirection
    Given Open landing page
    When Hover pointer over "Articles" menu in the navigation bar
    And Click on "Personal Loan" article category drop down option
    Then Verify url "/articles/personal-loan" page is displayed

  Scenario: TI Article Category drop down redirection
    Given Open landing page
    When Hover pointer over "Articles" menu in the navigation bar
    And Click on "Travel Insurance" article category drop down option
    Then Verify url "/articles/travel-insurance" page is displayed

  Scenario: Insurance Article Category drop down redirection
    Given Open landing page
    When Hover pointer over "Articles" menu in the navigation bar
    And Click on "Insurance" article category drop down option
    Then Verify url "/articles/insurance" page is displayed

  Scenario: Money Tips Article Category drop down redirection
    Given Open landing page
    When Hover pointer over "Articles" menu in the navigation bar
    And Click on "Money Tips" article category drop down option
    Then Verify url "/articles/money-tips" page is displayed

  Scenario: Debt Management Article Category drop down redirection
    Given Open landing page
    When Hover pointer over "Articles" menu in the navigation bar
    And Click on "Debt Management" article category drop down option
    Then Verify url "/articles/debt-management" page is displayed

  Scenario: Travel & Lifestyle Article Category drop down redirection
    Given Open landing page
    When Hover pointer over "Articles" menu in the navigation bar
    And Click on "Travel & Lifestyle" article category drop down option
    Then Verify url "/articles/travel-lifestyle" page is displayed

     #----------Newletter Subscription----------#
  Scenario: Newsletter subscription
    Given Open landing page
    When Verify newsletter subscription page is displayed
    And Submit a valid email address "imam@nextbase.co"
    Then Newsletter subscription popup successful message is displayed

#  - Privacy
  Scenario: Privacy Policy redirection
    Given Open landing page
    When Scroll into More menu in the Comparehero footer
    And Click on "Privacy Policy"
    Then Verify "Privacy Policy" page is displayed

    #  - Terms
  Scenario: Terms & Conditions redirection
    Given Open landing page
    When Scroll into More menu in the Comparehero footer
    And Click on "Terms and Condition"
    Then Verify "Terms and Condition" page is displayed