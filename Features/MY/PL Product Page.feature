Feature: Comparehero.my PL Product Page

  Scenario: Ensure user can load comparehero Product Page
    Given Open the Personal Loan page
    When Click on Get Result button and open the Personal Loan result page
    And Click on any Product Name
    Then Verify the augmented Product page is displayed
    And Verify the url is "https://www.comparehero.my/personal-loan/product/standard-chartered-cash-one?version=augmented"

  Scenario: Ensure top Apply Now button works
    Given Open the Personal Loan Product page
    When Click on Apply now CTA
    Then Verify the Step 1 for Loan Information Form is displayed

  Scenario: Ensure Apply Now button form bottom Sticky bar works
    Given Open the Personal Loan Product page and scroll down the page
    When Click on Apply now CTA from bottom sticky bar
    Then Verify the Step 1 for Loan Information Form is displayed

  Scenario: Ensure Limited time promotion are displayed
    Given Open the Personal Loan Product page
    When Scroll into Limited time promotion
    Then Verify "Limited time promotion" is displayed

  Scenario: Ensure the Product Details section is displayed correctly
    Given Open the Personal Loan Product page
    When Scroll into Product Details
    Then Verify all sections tab displayed correctly:
      | Features                   |
      | Eligibility                |
      | Fees & Charges             |
      | Document required to apply |

  Scenario: Verify the Features section is displayed correctly
    Given Open the Personal Loan Product page
    When Click on Features section tab
    Then Verify "Features" as section titles is displayed
    And Verify the url is "https://www.comparehero.my/personal-loan/product/standard-chartered-cash-one?version=augmented#features"
    And Verify all details content Features section is displayed
    And Verify the Features table content are displayed correctly:
      | Minimum loan amount          |
      | Maximum loan amount          |
      | Minimum duration of the loan |
      | Maximum duration of the loan |
    And Verify the monthly repayment calculator box is displayed

  Scenario: Verify the Eligibility section is displayed correctly
    Given Open the Personal Loan Product page
    When Click on Eligibility section tab
    Then Verify "Eligibility" as section titles is displayed
    And Verify the url is "https://www.comparehero.my/personal-loan/product/standard-chartered-cash-one?version=augmented#eligibility"
    And Verify the Eligibility table content are displayed correctly:
      | Minimum Age            |
      | Maximum Age            |
      | Minimum Monthly Income |
      | Employment Type        |

  Scenario: Verify the Fees & Charges section is displayed correctly
    Given Open the Personal Loan Product page
    When Click on Fees & Charges section tab
    Then Verify "Fees & Charges" as section titles is displayed
    And Verify the url is "https://www.comparehero.my/personal-loan/product/standard-chartered-cash-one?version=augmented#fee"
    And Verify Late payment fee on the details of Fees & Charges

  Scenario: Verify the Document required to apply section is displayed correctly
    Given Open the Personal Loan Product page
    When Click on Document required to apply section tab
    Then Verify "Document required to apply" as section titles is displayed
    And Verify the url is "https://www.comparehero.my/personal-loan/product/standard-chartered-cash-one?version=augmented#doc-required"
    And Verify Document required to apply section details are displayed and contains the following:
      | Salaried Employees |
      | Self-employed      |

  Scenario: Verify the Loan repayment table is displayed correctly
    Given Open the Personal Loan Product page
    When Scroll into Loan repayment table
    Then Verify Monthly repayment are shown in the table from 12 to 84 months
    And Verify Loan Amount are shown in the table from 5000 to 250000 RM

  Scenario: Verify the Related Product is displayed correctly
    Given Visits the product page url "https://www.comparehero.my/personal-loan/product/citibank"
    When Scroll into Looking for more options?
    Then Verify for more related product option are displayed:
      | SCB CashOne                         |
      | JCL Personal Loan                   |
      | Hongleong Bank Personal Financing-i |
    
  Scenario: Verify the Related Product are clickable
    Given Open the Personal Loan Product page
    When Scroll into Looking for more options?
    And Clicks any related product box
    Then Verify it can navigate properly to the related product page when clicking on it

  Scenario: Subscribe with an invalid email
    Given Open the Personal Loan Product page
    When Scroll into newsletter section
    And Input email with the invalid email format "test123@co"
    Then Verify "Please input a valid email address" message is displayed

  Scenario: Subscribe with a new valid email
    Given Open the Personal Loan Product page
    When Scroll into newsletter section
    And Input email with the valid email format "test123+15@mail.com"
    And Verify "Email is valid" message is displayed
    And Click on Subscribe button
    Then Verify "Thank you" message displayed after submit

  Scenario: Subscribe with an existing email
    Given Open the Personal Loan Product page
    When Scroll into newsletter section
    And Input email with the existing email format "test123@mail.com"
    And Verify "Email is valid" message is displayed
    And Click on Subscribe button
    Then Verify "You're already subscribed" message displayed after submit

  Scenario: Ensure user can load comparehero Product Page via partner page
    Given Open comparehero Partner page "https://www.comparehero.my/partners"
    When Scroll into Personal loan provider
    And Click on CITI product on Personal Loan Providers
    And Verify redirect to the CITI Product Page detail
    And Click on any product Image from the list
    Then Verify the CITI product page augmented version is displayed
    And Verify the url is "https://www.comparehero.my/personal-loan/product/citibank?version=augmented"

  Scenario: Ensure user can load comparehero Product Page Standalone
    Given Visits the product page url "https://www.comparehero.my/personal-loan/product/citibank"
    When Scroll down the page
    Then Verify the Personal Loan Product page is displayed with title Personal Loan and CITI logo on the right side page



