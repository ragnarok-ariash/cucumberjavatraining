Feature: PL Regression Test

Scenario: PL Results Page - Filter and Apply Alliance
    Given Open landing page
    When Click on PL CTA button
    And Verify PL Funnel Page
    And Enter with Loan Amount "50000"
    * Select "2 years / 24 months" for Loan Tenure
    * Click Get Result Button
    And Verify PL Result Page
    * Click Filter "Alliance Bank" checkbox
    * Click Apply Now button on loan with name "Medical Loan - Bariatric Surgery"
    And Verify PL Application Form Step 1
    * Enter PL Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Click Button Next on PL Application Form
    And Verify PL Application Form Step 2
    * Select "Below RM 2,000" for Monthly Base Salary
    * Select "Experienced (> 12 months)" for long have you been working
    * Select "Salaried Employment (MNC/GLC/Berhad)" for Employment Type as PL Info
    * Select "Yes" for do you have an existing Credit Card or Loan
    * Select "None" for Any overdue payment in the past 12 months
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on PL Submit button
    Then Verify Thank you Page PL
#    And Close browser

  Scenario: PL Results Page - Filter and Apply SCB
    Given Open landing page
    When Click on PL CTA button
    And Verify PL Funnel Page
    And Enter with Loan Amount "50000"
    * Select "2 years / 24 months" for Loan Tenure
    * Click Get Result Button
    And Verify PL Result Page
    * Click Filter "Standard Chartered" checkbox
    * Click Apply Now button on loan with name "Standard Chartered Quick Cash EDGE™"
    And Verify PL Application Form Step 1
    * Enter PL Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Click Button Next on PL Application Form
    And Verify PL Application Form Step 2
    * Select "Below RM 2,000" for Monthly Base Salary
    * Select "Experienced (> 12 months)" for long have you been working
    * Select "Salaried Employment (MNC/GLC/Berhad)" for Employment Type as PL Info
    * Select "Yes" for do you have an existing Credit Card or Loan
    * Select "None" for Any overdue payment in the past 12 months
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on PL Submit button
    Then Verify Thank you Page PL
#    And Close browser

  Scenario: PL Partners Page - Apply - CITI - DOB
    Given Open landing page
    When Click on Partner CTA button
    And Verify Partner Page
    And Click Tab "pl" Partner in Partner Page
    And click "personal-loan" "Citibank" Logo button
    And Verify "personal-loan" "citibank" Partner Page
    And Click Apply Now button on Partner "PL" with name "Personal Loan"
    And Verify PL Application Form Step 1
    * Enter PL Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Select "10/08/1991" for Date of Birth
    * Click Button Next on PL Application Form
    And Verify PL Application Form Step 2
    * Select "Below RM 2,000" for Monthly Base Salary
    * Select "Experienced (> 12 months)" for long have you been working
    * Select "Salaried Employment (MNC/GLC/Berhad)" for Employment Type as PL Info
    * Select "Yes" for do you have an existing Credit Card or Loan
    * Select "None" for Any overdue payment in the past 12 months
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on PL Submit button
    Then Verify Thank you Page Redirect
#    And Close browser

  Scenario: PL Partners Page - Apply - BM - HLB
    Given Open landing page
    When Click on Partner CTA button
    And Verify Partner Page
    And Click Tab "pl" Partner in Partner Page
    And click "personal-loan" "Hong Leong Bank" Logo button
    And Verify "personal-loan" "hong-leong" Partner Page
    And Click Apply Now button on Partner "PL" with name "Personal Financing-i"
    And Verify PL Application Form Step 1
    * Enter PL Personal Information with Full Name "Test" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select "Malaysian" for actived Nationality
    * Select "Kuala Lumpur" for State of Residence
    * Click Button Next on PL Application Form
    And Verify PL Application Form Step 2
    * Select "Below RM 2,000" for Monthly Base Salary
    * Select "Experienced (> 12 months)" for long have you been working
    * Select "Salaried Employment (MNC/GLC/Berhad)" for Employment Type as PL Info
    * Select "Yes" for do you have an existing Credit Card or Loan
    * Select "None" for Any overdue payment in the past 12 months
    * Select I agree to the Terms & Conditions and Privacy Policy
    * Click on PL Submit button
    Then Verify Thank you Page PL
#    And Close browser

