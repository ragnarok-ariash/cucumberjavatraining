Feature: TI Regression Test

  Scenario: TI - Filter and Apply Berjaya Sompo - Myself
    Given Open landing page
    When Click on TI CTA button
    And Verify TI Funnel Page
    * Select "Mexico" for Travelling from Malaysia to
    * Select "20/08/2020" for Start Date
    * Select "27/08/2020" for End Date
    * Select "Myself" for This policy is to insure
    * Click Button Lest Go
    And Verify TI Result Page
    * Click TI Filter "Berjaya Sompo Insurance" checkbox
    * Click Apply Now button on TI Data "Travel Plus Elite Area 2"
    And Verify TI Application Form
    * Enter Personal Information with Full Name "test TI" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select I Agree to the Terms & Conditions and Privacy Policy
    * Click Button Go To Provider Page
    Then Verify redirect site
    And Select Tab on application success
    And Verify Thank you Page TI
#    And Close browser

  Scenario: TI - Apply Allianz
    Given Open landing page
    When Click on TI CTA button
    And Verify TI Funnel Page
    * Select "Mexico" for Travelling from Malaysia to
    * Select "20/08/2020" for Start Date
    * Select "27/08/2020" for End Date
    * Select "Myself" for This policy is to insure
    * Click Button Lest Go
    And Verify TI Result Page
    * Click TI Filter "Allianz General Insurance" checkbox
    * Click Apply Now button on TI Data "Allianz TravelCare - Worldwide"
    And Verify TI Application Form
    * Enter Personal Information with Full Name "test TI" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select I Agree to the Terms & Conditions and Privacy Policy
    * Click Button Go To Provider Page
    Then Verify redirect site
    And Select Tab on application success
    And Verify Thank you Page TI
#    And Close browser

  Scenario: TI Partners - Filter and Apply AMGeneral - No Redirect
    Given Open landing page
    When Click on Partner CTA button
    And Verify Partner Page
    And Click Tab "ti" Partner in Partner Page
    And click "travel-insurance" "AmGeneral Insurance Berhad" Logo button
    And Verify "travel-insurance" "kurnia" Partner Page
    And Click Here To Apply on Partner TI
    And Verify TI Funnel Page
    * Select "Mexico" for Travelling from Malaysia to
    * Select "20/08/2020" for Start Date
    * Select "27/08/2020" for End Date
    * Select "Myself" for This policy is to insure
    * Click Button Lest Go
    And Verify TI Result Page
    * Click TI Filter "Berjaya Sompo Insurance" checkbox
    * Click Apply Now button on TI Data "Travel Plus Elite Area 2"
    And Verify TI Application Form
    * Enter Personal Information with Full Name "test TI" and Phone Number "0123333333" and Email "test@gmail.com"
    * Select I Agree to the Terms & Conditions and Privacy Policy
    * Click Button Go To Provider Page
    Then Verify redirect site
    And Select Tab on application success
    And Verify Thank you Page TI
#    And Close browser

  Scenario: TI Filter - Plans By Providers On TI Result Page
    Given Open landing page
    When Click on TI CTA button
    And Verify TI Funnel Page
    * Select "Mexico" for Travelling from Malaysia to
    * Select "20/08/2020" for Start Date
    * Select "27/08/2020" for End Date
    * Select "Myself" for This policy is to insure
    * Click Button Lest Go
    And Verify TI Result Page
    * Click TI Filter "Berjaya Sompo Insurance" checkbox
    * Verify on TI Data "Travel Plus Elite Area 2"
    * Click TI Filter "AXA Affin Insurance Malaysia" checkbox
    * Verify on TI Data "AXA SmartTraveller VIP - Area 2"
#    And Close browser

  Scenario: TI Filter - TI Sorting
    Given Open landing page
    When Click on TI CTA button
    And Verify TI Funnel Page
    * Select "Mexico" for Travelling from Malaysia to
    * Select "20/08/2020" for Start Date
    * Select "27/08/2020" for End Date
    * Select "Myself" for This policy is to insure
    * Click Button Lest Go
    And Verify TI Result Page
    * Select "Cheapest Price" for Sort By
    * Verify on TI Data "Sompo Travel Lite - Area 2"
    * Select "Highest Medical & Other Expenses Amount" for Sort By
    * Verify on TI Data "Chubb First - Zone 2"
    * Select "Highest Lost Baggage & Belongings Amount" for Sort By
    * Verify on TI Data "AXA SmartTraveller VIP - Area 2"
    * Select "Highest Trip Cancellation Amount" for Sort By
    * Verify on TI Data "Chubb First - Zone 2"
    * Select "Highest Flight Delay Amount" for Sort By
    * Verify on TI Data "Chubb First - Zone 2"
    * Select "Highest Personal Accident Amount" for Sort By
    * Verify on TI Data "Travel Plus Elite Area 2"
#    And Close browser