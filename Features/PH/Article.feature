Feature: Article

  #----------Article Category drop down redirection----------#
  Scenario: CI Article Category drop down redirection
    Given Open landing page
    When Hover pointer over articles menu in the navigation bar
    And Click on Car Insurance article category drop down option
    And Verify Car Insurance article category page is displayed

  Scenario: CC Article Category drop down redirection
    Given Open landing page
    And Hover pointer over articles menu in the navigation bar
    And Click on Credit Card article category drop down option
    Then Verify Credit Card article category page is displayed

  Scenario: PL Article
    Given Open landing page
    When Hover pointer over articles menu in the navigation bar
    And Click on Personal Loan article category drop down option
    Then Verify Personal Loan article category page is displayed

  Scenario: GDP Article
    Given Open landing page
    When Hover pointer over articles menu in the navigation bar
    And Click on Gadget Protect article category drop down option
    Then Verify Gadget Protect article category page is displayed

  Scenario: Personal Finance Article
    Given Open landing page
    When Hover pointer over articles menu in the navigation bar
    And Click on Personal Finance article category drop down option
    Then Verify Personal Finance article category page is displayed

  Scenario: Lifestyle Article
    Given Open landing page
    When Hover pointer over articles menu in the navigation bar
    And Click on Lifestyle article category drop down option
    Then Verify Lifestyle article category page is displayed

  Scenario: Government Services Article
    Given Open landing page
    When Hover pointer over articles menu in the navigation bar
    And Click on Government Services article category drop down option
    Then Verify Government Services article category page is displayed

  #----------Article Page----------#
  Scenario: Article details - Article Image
    Given Open landing page
    When Hover pointer over articles menu in the navigation bar
    And Click on All Articles dropdown option
    And Verify All Articles page is displayed
    And Click on any articles image form the article list
    Then Verify article details page is displayed

  Scenario: Article details - Article Title
    Given Open landing page
    When Hover pointer over articles menu in the navigation bar
    And Click on All Articles dropdown option
    And Verify All Articles page is displayed
    And Click on any articles title form the article list
    Then Verify article details page is displayed