Feature: PH CC Regression Test

  Scenario: CC Lead Submission until redirected to Partner Site
    Given Open landing page
    When Click on CC CTA button
    And Click on Compare Credit Cards button on CC landing page
    And Verify CC Funnel Page
    And Select "Yes, over 1 year" for Do you have an active credit card question
    And Select "Employed in Private Sector" for What is your employment status question
    And Select "SSS ID" for Which of these documents could you provide question
    And Select "More than 3 years" for How long have you been in your current job question
    And Enter "50000" for What is your monthly income question
    And Click on Submit button in CC Funnel Page
    And Verify CC Result Page
    And Click Apply Now button on any credit card list
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test CC" and Last Name "Selenium" and Email "test@gmail.com" and Phone Number "001234567"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify redirect site

  @cc-partner
  Scenario: CC CITI Submission until redirected to CITI Site
    Given Open landing page
    When Click on CC CTA button
    And Click on partner logo "CITI" on Our partners bar
    And Click on Compare Now button on CC partner page
    And Click Apply Now button on any credit card list on the bottom of page
    And Verify CC Funnel Page of applied partner
    And Select "Yes, over 1 year" for Do you have an active credit card question
    And Select "Employed in Private Sector" for What is your employment status question
    And Select "SSS ID" for Which of these documents could you provide question
    And Select "More than 3 years" for How long have you been in your current job question
    And Enter "50000" for What is your monthly income question
    And Click on Submit button in CC Funnel Page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "CCselenium" and Email "test123@gmail.com" and Phone Number "012345678"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify redirect site

  @cc-partner
  Scenario: CC Maybank Submission until redirected to Success page - via dashboard page
    Given Open landing page
    When Click on partner logo "Maybank" on Our partners bar
    And Click on Compare Now button on CC partner page
    And Click Apply Now button on any credit card list on the bottom of page
    And Verify CC Funnel Page of applied partner
    And Select "Yes, 6 months to 1 year" for Do you have an active credit card question
    And Select "Employed in Government" for What is your employment status question
    And Select "TIN ID" for Which of these documents could you provide question
    And Select "More than 3 years" for How long have you been in your current job question
    And Enter "50000" for What is your monthly income question
    And Click on Submit button in CC Funnel Page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "CCselenium" and Email "test123@gmail.com" and Phone Number "012345678"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify CC application success page

  @cc-partner @product-image
  Scenario: CC Metrobank Submission - via Product Image
    Given Open landing page
    When Click on CC CTA button
    And Click on partner logo "Metrobank" on Our partners bar
    And Click on Compare Now button on CC partner page
    And Click on any credit card image from the list at the bottom of page
    #can redirect to detail product page-standalone
    And Verify detail credit card product page
    And Click on Apply Now on CC detail page
    And Verify CC Funnel Page
    And Select "Yes, over 1 year" for Do you have an active credit card question
    And Select "Employed in Private Sector" for What is your employment status question
    And Select "SSS ID" for Which of these documents could you provide question
    And Select "More than 3 years" for How long have you been in your current job question
    And Enter "50000" for What is your monthly income question
    And Click on Submit button in CC Funnel Page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "CCselenium" and Email "test123@gmail.com" and Phone Number "012345678"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify CC application success page

  @cc-partner @product-image
  Scenario: CC AUB Submission - via Product name
    Given Open landing page
    When Click on CC CTA button
    And Click on partner logo "AUB" on Our partners bar
    And Click on Compare Now button on CC partner page
    And Click the product name on any credit card from the list at the bottom of page
    #can redirect to detail product page-standalone
    And Verify detail credit card product page
    And Click on Apply Now on CC detail page
    And Verify CC Funnel Page
    And Select "Yes, over 1 year" for Do you have an active credit card question
    And Select "Employed in Private Sector" for What is your employment status question
    And Select "SSS ID" for Which of these documents could you provide question
    And Select "More than 3 years" for How long have you been in your current job question
    And Enter "50000" for What is your monthly income question
    And Click on Submit button in CC Funnel Page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "CCselenium" and Email "test123@gmail.com" and Phone Number "012345678"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify redirect site

  #via Result - Product Name
  @product-page @cc-result @product-name
  Scenario: CC Product Page AUB Gold Mastercard Submission - via Product Image
    Given Open landing page
    When Click on CC CTA button
    And Click on Compare Credit Cards button on CC landing page
    And Verify CC Funnel Page
    And Select "Yes, over 1 year" for Do you have an active credit card question
    And Select "Employed in Private Sector" for What is your employment status question
    And Select "SSS ID" for Which of these documents could you provide question
    And Select "More than 3 years" for How long have you been in your current job question
    And Enter "90000" for What is your monthly income question
    And Click on Submit button in CC Funnel Page
    And Verify CC Result Page
    And Click "Asia United Bank (AUB)" product filter
    And Click "AUB Gold Mastercard" Product Name from credit card list
    #can redirect to detail product page-augmented
    And Verify detail credit card product page
    And Click on Apply Now on CC detail page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "CCselenium" and Email "test123@gmail.com" and Phone Number "012345678"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify redirect site

  #via Result - Product Image
  @Product-Page @cc-result @product-image
  Scenario: CC Product Page AUB Easy Mastercard Submission - via Product Image
    Given Open landing page
    When Click on CC CTA button
    And Click on Compare Credit Cards button on CC landing page
    And Verify CC Funnel Page
    And Select "Yes, over 1 year" for Do you have an active credit card question
    And Select "Employed in Private Sector" for What is your employment status question
    And Select "SSS ID" for Which of these documents could you provide question
    And Select "More than 3 years" for How long have you been in your current job question
    And Enter "90000" for What is your monthly income question
    And Click on Submit button in CC Funnel Page
    And Verify CC Result Page
    And Click "Asia United Bank (AUB)" product filter
    And Click "AUB Easy Mastercard" credit card image from credit card list
    #can redirect to detail product page-augmented
    And Verify detail credit card product page
    And Click on Apply Now on CC detail page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "CCselenium" and Email "test123@gmail.com" and Phone Number "012345678"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify redirect site