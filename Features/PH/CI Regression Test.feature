Feature: PH CI Regression Test

  Scenario: CI Lead Submission until thank you page
    Given Open landing page
    When Click on CI CTA button
    And Click on Get a free quote button on CI landing page
    And Verify Car Information Page
    And Through dropdown, Enter your car details: "Toyota", "Camry", "2017", and "G 2.5L Gas A/T FWD Sedan 5-Seater"
    And Enter your car usage information by select "Private" as What is the primary use of your car and "No" as Are you paying off a car loan and "Yes" as Include coverage for Acts of Nature and "No" as Do you have an existing car insurance
    And Click on Get result button on Car Information Page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test CI" and Last Name "Selenium" and Email "test@gmail.com" and Phone Number "001234567"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    And Wait until PI form disappear
    And Click Apply Now button on any car insurance data list
    Then Verify CI application success page

  @ci-partner
  Scenario: CI Stronghold Insurance - Get Quote Button
    Given Open landing page
    When Click on partner logo "Stronghold Insurance" on Our partners bar
    And Verify "Stronghold Insurance" partner page
    And Click on Get Quote button on CI partner page
    And Verify Car Information Page
    And Through dropdown, Enter your car details: "Toyota", "Camry", "2017", and "G 2.5L Gas A/T FWD Sedan 5-Seater"
    And Enter your car usage information by select "Private" as What is the primary use of your car and "No" as Are you paying off a car loan and "Yes" as Include coverage for Acts of Nature and "No" as Do you have an existing car insurance
    And Click on Get result button on Car Information Page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "bddbypartner" and Email "test@gmail.com" and Phone Number "0987654321"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    And Wait until PI form disappear
    And Click Apply Now button on any car insurance data list
    Then Verify CI application success page

  @ci-partner
  Scenario: CI MAPFRE - Submit Claim Button
    Given Open landing page
    When Click on partner logo "MAPFRE" on Our partners bar
    And Verify "MAPFRE" partner page
    And Click on Submit Claim button
    And Verify Car Information Page
    And Through dropdown, Enter your car details: "Toyota", "Camry", "2017", and "G 2.5L Gas A/T FWD Sedan 5-Seater"
    And Enter your car usage information by select "Private" as What is the primary use of your car and "No" as Are you paying off a car loan and "Yes" as Include coverage for Acts of Nature and "No" as Do you have an existing car insurance
    And Click on Get result button on Car Information Page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "bddbypartner" and Email "test@gmail.com" and Phone Number "0987654321"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    And Wait until PI form disappear
    And Click Apply Now button on any car insurance data list
    Then Verify CI application success page

  @ci-partner
  Scenario: The Mercantile Insurance Corporation - Get Quote Button - Filter by CI Category
    Given Open landing page
    When Click on CI CTA button
    And Click on partner logo "The Mercantile Insurance Corporation" on Our partners bar
    And Verify "The Mercantile Insurance Corporation" partner page
    And Click on Get Quote button on CI partner page
    And Verify Car Information Page
    And Through dropdown, Enter your car details: "Toyota", "Camry", "2017", and "G 2.5L Gas A/T FWD Sedan 5-Seater"
    And Enter your car usage information by select "Private" as What is the primary use of your car and "No" as Are you paying off a car loan and "Yes" as Include coverage for Acts of Nature and "No" as Do you have an existing car insurance
    And Click on Get result button on Car Information Page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "bddbypartner" and Email "test@gmail.com" and Phone Number "0987654321"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    And Wait until PI form disappear
    And Click Apply Now button on any car insurance data list
    Then Verify CI application success page