Feature: About Us

  Scenario: About Us - Our Team category
    Given Open landing page
    When Click on About Us navigation bar menu
    And Verify About page is displayed
    And Click on Our Team
    Then Verify Our Team navigation page is displayed

  Scenario: About Us - How we earn category
    Given Open landing page
    When Click on About Us navigation bar menu
    And Verify About page is displayed
    And Click on How we earn
    Then Verify How we earn navigation page is displayed

  Scenario: About Us - Press category
    Given Open landing page
    When Click on About Us navigation bar menu
    And Verify About page is displayed
    And Click on Press
    Then Verify Press navigation page is displayed
  
  Scenario: About Us - Careers category
    Given Open landing page
    When Click on About Us navigation bar menu
    And Verify About page is displayed
    And Click on Careers
    Then Verify Careers navigation page is displayed