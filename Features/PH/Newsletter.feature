Feature: Newsletter Subscription

    #----------Newletter Subscription----------#
  Scenario: Newsletter subscription - via footer
    Given Open landing page
    When Verify newsletter subscription is displayed
    And Submit a valid email address
    Then Newsletter subscription successful message is displayed

  Scenario: Newsletter subscription - via article page
    Given Open landing page
    When Hover pointer over articles menu in the navigation bar
    And Click on All Articles dropdown option
    And Verify All Articles page is displayed
    And Submit "ph.test.subcsribe@mail.com" as an email address on newsletter subscription in All Article page
    Then Newsletter subscription successful message is displayed