Feature: Privacy

  Scenario: Privacy Policy - directed to MAPFRE Insular page
    Given Open landing page
    When Scroll into More menu in the Moneymax footer
    And Click on Privacy Policy
    And Click MAPFRE Insular
    Then Verify the MAPFRE Insular page is displayed

  Scenario: Privacy Policy - directed to Mercantile Car Insurance page
    Given Open landing page
    When Scroll into More menu in the Moneymax footer
    And Click on Privacy Policy
    And Click Mercantile Car Insurance
    Then Verify the Mercantile Car Insurance page is displayed