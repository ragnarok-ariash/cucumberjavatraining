Feature: PH GDP Regression Test

  Scenario: GDP Lead Submission
    Given Open landing page
    When Click on GDP CTA button
    And Click on Let's Do It button on GDP landing page
    And Verify Purchase Info modal form appearance
    And Enter Purchase Info with data "06/08/2020" and "YES"
    And Click on Next button on gdp form
    And Enter Device Info: "Apple", "iPhone 11", "25000"
    And Click on Next button on gdp form
    And Fill Personal Info with First Name "Test GDP", Last Name "GDP", mobile number "09181111111", and Email address "test@gmail.com"
    And Agree on Term and Condition and Personal Data Notice of AmTrust Mobile Solutions and consent on both Moneymax Privacy Policy and marketting offers
    And Click on Submit button on gdp form
    And Verify OTP Page
    And Check to confirm have proof of purchase on OTP Page
    And Enter IMEI "938228882222222" on OTP Page
    And Click Get it now button on OTP Page
    Then Verify Paygate Web Payment