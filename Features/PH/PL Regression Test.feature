Feature: PH PL Regression Test

  Scenario: PL Lead Submission until redirected to Partner Site
    Given Open landing page
    When Click on PL CTA button
    And Click on Compare Personal Loan button on PL landing page
    And Verify PL Funnel Page
    And Select "Employed Private Sector" for employment status
    And Select "More than 3 years" for how long have been in current job
    And Enter "50000" for how much to borrow
    And Select "36 months" for how long to return
    And Enter "100000" for monthly income
    And Select "Yes, over 1 year" for having an active card
    And Click on Get Results button in PL Funnel Page
    And Verify PL Result Page
    And Click Apply Now button on any pl data list
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test PL" and Last Name "Selenium" and Email "test@gmail.com" and Phone Number "001234567"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify redirect site

    #GDFI (Global Dominion Financing Incorporated)
  @pl-partner
  Scenario: PL GDFI Submission until redirected to Success page - via PL page
    Given Open landing page
    When Click on PL CTA button
    And Click on partner logo "GDFI" on Our partners bar
    And Click Apply Now button on PL partner page
    And Verify PL Funnel Page of applied partner
    And Select "Employed Private Sector" for employment status
    And Select "More than 3 years" for how long have been in current job
    And Enter "50000" for how much to borrow
    And Select "36 months" for how long to return
    And Enter "100000" for monthly income
    And Select "Yes, over 1 year" for having an active card
    And Click on Get Results button in PL Funnel Page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "PLselenium" and Email "test123@gmail.com" and Phone Number "012345678"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify PL application success page


  #Personal Loan 2 TALA (Desktop & mobile)
  @pl-partner
  Scenario: PL TALA Submission until redirected to Success page - via dashboard page
    Given Open landing page
    And Click on partner logo "TALA" on Our partners bar
    And Click Apply Now button on PL partner page
    And Verify PL Funnel Page of applied partner
    And Select "Employed BPO" for employment status
    And Select "1 to 2 years" for how long have been in current job
    And Enter "50000" for how much to borrow
    And Select "24 months" for how long to return
    And Enter "100000" for monthly income
    And Select "Yes, 6 months to 1 year" for having an active card
    And Click on Get Results button in PL Funnel Page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "PLselenium" and Email "test123@gmail.com" and Phone Number "012345678"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify PL application success page

  #via Result - Product Image
  @ignore-this
  Scenario: PL Product TALA Submission - via Result Page by Product Image
    Given Open landing page
    When Click on PL CTA button
    And Click on Compare Personal Loan button on PL landing page
    And Verify PL Funnel Page
    And Select "Employed Private Sector" for employment status
    And Select "More than 3 years" for how long have been in current job
    And Enter "50000" for how much to borrow
    And Select "36 months" for how long to return
    And Enter "100000" for monthly income
    And Select "Yes, over 1 year" for having an active card
    And Click on Get Results button in PL Funnel Page
    And Verify PL Result Page
    And Click "TALA" Product Image from loan list
        #Choose product logo TALA and can redirect to product page - augmented version
    And Verify "TALA" detail product page appearance
    And Click Apply Now on detail product page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "Selenium" and Email "test@gmail.com" and Phone Number "001234567"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify redirect site

  #via Result - Product Name
  @ignore-this
  Scenario: PL RFC Lead Submission - via Result Page by Product Name
    Given Open landing page
    When Click on PL CTA button
    And Click on Compare Personal Loan button on PL landing page
    And Verify PL Funnel Page
    And Select "Employed Private Sector" for employment status
    And Select "More than 3 years" for how long have been in current job
    And Enter "50000" for how much to borrow
    And Select "36 months" for how long to return
    And Enter "100000" for monthly income
    And Select "Yes, over 1 year" for having an active card
    And Click on Get Results button in PL Funnel Page
    And Verify PL Result Page
    And Click "EasyRFC Multi-Purpose Loan" Product Name from loan list
        #Choose product name Easy RFC and can redirect to product page - augmented version
    And Verify "EasyRFC Multi-Purpose Loan" detail product page appearance
    And Click Apply Now on detail product page
    And Verify PI pop up form appearance
    And Enter Personal Information with First Name "Test" and Last Name "Selenium" and Email "test@gmail.com" and Phone Number "001234567"
    And Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers
    And Click on PI Submit button
    Then Verify redirect site