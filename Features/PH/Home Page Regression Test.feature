Feature: Home Page Tests

  Scenario: CI Navigation bar redirection
    Given Open landing page
    When Hover pointer over Car Insurance menu in the navigation bar
    And Click on Car Insurance dropdown option
    Then Verify Car Insurance page is displayed

  Scenario: CC Navigation bar redirection
    Given Open landing page
    When Hover pointer over Credit Card menu in the navigation bar
    And Click on Credit Card dropdown option
    Then Verify Credit Card page is displayed

  Scenario: PL Navigation bar redirection
    Given Open landing page
    When Hover pointer over Loans menu in the navigation bar
    And Click on Personal Loans dropdown option
    Then Verify Personal Loans page is displayed

  Scenario: GDP Navigation bar redirection
    Given Open landing page
    When Hover pointer over Gadget Protect menu in the navigation bar
    And Click on Gadget Protect dropdown option
    Then Verify Gadget Protect page is displayed

  Scenario: Articles Navigation bar redirection
    Given Open landing page
    When Hover pointer over articles menu in the navigation bar
    And Click on All Articles dropdown option
    Then Verify All Articles page is displayed

  Scenario: About Us Navigation bar redirection
    Given Open landing page
    When Click on About Us navigation bar menu
    Then Verify About page is displayed

  Scenario: Contact Us Navigation bar
    Given Open landing page
    When Click on Talk To Us navigation bar menu
    Then Verify Talk To Us Section is displayed

    #  - Privacy
  Scenario: Privacy Policy redirection
    Given Open landing page
    When Scroll into More menu in the Moneymax footer
    And Click on Privacy Policy
    Then Verify Privacy Policy page is displayed

    #  - Terms
  Scenario: Terms & Conditions redirection
    Given Open landing page
    When Scroll into More menu in the Moneymax footer
    And Click on Terms & Conditions
    Then Verify Terms & Conditions page is displayed