# Cucumber Java-Selenium
This application is **Maven**-based project developed for the purpose of automation testing. Regarding the automation testing, this project using **Cucumber** framework along with **Selenium with java**. Cucumber is a testing approach which supports Behavior Driven Development (BDD). It explains the behavior of the application in a simple English text using Gherkin language. On the other hand, Selenium is an automation tool for Functional Testing of the web-based application. Selenium supports different language like java, ruby, python C#, etc. This project combining these two technology to enable robust automation testing while easy to use due to BDD part of Cucumber (**Gherkin** language). You can find about cucumber [here](https://cucumber.io/docs/cucumber/) and documentation regarding the java part usage is [here](https://cucumber.io/docs/installation/java/) and about Selenium [here](https://www.selenium.dev/documentation/en/)

---

## Prerequisites

- JDK 11 or later ([download](https://jdk.java.net/archive/))
- Java IDE: Intellij, Eclipse, etc.
- Webdriver (explained below)

---

## Webdriver

Webdriver is binary file which is important component required to run automation testing with Selenium. This binary is browser specific which means each browser has their own version of webdriver. Previously, this binary files need to be downloaded and put into PATH, but now it's handled through project's library thus developer no longer need to have webdrivers installed.

---

## Run the project

Before you start to run the project, be sure to:

* After clone the project, import all the maven dependency. Usually there's a balloon in the bottom of your IDE to import all of the dependency.
* Ensure the project's SDK is set up to 11 or later. You need to check:
    - File > Project Structure > Project > Project SDK & Project language level
    - File > Project Structure > Modules > Dependencies > Module SDK
    - File > Project Structure > Modules > Sources > Language Level

After you set the webdriver, you can then run the project properly. Generally, Cucumber can be run through several ways, two of them can be done by using **command line** or using **junit** way as described [here](https://cucumber.io/docs/cucumber/api/#running-cucumber).

- ** Junit (*runner class*) :**
    This method is done by creating a runner class, then configure it to suit the test needs. This project already has runner classes you can use to run the test. It's located within **testrunners** package. So you can run the project by right click on this runner class then choose **"*run*"**. Please note that you might need to modify the configuration attached to the class to suit the test needs. If you are not familiar with the configuration setup, please refer to [this documentation](https://cucumber.io/docs/cucumber/api/#junit). Additionally, you might want to take a look at how to [running a subset of scenarios](https://cucumber.io/docs/cucumber/api/#running-a-subset-of-scenarios). Within testrunners package, there is ***ParallelRunner.java*** class which is **TestNG** runner class that can be used to execute the test scenarios parallely to save the time required to preform multiple test scenarios. But please note, when doing parallel execution, static variable might not work, so please use **pageobjects/PageData.java** class to share variable/state among steps.     

- ** Command Line (*plugin way*) :**
    Since this project already using maven and expected to be used with IDE, it's advised to run the project using plugin as described [here](https://cucumber.io/docs/tools/java/#ides) instead of manually execute cli commands through terminal/command prompt. To do so, please install "Cucumber for Java" plugin (along with "Gherkin" plugin if using InteliJ and Natural plugin if using eclipse). [Install plugin with IntelliJ](https://www.jetbrains.com/help/idea/managing-plugins.html#), [Install plugin with Eclipse](https://www.tutorialspoint.com/eclipse/eclipse_install_plugins.htm). After the plugin installed, you can choose from following methods that seems easier to do.
    
    - Click Add Configuration. It's located on top part of your IDE. After a window comes up, Add new configuration, select **"*Cucumber java*"**, then fill in the parameters as follow:
        * **Main class**: cucumber.api.cli.Main
        * **Glue**: stepdefinitions.base stepdefinitions.ph (or MY, TH - Combination of Base and PH/MY/TH)
        * **Feature of folder path**: {Select the feature file you want to test or provide directory path if you want to test more than one feature file within specific directory of the project}
        * **Program arguments**: --plugin org.jetbrains.plugins.cucumber.java.run.CucumberJvm3SMFormatter {usually already filled in automatically}. Additionally you can filter the scenarios to run by its tag by including this parameter ***--tags "@scenario-tag"***
        * **Environment Variables**: {as described in below part}
        
    - Right click on feature file or feature name/description to test, then click "Run" on the context menu to run all the scenarios for that particular feature. If you want to run only one certain scenario, right click on the scenario name then click "Run" on the context menu. For the first time, after click "Run" stop the process, because there are parameters that still need to fill. First, click on ***Edit Configuration***, it's located on top part of your IDE. After a window comes up, then fill in the parameters as follow:
        * **Glue**: stepdefinitions.base stepdefinitions.ph (or MY, TH - Combination of Base and PH/MY/TH)
        * **Environment Variables**: {as described in below part}
    
    Among those two steps, second way helps you to automatically fill in Main class, Feature path, and Program arguments. ***Alternatively***, you can still run the project through *build tools* way by runing this maven command:

    `mvn clean install -Dcucumber.options="--glue stepdefinitions \ --plugin pretty Feature" --name "^scenario-name$"`  
    


Before run the test, please setup **Environment Variable** for following keys:   

- "url" (required), it's the main/landing page url to be tested. Please supply the well-formatted URL by including 'http[s]://' and 'www'.
- "country" (required), it's the country code: [PH|MY|TH]
- "deploy_env" (required), it's the deployment environment code: [DEV|STAGING|PROD]
- "browser" (optional), it's kind of broweser: 'firefox' (default), 'chrome', 'IE', 'safari', or you can pass 'headless' for interface-less testing. Please note that clicking script sometimes might not working when doing testing in *headless* mode
- "display" [dekstop/mobile] (optional): ['desktop' (default) | 'mobile']
- "device_name", it's the device name based on supported emulation device onthe browser (e.g. 'Galaxy S5')
- "CUCUMBER_OPTIONS" (optional). Custom setting for cucumber to run with for example '--snippets camelcase' to print out java method in camelcase style when a gherkin step doesnt have its implementation.

---

## Development

In order to work with Cucumber Selenium in this project, you need to write BDD part and java part. These are you need to do:

* Make Feature file for BDD part
    - Create new file with extension .feature under Feature folder
    - Write the feature and scenario using Gherkin language
* Make Step definition for Java part
    - Create java class(es) to implment scenario from the Feature file. Create the java file under stepdefinitions package.
    - To implement certain step in java, you need to make a function and annotate it with Gherkin syntax along with step name. Step name on the annotation need to be exactly same with step defined in Feature file.
* Make Runner class
    - This part only for running the project through runner class. Runner class is empty class that need to be attached with special annotation. In this project this class need to be placed within testrunners package. Although you can take a look at already created runner class in this project, this class basically written as shown below. If you find difficulties regarding this one, please read the [documentation](https://cucumber.io/docs/cucumber/api/#junit).  
#  

```java
        @RunWith(Cucumber.class)
        @CucumberOptions(
            features = "Features/MYTest.feature",
            glue = {"stepdefinitions.base", "stepdefinitions.my"},
            plugin = {"pretty", "summary"}, strict = true, snippets = CAMELCASE,
            monochrome = true,
            dryRun = false /* set to false if want to perform actual test*/
        )
        public class RunnerClass {

        }
```

---

## Reporting

This project can be used to generate (HTML based) report based on the test result. Therefore, in order to generate report, you need to at least run the test until finished. To generate report, you can use terminal through IDE to directly navigate to the project root folder. At the root project run command `mvn verify`. Before you can generate report, you need to make sure to run the test using json formatter as the plugin, by checking this:

* **Runner Class:**
  You can use runner class and supply the annotation with `plugin = {"json:target/cucumber-reports/cucumber.json"}` inside `@CucumberOptions`
  
* **Plugin (right click):**
  If you prefer testing using IDE plugin, make sure to supply **Program arguments** field with `--plugin json:target/cucumber-reports/cucumber.json` by editing the run configuration instead of using `org.jetbrains.plugins.cucumber.java.run.CucumberJvm3SMFormatter` as the plugin


---

## Jenkins Integration

This section explains steps to run cucumber through Jenkins. Prequisite before doing this steps are having jenkins installed, know how to install jenkins plugin, and know how to create new job. Credit to [softwaretestinghelp](https://www.softwaretestinghelp.com/cucumber-jenkins-tutorial/).

1. Install following plugins:
    - Cucumber Plugin, 
    - Cucumber Living Documentation Plugin, 
    - Cucumber reports,
    - Cucumber json test reporting,
    - Cucumber Trend Reports,
    - cucumber-slack-notifier,
    - cucumber-perf
    - EnvInjector plugin
    
    Make sure while installing all the dependent plugins are successfully installed or else the reporting plugins might not work
    
2. Navigate to Manage -> Global Tool Configuration and set the path for JDK
    - Fill JAVA_HOME in Name input
    - Fill JDK path in JAVA_HOME input
    - Uncheck 'install automatically'
    
3. Set the path for Maven
    - Fill MAVEN_HOME in Name input
    - Fill Maven path in MAVEN_HOME input
    - Uncheck 'install automatically'
    
4. Create new job as Maven Project
5. Navigate to Build section, then 
    - Fill this project pom.xml file path in Root POM input
    - Fill 'test' in Goals and options input
    
6. Navigate to Pre Steps section, then fill in **Properties Content** with Environtment Variables as explained in top section of this README.
7. Once the above steps are done, click on the ***Build Now*** that is present in the left-hand panel of the Project. The build will be executed and the corresponding testing.xml file (which is mentioned over the pom.xml) will get executed. You might want to click on the build (number) that appears in bottom pannel to see the detailed log.
