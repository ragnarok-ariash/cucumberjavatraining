package testrunners;

import io.cucumber.junit.Cucumber;
import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;

import org.junit.runner.RunWith;
import org.testng.annotations.DataProvider;

import static io.cucumber.testng.CucumberOptions.SnippetType.CAMELCASE;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"Features/PH/About Us.feature"},
        glue = {"stepdefinitions.base", "stepdefinitions.ph"},
        plugin = {"org.jetbrains.plugins.cucumber.java.run.CucumberJvm3SMFormatter",
                "json:target/cucumber-reports/cucumber.json"},
        strict = true, snippets = CAMELCASE,
        monochrome = true,
        tags = "not @ignore-this"
)
public class ParallelRunner extends AbstractTestNGCucumberTests {

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }


}
