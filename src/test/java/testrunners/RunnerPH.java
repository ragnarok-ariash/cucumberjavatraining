package testrunners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import static io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"Features/PH/", "Features/PHTest.feature"},
        glue = {"StepDefinition.Base", "StepDefinition.PH"},
        plugin = {"org.jetbrains.plugins.cucumber.java.run.CucumberJvm3SMFormatter",
                "pretty",
                "summary",
                "html:target/cucumber-reports",
                "json:target/cucumber-reports/cucumber.json"},
        strict = true, snippets = CAMELCASE,
        monochrome = true,
        tags = "not @ignore-this",
        dryRun = false /* set to true if want to test unimplemented steps*/
)
public class RunnerPH {
}
