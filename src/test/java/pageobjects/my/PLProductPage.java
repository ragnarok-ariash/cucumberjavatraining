package pageobjects.my;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pageobjects.AbstractPageFactory;
import utils.SeleniumUtil;

import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;
import static pageobjects.my.PLProductPage.SECTIONTAB.*;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class PLProductPage extends AbstractPageFactory {
    private static final String APPLY_BTN_XPATH = "//div[@id='btn-apply-desktop']/button";
    private static final String APPLY_BTN_MOBILE_XPATH = "//div[@id='btn-apply-mobile']/button";

    public enum SECTIONTAB {
        FEATURES,
        ELIGIBILITY,
        FEE,
        DOCUMENT
    }

    @FindBy(how = How.ID, using = "navbar")
    public WebElement navbar;

    @FindBy(how = How.XPATH, using = APPLY_BTN_XPATH)
    private WebElement applyNowBtn;

    @FindBy(how = How.XPATH, using = APPLY_BTN_XPATH+"/a")
    private WebElement applyNowBtnLink;

    @FindBy(how = How.XPATH, using = APPLY_BTN_MOBILE_XPATH)
    private WebElement applyNowBtnMobile;

    @FindBy(how = How.XPATH, using = APPLY_BTN_MOBILE_XPATH+"/a")
    private WebElement applyNowBtnLinkMobile;

    @FindBy(how = How.XPATH, using = "//div[@class='bottom-footer padding-bottom']")
    public WebElement footerView;

    @FindBy(how = How.XPATH, using = "//div[@class='product-sticky-right']/a[contains(.,'Apply now')]")
    public WebElement stickyBtmApplyNowBtn;

    @FindBy(how = How.XPATH, using = "//div[@class='col-sm-7 pl-detail']//div[@class='promotion-box margin-bottom-space']")
    private WebElement limitedTimePromoView;

    @FindBy(how = How.XPATH, using = "//div[@class='col-sm-7 pl-detail pl-detail-set-margin']//div[@class='promotion-box margin-bottom-space']")
    private WebElement limitedTimePromoMobileView;

    @FindBy(how = How.XPATH, using = "//div[@class='product-detail']/div[@class='hidden-xs product-page-title']/h3[.='Product details']")
    private WebElement productDetailLabel;

    @FindBy(how = How.XPATH, using = "//div[@class='product-detail']/div[@class='hidden-xs product-page-title']/h3[.='Product details']")
    private WebElement productDetailLabelMobile;

    @FindBy(how = How.XPATH, using = "//div[@class='product-detail']/div[@class='hidden-xs product-page-title']/h3[.='Loan repayment table']")
    public WebElement loanRepaymentTableLabel;

    @FindBy(how = How.XPATH, using = "//h4[.='Looking for more options?']")
    public WebElement lookingForMoreOptionQLabel;

    @FindBy(how = How.XPATH, using = "//a[@class = 'nav-link' and .='Features']")
    private WebElement productDetailFeatureTab;
    @FindBy(how = How.XPATH, using = "//div[contains(@class,'navbar-product-page-mb visible-xs')]/a[.= 'Features']")
    private WebElement productDetailFeatureTabMobile;

    @FindBy(how = How.XPATH, using = "//a[@class = 'nav-link' and .='Eligibility']")
    private WebElement productDetailEligibilityTab;
    @FindBy(how = How.XPATH, using = "//div[contains(@class,'navbar-product-page-mb visible-xs')]/a[.= 'Eligibility']")
    private WebElement productDetailEligibilityTabMobile;

    @FindBy(how = How.XPATH, using = "//a[@class = 'nav-link' and .='Fees & Charges']")
    private WebElement productDetailFeeTab;
    @FindBy(how = How.XPATH, using = "//div[contains(@class,'navbar-product-page-mb visible-xs')]/a[.= 'Fees & Charges']")
    private WebElement productDetailFeeTabMobile;

    @FindBy(how = How.XPATH, using = "//a[@class = 'nav-link' and .='Document required to apply']")
    private WebElement productDetailDocumentTab;
    @FindBy(how = How.XPATH, using = "//div[contains(@class,'navbar-product-page-mb visible-xs')]/a[.= 'Document required to apply']")
    private WebElement productDetailDocumentTabMobile;

    @FindBy(how = How.XPATH, using = "//div[@class='product-detail']//div[@id='features']")
    public WebElement productDetailFeatureTabContent;

    @FindBy(how = How.XPATH, using = "//div[@class='product-detail']//div[@id='eligibility']")
    public WebElement productDetailEligibilityTabContent;

    @FindBy(how = How.XPATH, using = "//div[@class='product-detail']//div[@id='fee']")
    public WebElement productDetailFeeTabContent;

    @FindBy(how = How.XPATH, using = "//div[@class='product-detail']//div[@id='doc-required']")
    public WebElement productDetailDocumentTabContent;

    @FindBy(how = How.XPATH, using = "//div[@class='product-page-pl-calculator-box set-mt-50 set-ml-5']")
    public WebElement monthlyRepaymentCalcLeftView;

    @FindBy(how = How.XPATH, using = "//div[@class='product-page-pl-calculator-box-right set-ml-5']")
    public WebElement monthlyRepaymentCalcRightView;

    @FindBy(how = How.XPATH, using = "//button[contains(@class,'openStateToggle')]")
    public WebElement surveyCollapseBtn;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'surveyContainer')]")
    public WebElement surveyContentView;

    @FindBy(how = How.XPATH, using = "//div[@class='row no-margin-side hidden-xs']")
    private WebElement moreOptionProductsContainer;
    @FindBy(how = How.XPATH, using = "//div[@class='carousel-inner']")
    private WebElement moreOptionProductsContainerMobile;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'product-more')]")
    private List<WebElement> moreOptionProducts;
    @FindBy(how = How.XPATH, using = "//div[@class='carousel-inner']/div")
    private List<WebElement> moreOptionProductsMobile;

    private static final String MORE_OPTION_CITIPL_XPATH = "//div[contains(@class, 'product-more')]/a[contains(@href, 'product/citibank')]";
    private static final String MORE_OPTION_CITIPL_MOBILE_XPATH = "//div[contains(@class, 'item')]//a[contains(@href, 'product/citibank')]";
    @FindBy(how = How.XPATH, using = MORE_OPTION_CITIPL_XPATH+"/parent::div")
    private WebElement moreOptionCitiPL;
    @FindBy(how = How.XPATH, using = MORE_OPTION_CITIPL_MOBILE_XPATH+"/ancestor::div[contains(@class, 'item')]")
    private WebElement moreOptionCitiPLMobile;
    @FindBy(how = How.XPATH, using = MORE_OPTION_CITIPL_XPATH)
    private WebElement moreOptionCitiPLLink;
    @FindBy(how = How.XPATH, using = MORE_OPTION_CITIPL_MOBILE_XPATH)
    private WebElement moreOptionCitiPLLinkMobile;

    private static final String MORE_OPTION_SCB_XPATH = "//div[contains(@class, 'product-more')]/a[contains(@href, 'product/standard-chartered-cash-one')]";
    private static final String MORE_OPTION_SCB_MOBILE_XPATH = "//div[contains(@class, 'item')]//a[contains(@href, 'product/standard-chartered-cash-one')]";
    @FindBy(how = How.XPATH, using = MORE_OPTION_SCB_XPATH+"/parent::div")
    private WebElement moreOptionSCB;
    @FindBy(how = How.XPATH, using = MORE_OPTION_SCB_MOBILE_XPATH+"/ancestor::div[contains(@class, 'item')]")
    private WebElement moreOptionSCBMobile;
    @FindBy(how = How.XPATH, using = MORE_OPTION_SCB_XPATH)
    private WebElement moreOptionSCBLink;
    @FindBy(how = How.XPATH, using = MORE_OPTION_SCB_MOBILE_XPATH)
    private WebElement moreOptionSCBLinkMobile;

    private static final String MORE_OPTION_JCLPL_XPATH = "//div[contains(@class, 'product-more')]/a[contains(@href, 'product/jcl')]";
    private static final String MORE_OPTION_JCLPL_MOBILE_XPATH = "//div[contains(@class, 'item')]//a[contains(@href, 'product/jcl')]";
    @FindBy(how = How.XPATH, using = MORE_OPTION_JCLPL_XPATH+"/parent::div")
    private WebElement moreOptionJCLPL;
    @FindBy(how = How.XPATH, using = MORE_OPTION_JCLPL_MOBILE_XPATH+"/ancestor::div[contains(@class, 'item')]")
    private WebElement moreOptionJCLPLMobile;
    @FindBy(how = How.XPATH, using = MORE_OPTION_JCLPL_XPATH)
    private WebElement moreOptionJCLPLLink;
    @FindBy(how = How.XPATH, using = MORE_OPTION_JCLPL_MOBILE_XPATH)
    private WebElement moreOptionJCLPLLinkMobile;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'product-more')]//img[@alt = 'Personal Financing-i']/parent::div")
    private WebElement moreOptionHongleongBankPFImageView;
    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'item')]//img[@alt='Personal Financing-i']/parent::div")
    private WebElement moreOptionHongleongBankPFImageViewMobile;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'product-more')]//div[.= 'Personal Financing-i']")
    private WebElement moreOptionHongleongBankPFLabel;
    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'item')]//div[contains(@class, 'more-options-content') and contains(.,'Personal Financing-i')]")
    private WebElement moreOptionHongleongBankPFLabelMobile;

    @FindBy(how = How.XPATH, using = "//div[@id='newsletter']")
    public WebElement newsletterSectionView;

    @FindBy(how = How.XPATH, using = "//input[@id='subscribe-email-input']")
    private WebElement newsletterEmailInput;
    @FindBy(how = How.XPATH, using = "//input[@id='subscribe-email-input-mobile']")
    private WebElement newsletterEmailInputMobile;

    @FindBy(how = How.XPATH, using = "//span[@id='validation-text']")
    private WebElement newsletterSubscribeValidationText;
    @FindBy(how = How.XPATH, using = "//span[@id='validation-text-mobile']")
    private WebElement newsletterSubscribeValidationTextMobile;

    @FindBy(how = How.XPATH, using = "//div[@id='validation-popup']")
    private WebElement newsletterSubscribeValidationPopup;
    @FindBy(how = How.XPATH, using = "//div[@id='validation-popup-mobile']")
    private WebElement newsletterSubscribeValidationPopupMobile;

    @FindBy(how = How.XPATH, using = "//div[@id='mail-button']")
    private WebElement newsletterSubscribeBtn;
    @FindBy(how = How.ID, using = "mail-button-mobile")
    private WebElement newsletterSubscribeBtnMobile;

    @FindBy(how = How.XPATH, using = "//tr[@class='repayment-thead-tr']/th[@class = 'thead-loan-tenure']")
    private List<WebElement> monthlyRepaymenTableHead;
    @FindBy(how = How.XPATH, using = "//table[@class='check-ios-table']//th[@class='cag-td-col cag-td-col-top set-box-shadow-7']")
    private List<WebElement> monthlyRepaymenTableHeadMobile;

    @FindBy(how = How.XPATH, using = "//tbody[@class='repayment-tbody repayment-tbody-left ns-repayment-tbody']/tr")
    private List<WebElement> loanAmountTablerow;
    @FindBy(how = How.XPATH, using = "//table[@class='check-ios-table']/tbody/tr")
    private List<WebElement> loanAmountTablerowMobile;

    @FindBy(how = How.XPATH, using = "//h1[@class='main-title']")
    public WebElement mainTitle;
    @FindBy(how = How.XPATH, using = "//div[@class='col-sm-3 img-wrap-pl']/img[@alt='Personal Loan']")
    private WebElement productImage;
    @FindBy(how = How.XPATH, using = "//div[@class='col-sm-12 pl-detail']/img[@alt='Personal Loan']")
    private WebElement productImageMobile;


    @Autowired
    private SeleniumUtil seleniumUtil;

    public PLProductPage(GlobalManager globalManager) {
        super(globalManager);
    }

    public void closeSurvey() {
        if (seleniumUtil.elementExist(By.xpath("//div[contains(@class, 'surveyContainer')]"))) {
            seleniumUtil.clickElement(surveyCollapseBtn);
            seleniumUtil.waitUntilAttributeElementContains(surveyContentView, "class", "minimized");
        }
    }

    public WebElement getApplyNowBtn() {
        return globalManager.display == DisplayType.DESKTOP ? applyNowBtn : applyNowBtnMobile;
    }

    public WebElement getApplyNowBtnLink() {
        return globalManager.display == DisplayType.DESKTOP ? applyNowBtnLink : applyNowBtnLinkMobile;
    }

    public WebElement getLimitedTimePromoView() {
        return globalManager.display == DisplayType.DESKTOP ? limitedTimePromoView : limitedTimePromoMobileView;
    }

    public WebElement getProductDetailTab(SECTIONTAB sectiontab) {
        switch (sectiontab) {
            case FEATURES:
                return globalManager.display == DisplayType.DESKTOP ? productDetailFeatureTab : productDetailFeatureTabMobile;
            case ELIGIBILITY:
                return globalManager.display == DisplayType.DESKTOP ? productDetailEligibilityTab : productDetailEligibilityTabMobile;
            case FEE:
                return globalManager.display == DisplayType.DESKTOP ? productDetailFeeTab : productDetailFeeTabMobile;
            case DOCUMENT:
            default:
                return globalManager.display == DisplayType.DESKTOP ? productDetailDocumentTab : productDetailDocumentTabMobile;
        }
    }

    public WebElement getProductDetailTabContent(SECTIONTAB sectiontab) {
        switch (sectiontab) {
            case FEATURES:
                return productDetailFeatureTabContent;
            case ELIGIBILITY:
                return productDetailEligibilityTabContent;
            case FEE:
                return productDetailFeeTabContent;
            case DOCUMENT:
            default:
                return productDetailDocumentTabContent;
        }
    }

    public SECTIONTAB getEnumForTabTitle(String tabTitle) {
        switch (tabTitle) {
            case "Features":
            default:
                return FEATURES;
            case "Eligibility":
                return ELIGIBILITY;
            case "Fees & Charges":
                return FEE;
            case "Document required to apply":
                return DOCUMENT;
        }
    }

    public WebElement getEligibilityTable() {
        String tableXpath = globalManager.display == DisplayType.DESKTOP ?
                ".//table[@class='hidden-xs table']" :
                ".//table[@class='visible-xs table']";

        return productDetailEligibilityTabContent.findElement(By.xpath(tableXpath));
    }

    public WebElement getFeeAndChargeTable() {
        String tableXpath = globalManager.display == DisplayType.DESKTOP ?
                ".//table[@class='hidden-xs table']" :
                ".//table[@class='visible-xs table']";

        return productDetailFeeTabContent.findElement(By.xpath(tableXpath));
    }

    public WebElement getProductDetailLabel() {
        return globalManager.display == DisplayType.DESKTOP ? productDetailLabel : productDetailLabelMobile;
    }

    public List<WebElement> getMonthlyRepaymentTableHead() {
        return globalManager.display == DisplayType.DESKTOP ? monthlyRepaymenTableHead : monthlyRepaymenTableHeadMobile;
    }

    public List<WebElement> getLoanAmountTablerow() {
        return globalManager.display == DisplayType.DESKTOP ? loanAmountTablerow : loanAmountTablerowMobile;
    }

    public WebElement getMoreOptionCitiPL() {
        return globalManager.display == DisplayType.DESKTOP ? moreOptionCitiPL : moreOptionCitiPLMobile;
    }

    public WebElement getMoreOptionCitiPLLink() {
        return globalManager.display == DisplayType.DESKTOP ? moreOptionCitiPLLink : moreOptionCitiPLLinkMobile;
    }

    public WebElement getMoreOptionSCB() {
        return globalManager.display == DisplayType.DESKTOP ? moreOptionSCB : moreOptionSCBMobile;
    }

    public WebElement getMoreOptionSCBLink() {
        return globalManager.display == DisplayType.DESKTOP ? moreOptionSCBLink : moreOptionSCBLinkMobile;
    }

    public WebElement getMoreOptionJCLPL() {
        return globalManager.display == DisplayType.DESKTOP ? moreOptionJCLPL : moreOptionJCLPLMobile;
    }

    public WebElement getMoreOptionJCLPLLink() {
        return globalManager.display == DisplayType.DESKTOP ? moreOptionJCLPLLink : moreOptionJCLPLLinkMobile;
    }

    public WebElement getMoreOptionHongleongBankPFImageView() {
        return globalManager.display == DisplayType.DESKTOP ? moreOptionHongleongBankPFImageView : moreOptionHongleongBankPFImageViewMobile;
    }

    public WebElement getMoreOptionHongleongBankPFLabel() {
        return globalManager.display == DisplayType.DESKTOP ? moreOptionHongleongBankPFLabel : moreOptionHongleongBankPFLabelMobile;
    }

    public WebElement getNewsletterEmailInput() {
        return globalManager.display == DisplayType.DESKTOP ? newsletterEmailInput : newsletterEmailInputMobile;
    }

    public WebElement getNewsletterSubscribeValidationPopup() {
        return globalManager.display == DisplayType.DESKTOP ? newsletterSubscribeValidationPopup : newsletterSubscribeValidationPopupMobile;
    }

    public WebElement getNewsletterSubscribeValidationText() {
        return globalManager.display == DisplayType.DESKTOP ? newsletterSubscribeValidationText : newsletterSubscribeValidationTextMobile;
    }

    public WebElement getNewsletterSubscribeBtn() {
        return globalManager.display == DisplayType.DESKTOP ? newsletterSubscribeBtn : newsletterSubscribeBtnMobile;
    }

    public WebElement getProductImage() {
        return globalManager.display == DisplayType.DESKTOP ? productImage : productImageMobile;
    }

    public WebElement getAnyMoreOptionProduct() {
        seleniumUtil.waitUntilChildElementVisible(
                globalManager.display ==  DisplayType.DESKTOP ?
                        moreOptionProductsContainer : moreOptionProductsContainerMobile,
                By.tagName("div"));
        return globalManager.display ==  DisplayType.DESKTOP ? moreOptionProducts.get(0) : moreOptionProductsMobile.get(0);
    }

}
