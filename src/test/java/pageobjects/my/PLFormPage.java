package pageobjects.my;

import globalmanagers.GlobalManager;
import io.cucumber.spring.ScenarioScope;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pageobjects.AbstractPageFactory;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class PLFormPage extends AbstractPageFactory {

    @FindBy(how = How.ID, using = "loan-amount")
    public WebElement loanAmoutInput;

    @FindBy(how = How.ID, using = "ddl-loan-tenure")
    public WebElement loanTenureDropdown;

    @FindBy(how = How.ID, using = "button-submit-loan")
    public WebElement getResultBtn;

    public PLFormPage(GlobalManager globalManager) {
        super(globalManager);
    }
}
