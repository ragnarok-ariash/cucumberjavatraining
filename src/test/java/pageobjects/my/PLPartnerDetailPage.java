package pageobjects.my;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pageobjects.AbstractPageFactory;
import utils.SeleniumUtil;

import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;
import static org.junit.Assert.assertNotEquals;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class PLPartnerDetailPage extends AbstractPageFactory {

    @FindBy(how = How.XPATH, using = "//div[@class='list-data-loan']")
    public WebElement listDataLoan;

    @FindBy(how = How.XPATH, using = "//div[@class='list-data-loan']/div")
    public List<WebElement> loanListItems;

    @Autowired
    private SeleniumUtil seleniumUtil;

    public PLPartnerDetailPage(GlobalManager globalManager) {
        super(globalManager);
    }

    public WebElement getAnyLoanItem() {
        seleniumUtil.waitUntilChildElementVisible(listDataLoan, By.tagName("div"));
        assertNotEquals(0, loanListItems.size());
        return loanListItems.get(0);
    }

    private String getImageWrapperPath() {
        return globalManager.display == DisplayType.DESKTOP ?
                ".//div[@class='hidden-xs hidden-sm text-center']" :
                ".//div[@class = 'visible-xs visible-sm']//div[contains(@class, 'sponsored-logo-gap')]";
    }

    public String getProductUrlFromLoanItem(WebElement loanItem) {
        return loanItem.findElement(By.xpath(getImageWrapperPath() + "/a[@class = 'product_pl_link']")).getAttribute("href");
    }

    public WebElement getImageElementFromLoanItem(WebElement loanItem) {
        return loanItem.findElement(By.xpath(getImageWrapperPath() + "/a[@class = 'product_pl_link']/img"));
    }
}
