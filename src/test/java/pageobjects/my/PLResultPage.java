package pageobjects.my;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pageobjects.AbstractPageFactory;
import utils.SeleniumUtil;

import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class PLResultPage extends AbstractPageFactory {

    @FindBy(how = How.ID, using = "list-data")
    public WebElement listContainer;

    @FindBy(how = How.XPATH, using = "//div[@id='list-data']/child::*")
    public List<WebElement> listItems;

    @Autowired
    private SeleniumUtil seleniumUtil;

    public PLResultPage(GlobalManager globalManager) {
        super(globalManager);
    }

    private WebElement getDataFromListItem(int index) {
        seleniumUtil.waitFor(ExpectedConditions.presenceOfNestedElementLocatedBy(listContainer, By.tagName("div")));
        //verify is there any data to select
        assertNotEquals(0, listItems.size());
        return listItems.get(index);
    }

    private WebElement getApplyBtnOutOfThisItem(WebElement dataItem) {
        return dataItem.findElement(By.xpath(".//div[@class='hidden-xs hidden-sm text-center']/a[contains(.,'Apply Now')]"));
    }

    private WebElement getTitleOutOfThisItem(WebElement dataItem) {
        return dataItem.findElement(By.xpath(globalManager.display == DisplayType.DESKTOP ?
                ".//div[contains(@class, 'panel-heading')]//div[@class = 'hidden-xs hidden-sm']//b/a" :
                "//h2[@class='media-heading']//a"));
    }

    public void applyOnThisItem(WebElement itemToApply) {
        seleniumUtil.clickElement(getApplyBtnOutOfThisItem(itemToApply));
    }

    public WebElement getAnyProduct() {
        return getDataFromListItem(0);
    }

    public WebElement getAnyProductName() {
        return getTitleOutOfThisItem(getAnyProduct());
    }

    public String applyOnAnyProduct() {
        String urlToBe = getApplyBtnOutOfThisItem(getAnyProduct()).getAttribute("href");
        assertNotNull(urlToBe);
        applyOnThisItem(getAnyProduct());
        seleniumUtil.waitUntilFinishRedirectToURL(urlToBe);

        return urlToBe;
    }
}
