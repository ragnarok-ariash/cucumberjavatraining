package pageobjects.my;

import globalmanagers.GlobalManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pageobjects.AbstractPageFactory;
import utils.SeleniumUtil;

import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;
import static org.junit.Assert.assertNotEquals;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class PartnerPage extends AbstractPageFactory {

    @FindBy(how = How.XPATH, using = "//div[@id='pills-all']//h1[contains(.,'Personal Loan Providers')]")
    public WebElement plProviderLabel;

    @FindBy(how = How.XPATH, using = "//div[@id='personal-loan-provider-all']")
    public WebElement plProviderList;

    @FindBy(how = How.XPATH, using = "//div[@id='personal-loan-provider-all']/div")
    public List<WebElement> plProviderListItems;

    @Autowired
    private SeleniumUtil seleniumUtil;

    public PartnerPage(GlobalManager globalManager) {
        super(globalManager);
    }

    public WebElement getAnyPLProvider() {
        seleniumUtil.waitUntilChildElementVisible(plProviderList, By.tagName("div"));
        assertNotEquals(0, plProviderListItems.size());
        return plProviderListItems.get(0);
    }
}
