package pageobjects.my;

import globalmanagers.GlobalManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pageobjects.AbstractPageFactory;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class PLInfoFormPage extends AbstractPageFactory {
    @FindBy(how = How.XPATH, using = "//div[@class='overflow-hidden form-container']")
    public WebElement formContainer;

    @FindBy(how = How.XPATH, using = "//form[@class='flex section-form-content']")
    public WebElement formContent;

    @FindBy(how = How.XPATH, using = "//span[@class='form-application-step']")
    public WebElement formStepLabel;

    public PLInfoFormPage(GlobalManager globalManager) {
        super(globalManager);
    }
}
