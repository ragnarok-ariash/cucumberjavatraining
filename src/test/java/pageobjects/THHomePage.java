package pageobjects;

import stepdefinitions.base.BaseSteps;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class THHomePage  extends HomePageView {
    @FindBy(how = How.XPATH, using = "//div[@class='col-sm-8 col-md-6 three-box']//a[contains(.,'ประกันรถยนต์')]")
    @CacheLookup
    public WebElement CICTABtn;

    @FindBy(how = How.XPATH, using = "//div[@class='col-sm-8 col-md-6 three-box']//a[contains(.,'บัตรเครดิต')]")
    @CacheLookup
    public WebElement CCCTABtn;

    @FindBy(how = How.XPATH, using = "//div[@class='col-sm-8 col-md-6 three-box']//a[contains(.,'สินเชื่อส่วนบุคคล')]")
    @CacheLookup
    public WebElement PLCTABtn;

    @FindBy(how = How.ID, using = "onesignal-popover-cancel-button")
    public WebElement oneSignalPopup;

    @Override
    public void clickOnCtaBtn(BaseSteps.CTAType ctaType, WebDriver driver) {
        WebElement ctabtn = null;
        switch (ctaType) {
            case CI_CTA:
                ctabtn = CICTABtn;
                break;
            case CC_CTA:
                ctabtn = CCCTABtn;
                break;
            case PL_CTA:
                ctabtn = PLCTABtn;
                break;
        }
        clickCtaBtn(ctabtn, driver);
    }
}
