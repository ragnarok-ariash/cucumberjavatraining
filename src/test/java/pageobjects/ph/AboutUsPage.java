package pageobjects.ph;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AboutUsPage {
    @FindBy(how = How.XPATH, using = "//li/a[.='OUR TEAM']")
    public WebElement ourTeamTab;
    @FindBy(how = How.ID, using = "about2")
    public WebElement ourTeamTabContent;

    @FindBy(how = How.XPATH, using = "//li/a[.='HOW WE EARN']")
    public WebElement howWeEarnTab;
    @FindBy(how = How.ID, using = "about3")
    public WebElement howWeEarnTabContent;

    @FindBy(how = How.XPATH, using = "//li/a[.='PRESS']")
    public WebElement pressTab;
    @FindBy(how = How.ID, using = "about4")
    public WebElement pressTabContent;

    @FindBy(how = How.XPATH, using = "//li/a[.='CAREERS']")
    public WebElement careerTab;
    @FindBy(how = How.ID, using = "about5")
    public WebElement careerTabContent;
}
