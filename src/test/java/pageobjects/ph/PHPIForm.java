package pageobjects.ph;

import utils.SeleniumUtil;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertTrue;

public class PHPIForm {
    @FindBy(how = How.XPATH, using = "//div[@id = 'personal-info']")
    public WebElement formContainer;

    @FindBy(how = How.XPATH, using = "//div[@id = 'personal-info']//div[@class = 'modal-body']")
    public WebElement formBody;

    @FindAll({
            @FindBy(how = How.ID, using = "first-name-ci"),
            @FindBy(how = How.ID, using = "first-name-cc"),
            @FindBy(how = How.ID, using = "first-name-pl"),
            @FindBy(how = How.ID, using = "first-name-loan")
    })
    @CacheLookup
    public WebElement firstNameInput;

    @FindAll({
            @FindBy(how = How.ID, using = "last-name-ci"),
            @FindBy(how = How.ID, using = "last-name-cc"),
            @FindBy(how = How.ID, using = "last-name-loan")
    })
    @CacheLookup
    public WebElement lastNameInput;

    @FindAll({
            @FindBy(how = How.ID, using = "email-ci"),
            @FindBy(how = How.ID, using = "email-cc"),
            @FindBy(how = How.ID, using = "email-loan")
    })
    @CacheLookup
    public WebElement emailInput;

    @FindAll({
            @FindBy(how = How.ID, using = "phone-ci"),
            @FindBy(how = How.ID, using = "phone-cc"),
            @FindBy(how = How.ID, using = "phone-loan")
    })
    @CacheLookup
    public WebElement phoneInput;

    @FindAll({
            @FindBy(how = How.CSS, using = "button[data-selector-id='pi_submit_button']"),
            @FindBy(how = How.XPATH, using = "//button[contains(@class,'btn-show-result-loan')]"),
            @FindBy(how = How.XPATH, using = "//button[contains(@class,'btn-show-result-cc')]")
    })
    public WebElement submitBtn;

    @FindBy(how = How.XPATH, using = "//div[@class='checkbox check']//input[@name='agree-term-cond']")
    private WebElement tncCheckbox;

    @FindAll({
            @FindBy(how = How.XPATH, using = "//input[@id='ci-checkbox-offers']"),
            @FindBy(how = How.XPATH, using = "//input[@id='cc-checkbox-offers']"),
            @FindBy(how = How.XPATH, using = "//input[@id='pl-checkbox-offers']")
    })
    private WebElement consentCheckbox;

    public void setElement(WebElement element, String elementVal, SeleniumUtil seleniumUtil) {
        seleniumUtil.waitFor(ExpectedConditions.elementToBeClickable(element));
        retryingFindClick(element);
        element.clear();
        seleniumUtil.enterInputWithTextByElement(elementVal, element, true, false);
    }

    public void checkAgreement(SeleniumUtil seleniumUtil) {
        if (!tncCheckbox.isSelected())
            checkCheckBox(tncCheckbox, seleniumUtil);

        if (!consentCheckbox.isSelected())
            checkCheckBox(consentCheckbox, seleniumUtil);

//        WebDriverWait wait = new WebDriverWait(driver, EnvManager.getInstance().getTimeout());
//        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name='agree-term-cond']")));
//        List<WebElement> checkboxes = driver.findElements(By.cssSelector("input[name='agree-term-cond']"));
//        checkboxes.stream()
//                .filter(elem -> !elem.isSelected())
//                .forEach(elem -> elem.findElement(By.xpath("following-sibling::span[@class='checkmark']")).click());
    }

    private void checkCheckBox(WebElement checkbox, SeleniumUtil seleniumUtil) {
        seleniumUtil.clickElementUsingScript(checkbox);
        seleniumUtil.waitFor(ExpectedConditions.elementSelectionStateToBe(checkbox, true));
        assertTrue(checkbox.isSelected());
    }

    public boolean retryingFindClick(WebElement element) {
        boolean result = false;
        int attempts = 0;
        while(attempts < 10) {
            try {
                element.click();
                result = true;
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
        return result;
    }
}
