package pageobjects.ph;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class ArticlePage {

    @FindAll({
            @FindBy(how = How.XPATH, using = "//div[@class='col-sm-8 col-md-9 left-side']"),
            @FindBy(how = How.ID, using = "articles-list-section")
    })
    public WebElement listContainer;

    @FindBy(how = How.ID, using = "email-desktop")
    public WebElement emailInputForSubscription;

    @FindBy(how = How.ID, using = "sub-widget-button-desktop")
    public WebElement emailSubscriptionBtn;

    @FindAll({
            @FindBy(how = How.XPATH, using = "//div[@class='col-sm-8 col-md-9 left-side']/child::*"),
            @FindBy(how = How.XPATH, using = "//*[@id='articles-list-section']/child::*")
    })
    public List<WebElement> listData;

    public WebElement getArticleLink(WebElement data) {
        if (data.findElements(By.xpath(".//div[@class = 'article-card']")).size() != 0)
            return data.findElement(By.xpath(".//div[@class = 'article-card']/a"));
        else
            return data.findElement(By.xpath(".//div[@class = 'media-left img-articles']/a"));
    }

    public WebElement getArticleImage(WebElement data) {
        if (data.findElements(By.xpath(".//div[@class = 'article-card']")).size() != 0)
            return data.findElement(By.xpath(".//div[@class = 'article-card']/a/img"));
        else
            return data.findElement(By.xpath(".//div[@class = 'media-left img-articles']//img"));
    }

    public WebElement getArticleTitle(WebElement data) {
        if (data.findElements(By.xpath(".//div[@class = 'article-card']")).size() != 0)
            return data.findElement(By.xpath(".//div[@class = 'article-card']//div[@class = 'article-details']/h3"));
        else
            return data.findElement(By.xpath(".//div[@class = 'media-body']/h3[@class = 'media-heading']"));
    }

    public WebElement getArticleTitleAsLink(WebElement data) {
        if (data.findElements(By.xpath(".//div[@class = 'article-card']")).size() != 0)
            return data.findElement(By.xpath(".//div[@class = 'article-card']//div[@class = 'article-details']/h3/a"));
        else
            return data.findElement(By.xpath(".//div[@class = 'media-body']/h3[@class = 'media-heading']/a"));
    }
}
