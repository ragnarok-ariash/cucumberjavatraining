package pageobjects.ph;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class GDPForm {
    @FindBy(how = How.XPATH, using = "//div[@class='modal-dialog modal-lg']/div[@class='modal-content']")
    public WebElement formContainer;

    @FindBy(how = How.XPATH, using = "//div[@class='modal-dialog modal-lg']/div[@class='modal-content']")
    public WebElement formContainerMobile;

    @FindBy(how = How.XPATH, using = "//div[@class='modal-dialog modal-lg']/div[@class='modal-content']/descendant::p[.='Purchase Info']")
    public WebElement formMobilePurchaseInfoLabel;

    @FindBy(how = How.XPATH, using = "//div[@id='step1-form-pi']")
    public WebElement step1Form;

    @FindBy(how = How.XPATH, using = "//div[@id='step2-form-pi']")
    public WebElement step2Form;

    @FindBy(how = How.XPATH, using = "//div[@id='step3-form-pi']")
    public WebElement step3Form;

    @FindBy(how = How.XPATH, using = "//input[@id='date_of_purchase']")
    public WebElement purchaseDateInput;

    @FindBy(how = How.XPATH, using = "//div[@id='ui-datepicker-div']")
    public WebElement datepickerUI;

    @FindBy(how = How.XPATH, using = "//table[@class='ui-datepicker-calendar']")
    public WebElement datepickerUIMobile;

    @FindBy(how = How.XPATH, using = "//div[@id='is_local_switch']")
    public WebElement localCheckSwitch;

    @FindBy(how = How.XPATH, using = "//div[@id='is_local_switch']//label[@data-selector-type = 'Yes']")
    public WebElement localCheckSwitchYes;

    @FindBy(how = How.XPATH, using = "//div[@id='is_local_switch']//label[@data-selector-type = 'No']")
    public WebElement localCheckSwitchNo;

    @FindBy(how = How.XPATH, using = "//button[@id='nextBtn']")
    public WebElement nextBtn;

    @FindBy(how = How.XPATH, using = "//button[@id='nextBtn']")
    public WebElement submitBtn;

    @FindBy(how = How.ID, using = "device_brand_input_label")
    public WebElement deviceBrandDropdown;

    @FindBy(how = How.ID, using = "device_model_input_label")
    public WebElement deviceModelDropdown;

    @FindBy(how = How.ID, using = "purchase_amount_input_label")
    public WebElement purchasePriceInput;

    @FindBy(how = How.ID, using = "first_name_input_label")
    public WebElement firstNameInput;

    @FindBy(how = How.ID, using = "last_name_input_label")
    public WebElement lastNameInput;

    @FindBy(how = How.ID, using = "mobile_number-gc")
    public WebElement mobileNumberInput;

    @FindBy(how = How.ID, using = "email_input_label")
    public WebElement emailInput;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'mmx-pi-desktop')]//section[@class='phone-insurance-result']/child::section[contains(@class,'visible-lg')]/descendant::input[@name='imei_input_label']")
    public WebElement emailInputOTP;

    @FindBy(how = How.XPATH, using = "//div[@class='mmx-pi-mobile visible-xs']//section[@class='phone_result row visible-xs']//input[@name='imei_input_label']")
    public WebElement emailInputOTPMobile;

    @FindBy(how = How.XPATH, using = "//div[@class='mmx-pi-desktop hidden-md hidden-xs hidden-sm']//section[@class='phone_result row visible-lg']//table[@class='table_features']")
    public WebElement featuresTable;

    @FindBy(how = How.XPATH, using = "//div[@class='mmx-pi-mobile visible-xs']//table[@class='features_mobile_table']")
    public WebElement featuresTableMobile;

}
