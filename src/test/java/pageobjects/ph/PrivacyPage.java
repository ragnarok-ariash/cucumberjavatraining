package pageobjects.ph;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PrivacyPage {
    @FindBy(how = How.LINK_TEXT, using = "How it Works")
    public WebElement howItWorksMenu;

    @FindBy(how = How.LINK_TEXT, using = "Frequently Asked Questions")
    public WebElement faqMenu;

    @FindBy(how = How.LINK_TEXT, using = "Related Articles")
    public WebElement relatedArticleMenu;

    @FindBy(how = How.LINK_TEXT, using = "MAPFRE Insular")
    public WebElement mafreInsularMenu;

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Mercantile Car Insurance"),
            @FindBy(how = How.LINK_TEXT, using = "Mercantile Insurance")
    })
    public WebElement mercantileCIMenu;
}
