package pageobjects.ph;

import enums.DisplayType;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedCondition;
import pageobjects.HomePageView;
import stepdefinitions.base.BaseSteps;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.SeleniumUtil;

@Slf4j
public class PHHomePage extends HomePageView {
    @FindAll({
            @FindBy(how = How.XPATH, using = "//div[@class = 'btn btn-success btn-full' and contains(text(), 'Car Insurance')]"),
            @FindBy(how = How.XPATH, using = "//a[@class = 'btn btn-success btn-full' and contains(text(), 'Car Insurance')]")
    })
    @CacheLookup
    public WebElement CICTABtn;

    @FindAll({
            @FindBy(how = How.XPATH, using = "//div[@class = 'btn btn-success btn-full' and contains(text(), 'Credit Cards')]"),
            @FindBy(how = How.XPATH, using = "//a[@class = 'btn btn-success btn-full' and contains(text(), 'Credit Cards')]")
    })
    @CacheLookup
    public WebElement CCCTABtn;

    @FindAll({
            @FindBy(how = How.XPATH, using = "//div[@class = 'btn btn-success btn-full' and contains(text(), 'Personal Loans')]"),
            @FindBy(how = How.XPATH, using = "//a[@class = 'btn btn-success btn-full' and contains(text(), 'Personal Loans')]")
    })
    @CacheLookup
    public WebElement PLCTABtn;

    @FindAll({
            @FindBy(how = How.XPATH, using = "//div[@class = 'btn btn-success btn-full' and contains(text(), 'Gadget Protect')]"),
            @FindBy(how = How.XPATH, using = "//a[@class = 'btn btn-success btn-full' and contains(text(), 'Gadget Protect')]")
    })
    @CacheLookup
    public WebElement GDPCTABtn;

    @FindBy(how = How.CLASS_NAME, using = "cookie-policy")
    public WebElement cookiePolicyWrapper;

    @FindBy(how = How.XPATH, using = "//button[@id='accept-cookies']")
    public WebElement cookieAcceptBtn;

    // Navigation bar menus
    @FindBy(how = How.XPATH, using = "//a[@class = 'dropdown-toggle' and contains(text(), 'Car Insurance')]")
    @CacheLookup
    public WebElement CINavBarMenu;
    @FindBy(how = How.XPATH, using = "//a[@class = 'dropdown-toggle' and contains(text(), 'Credit Cards')]")
    @CacheLookup
    public WebElement CCNavBarMenu;
    @FindBy(how = How.XPATH, using = "//a[@class = 'dropdown-toggle' and contains(text(), 'Loans')]")
    @CacheLookup
    public WebElement PLNavBarMenu;
    @FindBy(how = How.XPATH, using = "//a[@class = 'dropdown-toggle' and contains(text(), 'Gadget Protect')]")
    @CacheLookup
    public WebElement GDPNavBarMenu;
    @FindBy(how = How.XPATH, using = "//a[@class = 'dropdown-toggle' and contains(text(), 'Articles')]")
    @CacheLookup
    public WebElement articlesNavBarMenu;
    @FindBy(how = How.XPATH, using = "//a[.='About Us']")
    @CacheLookup
    public WebElement aboutUsNavBarMenu;
    @FindBy(how = How.XPATH, using = "//a[@class = 'dropdown-toggle hidden-xs contact-icon' and contains(text(), 'TALK TO US')]")
    @CacheLookup
    public WebElement talkToUsNavBarMenu;
    @FindBy(how = How.XPATH, using = "//a[contains(.,'Contact Us')]")
    public WebElement contactUsNavBarMenu;

    @FindBy(how = How.XPATH, using = "//button[contains(@class, 'navbar-toggle')]")
    @CacheLookup
    public WebElement mobileNavBarToggle;

    @FindBy(how = How.ID, using = "navbar")
    @CacheLookup
    public WebElement mobileNavBar;

    //Navigation bar drop down menus
    @FindBy(how = How.XPATH, using = "//a[@class = 'dropdown-header' and contains(text(), 'Car Insurance')]")
    @CacheLookup
    public WebElement CIDropDownMenu;
    @FindBy(how = How.XPATH, using = "//a[@class = 'dropdown-header' and contains(text(), 'Credit Cards')]")
    @CacheLookup
    public WebElement CCDropDownMenu;
    @FindBy(how = How.XPATH, using = "//a[@class = 'dropdown-header' and contains(text(), 'Personal Loans')]")
    @CacheLookup
    public WebElement PLDropDownMenu;
    @FindBy(how = How.XPATH, using = "//a[@class = 'dropdown-header' and contains(text(), 'Gadget Protect')]")
    @CacheLookup
    public WebElement GDPDropDownMenu;
    @FindBy(how = How.XPATH, using = "//a[.='Articles']")
    @CacheLookup
    public WebElement articlesDropDownMenu;

    @FindBy(how = How.LINK_TEXT, using = "All Articles")
    public WebElement allArticlesDropDownMenu;

    //Article categories dropdown menus
    @FindBy(how = How.XPATH, using = "//li[@class='col-md-12 col-sm-12']//li[contains(.,'Car Insurance')]")
    @CacheLookup
    public WebElement carInsuranceArticleCategory;
    @FindBy(how = How.XPATH, using = "//li[@class='col-md-12 col-sm-12']//li[contains(.,'Credit Card')]")
    @CacheLookup
    public WebElement creditCardArticleCategory;
    @FindBy(how = How.XPATH, using = "//li[@class='col-md-12 col-sm-12']//li[contains(.,'Personal Loan')]")
    @CacheLookup
    public WebElement personalLoanArticleCategory;
    @FindBy(how = How.XPATH, using = "//li[@class='col-md-12 col-sm-12']//li[contains(.,'Gadget Protect')]")
    @CacheLookup
    public WebElement gadgetProtectArticleCategory;
    @FindBy(how = How.XPATH, using = "//li[@class='col-md-12 col-sm-12']//li[contains(.,'Personal Finance')]")
    @CacheLookup
    public WebElement personalFinanceArticleCategory;
    @FindBy(how = How.XPATH, using = "//li[@class='col-md-12 col-sm-12']//li[contains(.,'Lifestyle')]")
    @CacheLookup
    public WebElement lifestyleArticleCategory;
    @FindBy(how = How.XPATH, using = "//li[@class='col-md-12 col-sm-12']//li[contains(.,'Government Services')]")
    @CacheLookup
    public WebElement governmentArticleCategory;

    //Newsletter subscription section
    @FindBy(how = How.XPATH, using = "//div[@id='subscribeWidgetContent']")
    @CacheLookup
    public WebElement newsletterSection;
    @FindBy(how = How.XPATH, using = "//input[@id='email-desktop']")
    @CacheLookup
    public WebElement emailInputField;
    @FindBy(how = How.XPATH, using = "//button[@id='sub-widget-button-desktop']")
    @CacheLookup
    public WebElement subscribeButton;

    @FindAll({
            @FindBy(how = How.XPATH, using = "//p[.='Thank you for subscribing to the moneymax newsletter']"),
            @FindBy(how = How.CLASS_NAME, using = "subscription-success")
    })
    @CacheLookup
    public WebElement newsLetterSuccessfulMessage;

    //Footer section
    @FindBy(how = How.XPATH, using = "//div[@class='row visible-xs']//a[contains(.,'More')]")
    @CacheLookup
    public WebElement mobileFooterMoreMenu;
    @FindBy(how = How.XPATH, using = "//div[@class='row visible-lg']//a[contains(.,'Privacy Policy')]")
    public WebElement privacyPolicyMenu;
    @FindBy(how = How.XPATH, using = "//div[@class='row visible-lg']//a[contains(.,'Terms & Conditions')]")
    public WebElement termConditionMenu;
    @FindBy(how = How.XPATH, using = "//div[@class='collapse in']//a[contains(.,'Privacy Policy')]")
    public WebElement privacyPolicyMenuMobile;
    @FindBy(how = How.XPATH, using = "//div[@class='collapse in']//a[contains(.,'Terms & Conditions')]")
    public WebElement termConditionMenuMobile;
    @FindBy(how = How.XPATH, using = "//div[@class='collapse in']//a[contains(.,'Government Services Articles')]")
    public WebElement govServiceArticleMenuMobile;


    @Override
    public void clickOnCtaBtn(BaseSteps.CTAType ctaType, WebDriver driver) {
        WebElement ctabtn = null;
        switch (ctaType) {
            case CI_CTA:
                ctabtn = CICTABtn;
                break;
            case CC_CTA:
                ctabtn = CCCTABtn;
                break;
            case PL_CTA:
                ctabtn = PLCTABtn;
                break;
            case GDP_CTA:
                ctabtn = GDPCTABtn;
                break;
        }
        clickCtaBtn(ctabtn, driver);
    }

    //Hover on navigation bar menus
    public void hoverNavBarMenu(String NavBarMenu, WebDriver driver) throws InterruptedException {
        if (globalManager.display == DisplayType.MOBILE) {
            openMobileNavbar();
        }
        String navBarMenuDropDownAreaXpath = null;
        WebElement navbarElement = null;
        Actions actions = new Actions(driver);
        switch (NavBarMenu) {
            case "CI":
                navbarElement = CINavBarMenu;
                navBarMenuDropDownAreaXpath = "//a[. = 'Car Insurance']/following-sibling::ul";
                actions.moveToElement(CINavBarMenu);
                break;
            case "CC":
                navbarElement = CCNavBarMenu;
                navBarMenuDropDownAreaXpath = "//a[. = 'Credit Cards']/following-sibling::ul";
                actions.moveToElement(CCNavBarMenu);
                break;
            case "PL":
                navbarElement = PLNavBarMenu;
                navBarMenuDropDownAreaXpath = "//a[. = 'Loans']/following-sibling::ul";
                actions.moveToElement(PLNavBarMenu);
                break;
            case "GDP":
                navbarElement = GDPNavBarMenu;
                navBarMenuDropDownAreaXpath = "//a[. = 'Gadget Protect']/following-sibling::ul";
                actions.moveToElement(GDPNavBarMenu);
                break;
            case "articles":
                navbarElement = articlesDropDownMenu;
                navBarMenuDropDownAreaXpath = "//a[. = 'Articles']/following-sibling::ul";
                actions.moveToElement(articlesDropDownMenu);
                break;
            case "talkToUs":
                actions.moveToElement(globalManager.display == DisplayType.DESKTOP ? talkToUsNavBarMenu : contactUsNavBarMenu);
                break;
        }
        if (globalManager.display == DisplayType.DESKTOP) {
            actions.perform();
            if (navBarMenuDropDownAreaXpath != null)
                seleniumUtil.waitUntilElementVisible(By.xpath(navBarMenuDropDownAreaXpath));
        } else {
            actions.click();
            actions.perform();
            if (navbarElement != null)
                seleniumUtil.waitUntilAttributeElementTobe(navbarElement, "aria-expanded", "true");
        }
    }

    public boolean isCookiePolicyDisplayed(WebDriver driver) {
        if (seleniumUtil.elementExist(By.className("cookie-policy")))
            return cookiePolicyWrapper.isDisplayed();
        else
            return false;
    }

    public void openMobileNavbar() {
        if (globalManager.display == DisplayType.MOBILE) {
            seleniumUtil.clickElement(mobileNavBarToggle);
            seleniumUtil.waitFor(new ExpectedCondition<Boolean>() {
                @NullableDecl
                @Override
                public Boolean apply(@NullableDecl WebDriver driver) {
                    //contactUsNavBarMenu is last child elements of mobileNavBar
                    String script = "var mobileNavBarBottom = arguments[0].getBoundingClientRect().bottom;"
                            + "var contactUsNavBarMenuBottom = arguments[1].getBoundingClientRect().bottom;"
                            + "return mobileNavBarBottom == contactUsNavBarMenuBottom;";

                    return (Boolean) ((JavascriptExecutor) seleniumUtil.getDriver()).executeScript(script, mobileNavBar, contactUsNavBarMenu);//mobileNavBar.getAttribute("aria-expanded").equals("true");
                }
            });
        }
    }

    public void openMobileFooterMoreMenu(SeleniumUtil seleniumUtil) {
        seleniumUtil.scrollElementToBelowElement(mobileFooterMoreMenu,
                seleniumUtil.getDriver().findElement(By.className("navbar-header")));
        seleniumUtil.clickElement(mobileFooterMoreMenu);
        seleniumUtil.waitUntilAttributeElementContains(mobileFooterMoreMenu, "aria-expanded", "true");
    }
}
