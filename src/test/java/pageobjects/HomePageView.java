package pageobjects;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import stepdefinitions.base.BaseSteps;
import utils.SeleniumUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertNotNull;

public abstract class HomePageView {
    protected SeleniumUtil seleniumUtil;
    protected GlobalManager globalManager;

    public void setSeleniumUtil(SeleniumUtil seleniumUtil) {
        this.seleniumUtil = seleniumUtil;
    }

    public void setGlobalManager(GlobalManager globalManager) {
        this.globalManager = globalManager;
    }

    public abstract void clickOnCtaBtn(BaseSteps.CTAType ctaType, WebDriver driver);

    //general implementation of click CTA button
    protected void clickCtaBtn(WebElement ctaBtn, WebDriver driver) {
        assertNotNull(ctaBtn);
        seleniumUtil.waitUntilElementVisible(ctaBtn);

        if (globalManager.display == DisplayType.MOBILE) {
//            String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
//                    + "var elementTop = arguments[0].getBoundingClientRect().top;"
//                    + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
//            JavascriptExecutor jse2 = (JavascriptExecutor)driver;
//            jse2.executeScript(scrollElementIntoMiddle, ctaBtn);
            seleniumUtil.scrollElementToBelowElement(ctaBtn,
                    globalManager.driver.findElement(By.className("navbar-header")));
        }
        seleniumUtil.clickElement(ctaBtn);
    }
}
