package pageobjects;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class PageData {
    public String productRedirectUrl;
    public String firstUrlSegmentWhenSubmitPI;
    public String productPageUrl;
    public String partnerDataId;
    public int numberOfWindowWhenSubmitPI;
    private Map<String, String> customData;

    public boolean redirectToPartnerProduct() {
        if (productRedirectUrl == null || productRedirectUrl.equalsIgnoreCase("null"))
            return false;
        else
            return true;
    }

    public Map<String, String> getCustomData() {
        if (customData == null)
            customData = new HashMap<>();

        return customData;
    }

    public void init() {
        productRedirectUrl = null;
        firstUrlSegmentWhenSubmitPI = null;
        productPageUrl = null;
        partnerDataId = null;
        numberOfWindowWhenSubmitPI = 0;
        customData = new HashMap<>();
    }

    public void setCustomData(String key, String value) {
        getCustomData().put(key, value);
    }

    public String getCustomData(String key) {
        return getCustomData().get(key);
    }
}
