package pageobjects;

import utils.SeleniumUtil;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class THPIForm {
    @FindBy(how = How.XPATH, using = "//div[@id = 'personal-info']")
    public WebElement formContainer;

    @FindBy(how = How.XPATH, using = "//div[@id = 'personal-info']//div[@class = 'modal-body']")
    public WebElement formBody;

    @FindAll({
            @FindBy(how = How.ID, using = "first-name-ci"),
            @FindBy(how = How.ID, using = "first-name-cc"),
            @FindBy(how = How.ID, using = "first-name-loan")
    })
    public WebElement firstNameInput;

    @FindAll({
            @FindBy(how = How.ID, using = "last-name-ci"),
            @FindBy(how = How.ID, using = "last-name-cc"),
            @FindBy(how = How.ID, using = "last-name-loan")
    })
    public WebElement lastNameInput;

    @FindAll({
            @FindBy(how = How.ID, using = "email-ci"),
            @FindBy(how = How.ID, using = "email-cc"),
            @FindBy(how = How.ID, using = "email-loan")
    })
    public WebElement emailInput;

    @FindAll({
            @FindBy(how = How.ID, using = "phone-ci"),
            @FindBy(how = How.ID, using = "phone-cc"),
            @FindBy(how = How.ID, using = "phone-loan")
    })
    public WebElement phoneInput;

    @FindAll({
            @FindBy(how = How.CSS, using = ".btn-show-result"),
            @FindBy(how = How.CSS, using = ".btn-show-result-cc"),
            @FindBy(how = How.CSS, using = ".btn-show-result-loan")
    })
    public WebElement submitBtn;

    @FindAll({
            @FindBy(how = How.ID, using = "line-id-ci"),
            @FindBy(how = How.ID, using = "range-income"),
            @FindBy(how = How.ID, using = "line-id-loan")
    })
    public WebElement lineIDorRangeIncomeInput;

    public boolean isFormDisplayed() {
        return formBody.isDisplayed();
    }

    public void setFirstName(String firstName, WebDriver driver) {
        retryingFindClick(firstNameInput);
        firstNameInput.clear();
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].value='"+firstName+"';", firstNameInput);
//        firstNameInput.sendKeys(firstName);
    }

    public void setLastName(String lastName, WebDriver driver) {
        retryingFindClick(lastNameInput);
        lastNameInput.clear();
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].value='"+lastName+"';", lastNameInput);
//        lastNameInput.sendKeys(lastName);
    }

    public void setEmailAddress(String emailAddress, WebDriver driver) {
        retryingFindClick(emailInput);
        emailInput.clear();
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].value='"+emailAddress+"';", emailInput);
//        emailInput.sendKeys(emailAddress);
    }

    public void setPhoneNumber(String phoneNumber, WebDriver driver) throws InterruptedException {
        retryingFindClick(phoneInput);
        phoneInput.clear();
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].value='"+phoneNumber+"';", phoneInput);
//        phoneInput.sendKeys(phoneNumber);
//        while (!phoneInput.getAttribute("value").equals(phoneNumber)){
//            phoneInput.clear();
//            phoneInput.sendKeys(phoneNumber);
//        }
    }

    public void setLineIDorRangeIncome(String lineIDorRangeIncome) {
        this.lineIDorRangeIncomeInput.clear();
        this.lineIDorRangeIncomeInput.sendKeys(lineIDorRangeIncome);
    }

    public static void checkAgreement(WebDriver driver) {
        List<WebElement> checkboxes = driver.findElements(By.cssSelector("input[name='agree-term-cond']"));
        checkboxes.stream()
                .filter(elem -> !elem.isSelected())
                .forEach(elem -> elem.findElement(By.xpath("following-sibling::span[@class='checkmark']")).click());
    }
    public static void checkAgreementPL(WebDriver driver) {
        List<WebElement> checkboxes = driver.findElements(By.cssSelector(".checkmark-checkbox-loan"));
        checkboxes.stream()
                .filter(elem -> !elem.isSelected())
                .forEach(elem -> elem.findElement(By.xpath("//span[@class='checkmark']")).click());
    }
    public THPIForm(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public boolean retryingFindClick(WebElement element) {
        boolean result = false;
        int attempts = 0;
        while(attempts < 10) {
            try {
                element.click();
                result = true;
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
        return result;
    }

    public void setElement(WebElement element, String elementVal, SeleniumUtil seleniumUtil) {
        seleniumUtil.waitFor(ExpectedConditions.elementToBeClickable(element));
        retryingFindClick(element);
        element.clear();
        JavascriptExecutor jse = (JavascriptExecutor)seleniumUtil.getDriver();
        jse.executeScript("arguments[0].value='"+elementVal+"';", element);
    }
}
