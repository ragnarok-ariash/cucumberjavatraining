package pageobjects;

import stepdefinitions.base.BaseSteps;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class MYHomePage extends HomePageView {
    @FindBy(how = How.XPATH, using = "//div[@class='col-xs-12 col-md-6 three-box']//div[@class='col-sm-4']/div[contains(.,'Credit Cards')]")
    @CacheLookup
    public WebElement CCCTABtn;

    @FindBy(how = How.XPATH, using = "//div[@class='col-xs-12 col-md-6 three-box']//div[@class='col-sm-4']/div[contains(.,'Personal Loans')]")
    @CacheLookup
    public WebElement PLCTABtn;

    @FindBy(how = How.XPATH, using = "//div[@class='col-xs-12 col-md-6 three-box']//div[@class='col-sm-4']/div[contains(.,'Travel Insurance')]")
    @CacheLookup
    public WebElement TICTABtn;

    @FindBy(how = How.XPATH, using = "//a[.='Our Partners:']")
    @CacheLookup
    public WebElement PartnerCTABtn;

    @FindBy(how = How.CLASS_NAME, using = "popup-overlay")
    public WebElement scammerWarningPopup;

    @FindBy(how = How.ID, using = "kv_form_close")
    public WebElement scammerPopupCloseBtn;

    @FindBy(how = How.XPATH, using = "//button[@class='_hj-2qaGY__styles__openStateToggle']")
    public WebElement surveyOnlyPopupCloseBtn;

    @FindBy(how = How.XPATH, using = "//button[@class='_hj-2qaGY__styles__openStateToggle']")
    public List<WebElement> surveyOnlyPopup;



    @Override
    public void clickOnCtaBtn(BaseSteps.CTAType ctaType, WebDriver driver) {
        WebElement ctabtn = null;
        switch (ctaType) {
            case CC_CTA:
                ctabtn = CCCTABtn;
                break;
            case PL_CTA:
                ctabtn = PLCTABtn;
                break;
            case TI_CTA:
                ctabtn = TICTABtn;
                break;
            case Partner_CTA:
                ctabtn = PartnerCTABtn;
                break;
        }
        clickCtaBtn(ctabtn, driver);
    }

    public boolean isScammerWarningPopupDisplayed() {
        return scammerWarningPopup.isDisplayed();
    }

    public void closeScammerPopup() {
        scammerPopupCloseBtn.click();
    }
    public void closeSurveyOnly() {
        if (surveyOnlyPopup.size() != 0) {
            seleniumUtil.waitUntilElementClickable(surveyOnlyPopupCloseBtn);
            surveyOnlyPopupCloseBtn.click();
        }
    }
}
