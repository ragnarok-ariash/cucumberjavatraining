package pageobjects.th;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pageobjects.AbstractPageFactory;
import utils.SeleniumUtil;

import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;
import static org.junit.Assert.assertNotEquals;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class CCResultPage extends AbstractPageFactory {

    @FindBy(how = How.ID, using = "list-data")
    public WebElement listContainer;

    @FindBy(how = How.XPATH, using = "//div[@id='list-data']/child::*")
    public List<WebElement> listItems;

    @Autowired
    private SeleniumUtil seleniumUtil;

    public CCResultPage(GlobalManager globalManager) {
        super(globalManager);
    }

    private WebElement getDataFromListItem(int index) {
        seleniumUtil.waitFor(ExpectedConditions.presenceOfNestedElementLocatedBy(listContainer, By.tagName("div")));
        //verify is there any data to select
        assertNotEquals(0, listItems.size());
        return listItems.get(index);
    }

    private WebElement getApplyBtnOutOfThisItem(WebElement dataItem) {
        return dataItem.findElement(By.xpath(".//div[@class='col-sm-3 text-center card-box']/a[.='สมัครเลย']"));
    }

    private WebElement getTitleOutOfThisItem(WebElement dataItem) {
        return dataItem.findElement(By.xpath(globalManager.display == DisplayType.DESKTOP ?
                ".//div[@class='hidden-xs hidden-sm']/a[@data-trackingtabname = 'Product Name']" :
                ".//h4[@class = 'media-heading']/b/a"));
    }

    private WebElement getImageOutOfThisItem(WebElement dataItem) {
        return dataItem.findElement(By.xpath(globalManager.display == DisplayType.DESKTOP ?
                ".//div[@class='col-sm-3 text-center card-box']//img" :
                ".//div[@class='row']//div[contains(@class,'col-xs-4')]//img"));
    }

    public void applyOnThisItem(WebElement itemToApply) {
        seleniumUtil.clickElement(getApplyBtnOutOfThisItem(itemToApply));
    }

    public WebElement getAnyProduct() {
        return getDataFromListItem(0);
    }

    public WebElement getAnyProductName() {
        return getTitleOutOfThisItem(getAnyProduct());
    }

    public WebElement getAnyProductImage() {
        return getImageOutOfThisItem(getAnyProduct());
    }

}
