package pageobjects.th;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pageobjects.AbstractPageFactory;
import utils.SeleniumUtil;

import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class CCPartnerPage extends AbstractPageFactory {
    private static final String LIST_ITEM_XPATH = "//div[@class='credit-card-provider right-content credit-card-list-by-provider']";
    @FindBy(how = How.XPATH, using = LIST_ITEM_XPATH)
    private WebElement listContainer;

    @FindBy(how = How.XPATH, using = LIST_ITEM_XPATH+"/div")
    private List<WebElement> listItems;

    @FindBy(how = How.XPATH, using = "//button[@class='btn btn-success btn-main btn-lg btn-ppage-apply btn-apply-cc']")
    private WebElement applyBtn;

    @FindBy(how = How.XPATH, using = "//div[@id='personal-info']")
    private WebElement personalInfoView;

    @FindBy(how = How.XPATH, using = "//div[@class='col-lg-offset-3 col-lg-3 col-md-12 col-xs-12 image_column']/div[@class='product_image']")
    public WebElement productImage;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'collapsible-nav')]")
    public WebElement stickyNavBar;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'collapsible-nav')]//li/a[@data-selector-id = 'fixed-nav-prodsummary']")
    public WebElement stickyNavBarCCInfo;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'collapsible-nav')]//li/a[@data-selector-id = 'fixed-nav-eligibility']")
    public WebElement stickyNavBarQualification;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'collapsible-nav')]//li/a[@data-selector-id = 'fixed-nav-fees']")
    public WebElement stickyNavBarFeeExpense;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'collapsible-nav')]//li/a[@data-selector-id = 'fix-nav-faqs']")
    public WebElement stickyNavBarFAQ;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'collapsible-nav')]//li[@id='sticky-img-collapsed-el']")
    public WebElement stickyNavBarCardLogo;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'collapsible-nav')]//button[@id='apply-btn-collapsed']")
    public WebElement stickyNavBarApplyBtn;

    @FindBy(how = How.XPATH, using = "//h2[@class='title font-bold-300']")
    public WebElement specialPromotionTitle;

    @FindBy(how = How.XPATH, using = "//div[@class='col-xs-12 col-lg-8']/a/img")
    public WebElement promotionBannerView;

    @FindBy(how = How.XPATH, using = "//section[@id='product_summary']")
    public WebElement productSummmaryView;

    @FindBy(how = How.XPATH, using = "//div[@id='newsletter']")
    public WebElement followUsView;

    @FindBy(how = How.XPATH, using = "//div[@id='eligibility_section']")
    public WebElement productEligibilityView;

    @FindBy(how = How.XPATH, using = "//div[@id='fees_and_charges']")
    public WebElement productFeeAndExpenseView;

    @FindBy(how = How.XPATH, using = "//section[@id='faqs_section']")
    public WebElement productFAQView;

    @FindBy(how = How.XPATH, using = "//section[@id='related_section']")
    public WebElement otherCCView;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'promo-image')]/img")
    public WebElement productLogo;

    @FindBy(how = How.XPATH, using = "//h3[@class='promo-short-desc']")
    public WebElement productTitle;

    @FindBy(how = How.XPATH, using = "//p[@class='promo-long-desc']/following-sibling::p[text()]")
    private List<WebElement> productDesc;

    @FindBy(how = How.XPATH, using = "//button[contains(@class, 'accordion') and contains(@class, 'accord-product-summary')]")
    private List<WebElement> accordionProductSummary;

    @FindBy(how = How.XPATH, using = "//div[@class='col-xl-6 col-xl-offset-3 col-lg-8 col-lg-offset-2']")
    public WebElement accordionProductSummaryView;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'image_column') and contains(@class, 'productpage')]/div[@class='product_image']//img")
    public WebElement providerLogo;

    @FindBy(how = How.XPATH, using = "//h1[@class='main-title text-primary-product-name']")
    public WebElement productDescTitle;

    @FindBy(how = How.XPATH, using = "//div[@class='gp-short-desc gp-short-desc-desktop hidden-xs hidden-sm hidden-md']")
    private WebElement productDescParagraph;

    @FindBy(how = How.XPATH, using = "//div[@class='gp-short-desc col-xs-12 hidden-lg']")
    private WebElement productDescParagraphMobile;

    @FindBy(how = How.XPATH, using = "//div[@class='annual-income']//p[@data-selector-id = 'desktop-annual-income-value']")
    public WebElement minMonthlyIncome;

    @FindBy(how = How.XPATH, using = "//div[@class='interest-rate']//p[@data-selector-id = 'desktop-interest-rate-value']")
    public WebElement interestPerYear;

    @FindBy(how = How.XPATH, using = "//div[@class='annual-fee']//p[@data-selector-id = 'desktop-annual-fee-value']")
    public WebElement annualFee;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'product-summary-details')]/div[@class='product_name']")
    public WebElement informationTitle;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'product-summary-details')]/div[@class='product_name']/following-sibling::ul")
    public WebElement informationDesc;

    @FindBy(how = How.XPATH, using = "//div[@id='eligibility_section']/div[@class='panel']//table//tr")
    public List<WebElement> eligibilityTableRows;

    @FindBy(how = How.XPATH, using = "//div[@id='fees_and_charges']/div[@class='panel']//table//tr")
    public List<WebElement> feeExpenseTableRows;

    @FindBy(how = How.XPATH, using = "//section[@id='related_section']//div[contains(@class, 'related-card')]")
    public List<WebElement> relatedCards;

    @FindBy(how = How.XPATH, using = "//input[@id='subscribe-email-input']")
    private WebElement subscribeInput;

    @FindBy(how = How.XPATH, using = "//input[@id='subscribe-email-input-mobile']")
    private WebElement subscribeInputMobile;

    @FindBy(how = How.XPATH, using = "//div[@id='mail-button']")
    private WebElement subscribeBtn;

    @FindBy(how = How.XPATH, using = "//div[@id='mail-button-mobile']")
    private WebElement subscribeBtnMobile;

    @FindBy(how = How.XPATH, using = "//div[@id='validation-popup']")
    private WebElement subscribeValidationPopup;

    @FindBy(how = How.XPATH, using = "//div[@id='validation-popup-mobile']")
    private WebElement subscribeValidationPopupMobile;

    @FindBy(how = How.XPATH, using = "//span[@id='validation-text']")
    private WebElement subscribeValidationText;

    @FindBy(how = How.XPATH, using = "//span[@id='validation-text-mobile']")
    private WebElement subscribeValidationTextMobile;

    @Autowired
    private SeleniumUtil seleniumUtil;

    public CCPartnerPage(GlobalManager globalManager) {
        super(globalManager);
    }

    private WebElement getDataFromListItem(int index) {
        seleniumUtil.waitFor(ExpectedConditions.presenceOfNestedElementLocatedBy(listContainer, By.tagName("div")));
        //verify is there any data to select
        assertNotEquals(0, listItems.size());
        return listItems.get(index);
    }

    private WebElement getImageOutOfThisItem(WebElement dataItem) {
        return dataItem.findElement(By.xpath(globalManager.display == DisplayType.DESKTOP ?
                ".//div[@class='col-sm-3 text-center card-box']//img" :
                ".//div[@class='row']//div[contains(@class,'col-xs-4')]//img"));
    }

    private WebElement getAnyProduct() {
        return getDataFromListItem(0);
    }

    public WebElement getAnyProductImage() {
        return getImageOutOfThisItem(getAnyProduct());
    }

    public WebElement getApplyBtn() {
        return applyBtn;
    }

    public WebElement getPersonalInfoView() {
        return personalInfoView;
    }

    public boolean productDescDescribed() {
        if (productDesc.size() == 0)
            return false;
        return productDesc.get(0).isDisplayed();
        /** alternate logic
        boolean desciptionDisplayed = true;
        for (WebElement _productDesc :
                productDesc) {
            desciptionDisplayed &= _productDesc.isDisplayed();
        }
        return productDesc.size() != 0 && desciptionDisplayed;
         */
    }

    public boolean productSummaryAccordionDisplayed() {
        boolean accordionDisplayed = true;
        for (WebElement _accordionProductSummary :
                accordionProductSummary) {
            accordionDisplayed &= _accordionProductSummary.isDisplayed();
        }
        System.out.println("accordionProductSummary.size(): "+accordionProductSummary.size());
        System.out.println("accordionDisplayed: "+accordionDisplayed);
        return accordionProductSummary.size() != 0 && accordionDisplayed;
    }

    public WebElement getProductDescParagraph() {
        return globalManager.display == DisplayType.DESKTOP ? productDescParagraph : productDescParagraphMobile;
    }

    public List<WebElement> getFaqs() {
        return productFAQView.findElements(By.xpath(".//div[@class='accord-container']"));
    }

    public WebElement getAnyRelatedProduct() {
        assertTrue(relatedCards.size()>0);
        return relatedCards.get(0);
    }

    public WebElement getSubscribeInput() {
        return globalManager.display == DisplayType.DESKTOP ? subscribeInput : subscribeInputMobile;
    }

    public WebElement getSubscribeBtn() {
        return globalManager.display == DisplayType.DESKTOP ? subscribeBtn : subscribeBtnMobile;
    }

    public WebElement getSubscribeValidationPopup() {
        return globalManager.display == DisplayType.DESKTOP ? subscribeValidationPopup : subscribeValidationPopupMobile;
    }

    public WebElement getSubscribeValidationText() {
        return globalManager.display == DisplayType.DESKTOP ? subscribeValidationText : subscribeValidationTextMobile;
    }
}
