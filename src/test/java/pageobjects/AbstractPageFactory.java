package pageobjects;

import globalmanagers.GlobalManager;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractPageFactory {

    protected GlobalManager globalManager;

    @Autowired
    public AbstractPageFactory(GlobalManager globalManager) {
        this.globalManager = globalManager;
        PageFactory.initElements(globalManager.driver, this);
    }
}
