package utils;

import globalmanagers.GlobalManager;
import io.cucumber.spring.ScenarioScope;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Component
@ScenarioScope
public class SeleniumUtil {
    public enum VerifyMethod {
        SOFT,
        HARD,
        IGNORETRAILING
    }

    private GlobalManager globalManager;

    @Autowired
    public SeleniumUtil(GlobalManager globalManager) {
        this.globalManager = globalManager;
    }

    public WebDriver getDriver() {
        return this.globalManager.driver;
    }

    public void waitUntilFinishRedirectToURL(String url) {
        waitFor(ExpectedConditions.urlToBe(url));
    }

    public void waitUntilFinishRedirectToURLWithOptionalTrailingSlash(String url) {
        waitFor(ExpectedConditions.urlMatches("^" + url + "\\/?$"));
    }

    public void waitUntilFinishRedirectToURLContains(String url) {
        waitFor(ExpectedConditions.urlContains(url));
    }

    public void waitUntilElementVisible(WebElement element) {
        waitFor(ExpectedConditions.visibilityOf(element));
    }

    public void waitUntilElementVisible(final By locator) {
        waitFor(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitUntilElementClickable(WebElement element) {
        waitFor(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitUntilElementClickable(final By locator) {
        waitFor(ExpectedConditions.elementToBeClickable(locator));
    }

    public void waitUntilElementGone(WebElement element) {
        waitFor(ExpectedConditions.invisibilityOf(element));
    }

    public void waitUntilElementGone(final By locator) {
        waitFor(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitUntilAttributeElementTobe(WebElement element, String attribute, String valueToBe) {
        waitFor(ExpectedConditions.attributeToBe(element, attribute, valueToBe));
    }

    public void waitUntilAttributeElementTobe(By locator, String attribute, String valueToBe) {
        waitFor(ExpectedConditions.attributeToBe(locator, attribute, valueToBe));
    }

    public void waitUntilAttributeElementContains(WebElement element, String attribute, String valueToContain) {
        waitFor(ExpectedConditions.attributeContains(element, attribute, valueToContain));
    }

    public void waitUntilAttributeElementContains(By locator, String attribute, String valueToContain) {
        waitFor(ExpectedConditions.attributeContains(locator, attribute, valueToContain));
    }

    public void waitUntilChildElementVisible(WebElement parentElement, By childLocator) {
        waitFor(ExpectedConditions.visibilityOfNestedElementsLocatedBy(parentElement, childLocator));
    }

    public void waitUntilTextPresentInElement(WebElement element, String expectedTextToPresent) {
        waitFor(ExpectedConditions.textToBePresentInElementValue(element, expectedTextToPresent));
    }

    public void waitFor(ExpectedCondition<?> waitCondition) {
        WebDriverWait wait = new WebDriverWait(getDriver(), globalManager.timeout);
        wait.until(waitCondition);
    }

    public void verifyPageUrl(String url, VerifyMethod verifyMethod) {
        switch (verifyMethod) {
            case HARD:
                waitUntilFinishRedirectToURL(url);
                assertURL(url);
                break;
            case SOFT:
                waitUntilFinishRedirectToURLContains(url);
                assertTrue(getDriver().getCurrentUrl().contains(url));
                break;
            case IGNORETRAILING:
                waitUntilFinishRedirectToURLContains(url);
                assertURLWithOptionalTrailingSlash(url);
                break;
        }
    }

    public void clickElement(WebElement element) {
        waitUntilElementVisible(element);
        waitUntilElementClickable(element);
        try {
            element.click();
        } catch (ElementClickInterceptedException e) {
            clickElementUsingScript(element);
        }

    }

    public void clickElementByLocator(By locator) {
        waitUntilElementVisible(locator);
        waitUntilElementClickable(locator);
        getDriver().findElement(locator).click();
//        Actions actions = new Actions(driver);
//        actions.moveToElement(element).click().build().perform();
    }

    public void clickElementByLocatorUsingScript(By locator) {
        waitUntilElementVisible(locator);
        waitUntilElementClickable(locator);
        clickUsingScript(getDriver().findElement(locator));
    }

    public void clickElementUsingScript(WebElement element) {
        waitUntilElementVisible(element);
        waitUntilElementClickable(element);
        clickUsingScript(element);
    }

    private void clickUsingScript(WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", element);
    }

    public void checkCheckBoxElement(By cbLocator, boolean check) {
        waitUntilElementVisible(cbLocator);
        WebElement checkbox = getDriver().findElement(cbLocator);
        if (checkbox.isSelected() != check)
            checkbox.click(); //this is to make checkbox state (check/unchecked) equal to @param check
    }

    public void enterInputWithTextByLocator(String text, By locator, boolean usingJavascript, boolean ensureElementTextEqual) {
        enterInputWithTextByElement(text, getDriver().findElement(locator), usingJavascript, ensureElementTextEqual);
    }

    public void enterInputWithTextByElement(String text, WebElement element, boolean usingJavascript, boolean ensureElementTextEqual) {
        waitUntilElementVisible(element);
        waitUntilElementClickable(element);
        element.click();
        String value = element.getAttribute("value");
        if (!value.isEmpty()) {
            System.out.println("input has existing value: " + value);
            element.clear();
            value = element.getAttribute("value");
            System.out.println("input value after clear command: " + value);
        }

        if (usingJavascript) {
            JavascriptExecutor executor = (JavascriptExecutor) getDriver();
            executor.executeScript("arguments[0].value='" + text + "'", element);
        } else
            element.sendKeys(text);
        if (ensureElementTextEqual)
            waitUntilTextPresentInElement(element, text);
    }

    public void selectDropdownWithValueByLocator(String text, By locator, int numOfElementMoreThan) {
        waitUntilElementVisible(locator);
        selectDropdownWithValueByElement(text, getDriver().findElement(locator), numOfElementMoreThan);
    }

    public void selectDropdownWithValueByElement(String text, WebElement element, int numOfElementMoreThan) {
        waitUntilElementVisible(element);
        waitUntilElementClickable(element);
        Select aDropdown = new Select(element);
        //wait until dropdownvalue get populated
        waitFor((ExpectedCondition<Boolean>) webDriver -> aDropdown.getOptions().size() > numOfElementMoreThan);

        aDropdown.selectByVisibleText(text);
    }

    public void scrollIntoElement(WebElement element) {
        scrollIntoElement(element, true);
    }

    public void scrollIntoElement(WebElement element, boolean asAWhole) {
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
        waitUntilElementAppearOnViewPort(element, asAWhole);
    }

    public void scrollElementToMiddle(WebElement element) {
        String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
        JavascriptExecutor jse2 = (JavascriptExecutor) getDriver();
        jse2.executeScript(scrollElementIntoMiddle, element);
    }

    public void scrollElementToBelowElement(WebElement element, WebElement anchor) {
        String scrollElementToBelowAnchor = "var headerBottom = arguments[0].getBoundingClientRect().bottom;"
                + "var elementTop = arguments[1].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-headerBottom);";

        JavascriptExecutor jse2 = (JavascriptExecutor) getDriver();
        jse2.executeScript(scrollElementToBelowAnchor, anchor, element);
    }

    public void scrollElementWithinScrollViewIntoView(WebElement element) {
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("arguments[0].scrollIntoView({behavior: 'smooth', block: 'nearest'})", element);
    }

    /**
     * @param element1 is upper bound
     * @param element2 is lower bound
     * @param yOffset  is distance between element1 and element2 to be considered as close
     */
    public void waitUntilElementsGetCloseWithSpecificYOffset(WebElement element1, WebElement element2, int yOffset) {
        waitFor(new ExpectedCondition<Boolean>() {

            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                String script = "var headerBottom = arguments[0].getBoundingClientRect().bottom;"
                        + "var elementY = arguments[1].getBoundingClientRect().y;"
                        + "return elementY - headerBottom < " + yOffset + ";";

                return (Boolean) ((JavascriptExecutor) getDriver()).executeScript(script, element1, element2);
            }
        });
    }

    public void waitUntilElementsGetScrolledToMiddle(WebElement element) {
        final int toleration = 20;
        waitFor(new ExpectedCondition<Boolean>() {

            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                String script = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                        + "var elementY = arguments[0].getBoundingClientRect().y;"
                        + "return elementY <= (viewPortHeight/2)+" + toleration + ";";

                return (Boolean) ((JavascriptExecutor) getDriver()).executeScript(script, element);
            }
        });
    }

    public void waitUntilElementAppearOnViewPort(WebElement element) {
        waitUntilElementAppearOnViewPort(element, true);
    }

    public void waitUntilElementAppearOnViewPort(WebElement element, boolean asAWhole) {
        waitFor(new ExpectedCondition<Boolean>() {

            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                return isElementVisibleOnViewPort(element, asAWhole);
            }
        });
    }

    public boolean isElementVisibleOnViewPort(WebElement element) {
        return isElementVisibleOnViewPort(element, true);
    }

    public boolean isElementVisibleOnViewPort(WebElement element, boolean asAWhole) {
        /**
         + "console.log('rect.top: '+ rect.top); "
         + "console.log('rect.left: '+ rect.left); "
         + "console.log('rect.bottom: '+ rect.bottom); "
         + "console.log('rect.right: '+ rect.right);"
         + "console.log('window.innerHeight: '+ window.innerHeight);"
         + "console.log('window.innerWidth: '+ window.innerWidth);"
         + "console.log('document.documentElement.clientHeight: '+ document.documentElement.clientHeight);"
         + "console.log('document.documentElement.clientWidth: '+ document.documentElement.clientWidth);";
         */
        String asAWholeScript = "var rect = arguments[0].getBoundingClientRect(); "
                + "return (" +
                "        rect.top >= 0 &&" +
                "        rect.left >= 0 &&" +
                "        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && " + /* or $(window).height() */
                "        rect.right <= (window.innerWidth || document.documentElement.clientWidth) " + /* or $(window).width() */
                "  );";

        String partialScript = "var rect = arguments[0].getBoundingClientRect(); console.log('rect.top: '+ rect.top);"
                + "return (" +
                "        (rect.top >= 0 &&" +
                "        rect.left >= 0 ) || " +
                "        (rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && " + /* or $(window).height() */
                "        rect.right <= (window.innerWidth || document.documentElement.clientWidth)) " + /* or $(window).width() */
                "  );";

        return (Boolean) ((JavascriptExecutor) getDriver()).executeScript(asAWhole ? asAWholeScript : partialScript, element);
    }

    public void waitForJQueryToBeInactive() {
        Boolean isJqueryUsed = (Boolean) ((JavascriptExecutor) getDriver())
                .executeScript("return (typeof(jQuery) != 'undefined')");
        if (!isJqueryUsed)
            return;

        waitFor(new ExpectedCondition<Boolean>() {

            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                Boolean ajaxIsComplete = (Boolean) (((JavascriptExecutor) getDriver())
                        .executeScript("return jQuery.active == 0"));

                return ajaxIsComplete;
            }
        });

    }

    public void assertURL(String url) {
        System.out.println("asserting current url: " + getDriver().getCurrentUrl() + " with: " + url);
        assertEquals(url, getDriver().getCurrentUrl());
    }

    public void assertURLWithRegex(String regex) {
        assertTrue(getDriver().getCurrentUrl().matches(regex));
    }

    public void assertURLWithOptionalTrailingSlash(String url) {
        assertTrue(getDriver().getCurrentUrl().matches("^" + url + "\\/?$"));
    }

    public boolean elementExist(By locator) {
        return getDriver().findElements(locator).size() != 0;
    }

    /**
     * This method only used to check whether one element text content satisfy text requirement
     *
     * @param elementContentMap is supply of any possibility locator and its text content for one certain element
     *                          This nature is made due to there's a possibility that a single (to be tested) element might appear with more than one locator strategy
     */
    public void verifyElementContent(Map<By, String> elementContentMap) {
        Iterator<Map.Entry<By, String>> it = elementContentMap.entrySet().iterator();
        boolean elementExist = false;
        while (it.hasNext() && !elementExist) {
            Map.Entry<By, String> pair = it.next();
            if (elementExist(pair.getKey())) {
                elementExist = true;
                assertEquals(pair.getValue(), getDriver().findElement(pair.getKey()).getText().trim());
            }
        }
    }

    public void hoverToElement(WebElement elem) {
        Actions actions = new Actions(getDriver());
        actions.moveToElement(elem);
        actions.perform();
    }

}
