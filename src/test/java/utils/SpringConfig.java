package utils;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

//@PropertySource("application.properties")
//@Component
//@Scope(SCOPE_CUCUMBER_GLUE)
@Configuration
@ComponentScan(basePackages = {"globalmanagers", "pageobjects", "utils"})
public class SpringConfig {

}
