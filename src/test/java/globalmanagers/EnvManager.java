package globalmanagers;

import enums.BrowserType;
import enums.CountryType;
import enums.DeploymentType;
import enums.DisplayType;

public class EnvManager {
    private String getEnv(String key) {
        return System.getenv(key);
    }

    public String getBaseUrl() {
        String baseUrl = getEnv(Constants.BASE_URL_ENV_KEY);
        if (baseUrl == null)
            throw new RuntimeException(getMissingEnvKeyMessage(Constants.BASE_URL_ENV_KEY));
        else {
            if (baseUrl.endsWith("/")) {
                baseUrl = baseUrl.substring(0, baseUrl.length() - 1);
            }
            return baseUrl;
        }
    }

    public BrowserType getBrowser() {
        String browserEnv = getEnv(Constants.BROWSER_ENV_KEY);
        if (browserEnv == null)
            return BrowserType.FIREFOX;

        switch (browserEnv) {
            case "firefox":
            default:
                return BrowserType.FIREFOX;
            case "chrome":
                return BrowserType.CHROME;
            case "IE":
                return BrowserType.INTERNETEXPLORER;
            case "safari":
                return BrowserType.SAFARI;
            case "headless":
                return BrowserType.HEADLESS;
        }

    }

    public DisplayType getDisplay() {
        String displayEnv = getEnv(Constants.DISPLAY_ENV_KEY);
        if (displayEnv == null)
            return DisplayType.DESKTOP;
        else
            return displayEnv.toLowerCase().equals("mobile") ? DisplayType.MOBILE : DisplayType.DESKTOP;

    }

    public String getMobileDevice() {
        String deviceName = getEnv(Constants.DEVICE_ENV_KEY);
        if (deviceName == null && getDisplay() == DisplayType.MOBILE)
            throw new RuntimeException("'" + Constants.DEVICE_ENV_KEY + "' Env variable is required when '" + Constants.DISPLAY_ENV_KEY + "' is 'mobile'");
        else if (deviceName == null)
            deviceName = "";

        return deviceName;

    }

    public CountryType getCountry() {
        String countryCode = getEnv(Constants.COUNTRY_ENV_KEY);
        if (countryCode == null)
            throw new RuntimeException(getMissingEnvKeyMessage(Constants.COUNTRY_ENV_KEY));
        switch (countryCode) {
            case "ph":
            case "PH":
                return CountryType.PH;
            case "my":
            case "MY":
                return CountryType.MY;
            case "th":
            case "TH":
                return CountryType.TH;
            default:
                throw new RuntimeException(getInvalidValueMessageForKey(Constants.COUNTRY_ENV_KEY, "PH, MY, TH"));
        }
    }

    public int getTimeout() {
        String timeoutString = getEnv(Constants.TIMEOUT_ENV_KEY);
        return timeoutString != null ? Integer.parseInt(timeoutString) : Constants.timeoutInSec;
    }

    public DeploymentType getDeploymentEnvironment() {
        String deploymentCode = getEnv(Constants.DEPLOY_ENVIRONMENT_ENV_KEY);
        if (deploymentCode == null)
            throw new RuntimeException(getMissingEnvKeyMessage(Constants.DEPLOY_ENVIRONMENT_ENV_KEY));
        switch (deploymentCode.toUpperCase()) {
            case "DEV":
                return DeploymentType.DEV;
            case "STAGING":
                return DeploymentType.STAGING;
            case "PROD":
                return DeploymentType.PROD;
            default:
                throw new RuntimeException(getInvalidValueMessageForKey(Constants.DEPLOY_ENVIRONMENT_ENV_KEY, "dev, staging, prod"));
        }

    }

    private String getMissingEnvKeyMessage(String envKey) {
        return "'" + envKey + "' Env variable is not specified";
    }

    private String getInvalidValueMessageForKey(String envKey, String validValues) {
        return "'" + envKey + "' Env variable value is invalid, , must be one of these: " + validValues;
    }
}
