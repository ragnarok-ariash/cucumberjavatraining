package globalmanagers;

import enums.CountryType;
import enums.DeploymentType;
import enums.DisplayType;
import enums.BrowserType;
import org.openqa.selenium.WebDriver;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
//@ScenarioScope
public class GlobalManager {
    public WebDriver driver;
    public String baseUrl;
    public BrowserType browser;
    public DisplayType display;
    public String deviceName;
    public CountryType country;
    public int timeout;
    public DeploymentType deployEnvironment;
    public Map<String, String> sharedContext;

    public GlobalManager() {
        EnvManager envManager = new EnvManager();
        baseUrl = envManager.getBaseUrl();
        browser = envManager.getBrowser();
        display = envManager.getDisplay();
        deviceName = envManager.getMobileDevice();
        country = envManager.getCountry();
        timeout = envManager.getTimeout();
        deployEnvironment = envManager.getDeploymentEnvironment();
        sharedContext = new HashMap<>();
    }
}
