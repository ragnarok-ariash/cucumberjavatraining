package globalmanagers;

import enums.BrowserType;
import enums.DisplayType;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.util.HashMap;
import java.util.Map;

public class WebDriverFactory {
    private EnvManager envManager;

    public WebDriverFactory() {
        this.envManager = new EnvManager();
    }

    public WebDriver createDriver(BrowserType browserType, DisplayType displayType) {
        switch (browserType) {
            case CHROME:
                return getChromeDriver(displayType);
            case FIREFOX:
            default:
                return getFirefoxDriver(displayType);
            case INTERNETEXPLORER:
                return getIEDriver(displayType);
            case SAFARI:
                return getSafariDriver(displayType);
            case HEADLESS:
                return getHeadlessDriver(displayType);
        }
    }

    private ChromeDriver getChromeDriver(DisplayType displayType) {
        WebDriverManager mgr = WebDriverManager.chromedriver();
        mgr.setup();
        switch (displayType) {
            case MOBILE:
                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", envManager.getMobileDevice());

                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
                chromeOptions.addArguments("--start-maximized");
                //chromeOptions.addArguments("headless"); --works!
                return new ChromeDriver(chromeOptions);
            case DESKTOP:
            default:
                return new ChromeDriver();
        }
    }

    private WebDriver getFirefoxDriver(DisplayType displayType) {
        WebDriverManager mgr = WebDriverManager.firefoxdriver();
        mgr.setup();
        switch (displayType) {
            case MOBILE:
                FirefoxProfile profile = new FirefoxProfile();
                String userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) " +
                        "AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 " +
                        "Mobile/15A356 Safari/604.1";
                String userAgent2 = "Mozilla/5.0 (Linux; Android 8.0.0;" +
                        "Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) " +
                        "Chrome/67.0.3396.99 Mobile Safari/537.36";
                profile.setPreference("general.useragent.override", userAgent);
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.setProfile(profile);
                Map<String, String> mobileEmulations = new HashMap<>();
                mobileEmulations.put("deviceName", envManager.getMobileDevice());

                FirefoxOptions firefoxOption = new FirefoxOptions();
                firefoxOption.setCapability("mobileEmulation", mobileEmulations);

                Map<String, Object> deviceMetrics = new HashMap<>();
                deviceMetrics.put("width", 360);
                deviceMetrics.put("height", 640);
                deviceMetrics.put("pixelRatio", 3.0);
                Map<String, Object> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceMetrics", deviceMetrics);
                mobileEmulation.put("userAgent", userAgent2);
                firefoxOption = new FirefoxOptions();
                firefoxOption.setCapability("mobileEmulation", mobileEmulation);

                return getChromeDriver(displayType);//new FirefoxDriver(firefoxOption);
            case DESKTOP:
            default:
                return new FirefoxDriver();
        }
    }

    private WebDriver getIEDriver(DisplayType displayType) {
        switch (displayType) {
            case MOBILE:
                return getChromeDriver(displayType);
            case DESKTOP:
            default:
                return new InternetExplorerDriver();
        }
    }

    private WebDriver getSafariDriver(DisplayType displayType) {
        switch (displayType) {
            case MOBILE:
                return getChromeDriver(displayType);
            case DESKTOP:
            default:
                return new SafariDriver();
        }
    }

    private WebDriver getHeadlessDriver(DisplayType displayType) {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        WebDriverManager mgr = WebDriverManager.chromedriver();
        mgr.setup();
        switch (displayType) {
            case MOBILE:

                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", envManager.getMobileDevice());
                chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
                //chromeOptions.addArguments("headless"); --works!
                return new ChromeDriver(chromeOptions);
            case DESKTOP:
            default:
                return new ChromeDriver(chromeOptions);
        }
    }

}
