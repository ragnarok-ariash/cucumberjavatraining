package globalmanagers;

public class Constants {
    public final static String BASE_URL_ENV_KEY = "url";
    public final static String BROWSER_ENV_KEY = "browser";
    public final static String DISPLAY_ENV_KEY = "display";
    public final static String DEVICE_ENV_KEY = "device_name";
    public final static String COUNTRY_ENV_KEY = "country";
    public final static String DEPLOY_ENVIRONMENT_ENV_KEY = "deploy_env";
    public final static String TIMEOUT_ENV_KEY = "timeout";
    public final static int timeoutInSec = 15;
}
