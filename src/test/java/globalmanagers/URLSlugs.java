package globalmanagers;

public class URLSlugs {
    public final static String CI = "/car-insurance";
    public final static String CC = "/credit-card";
    public final static String PL = "/personal-loan";
    public final static String GDP = "/gadget-protect";
    public final static String TI = "/travel-insurance";
    public final static String PARTNER = "/partners";
    public final static String APPLICATION_SUCCESS = "/application-success";
    public final static String PARTNER_PRODUCT_REDIRECT = "/page-redirect?redirect_url=";

    public final static String PRIVACY_POLICY = "/privacy-policy";
    public final static String TERM_AND_CONDITION = "/terms-and-conditions";
    public final static String ABOUTUS = "/about-us";

    public static final String ALL_ARTICLE = "/articles";
    public static final String ARTICLE_CI = "/articles/car-insurance";
    public static final String ARTICLE_CC = "/articles/credit-card";
    public static final String ARTICLE_PL = "/articles/personal-loan";
    public static final String ARTICLE_PERSONAL_FINANCE = "/articles/personal-finance";
    public static final String ARTICLE_GADGET_PROTECT = "/articles/gadget-protect";
    public static final String ARTICLE_GOV_SERVICE = "/articles/government-services";
    public static final String ARTICLE_LIFESTYLE = "/articles/lifestyle";

    public final static String CC_FUNNEL = CC+"/card-information";
    public final static String CC_FUNNEL_PARTNER = CC+"/card-information?productId=";
    public final static String CC_RESULT = CC+"/results/all";
    public final static String CC_SUCCESS = CC+APPLICATION_SUCCESS;
    public final static String CC_PRODUCT = CC+"/product";
    public final static String CC_ALL = CC+"/all";

    public final static String CI_NEW_QUOTE = "/car-insurance/new-quote";
    public final static String CI_SUCCESS = "/car-insurance"+APPLICATION_SUCCESS;
    public final static String CI_PARTNER = "/car-insurance/partners";

    public final static String PL_DETAILS = PL+"/loan-details";
    public final static String PL_INFO = PL+"/loan-information";
    public final static String PL_RESULT = PL+"/results";
    public final static String PL_DETAILS_PRODUCT = PL+"/loan-details?productId=";
    public final static String PL_SUCCESS = PL+APPLICATION_SUCCESS;
    public final static String PL_PRODUCT = PL+"/product/standard-chartered-cash-one?version=augmented";


    public final static String TI_RESULT = "/travel-insurance/result";
    public final static String TI_SUCCESS = "/travel-insurance"+APPLICATION_SUCCESS+"?provider_link";

    public final static String GDP_PAYGATE_DEV = "https://testpti.payserv.net/webpayment/default.aspx";
    public final static String GDP_PAYGATE_STAGING = "https://testpti.payserv.net/webpayment/default.aspx";
    public final static String GDP_PAYGATE_PROD = "https://ptiapps.paynamics.net/webpayment/Default.aspx";
}
