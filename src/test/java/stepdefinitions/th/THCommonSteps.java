package stepdefinitions.th;

import pageobjects.THPIForm;
import stepdefinitions.base.AbstractSteps;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.And;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Slf4j
public class THCommonSteps extends AbstractSteps {
    @And("Verify PI form appearance")
    public void verifyPIFormAppearance() throws PendingException {
        THPIForm PIForm = loadPageComponent(THPIForm.class);
        waitUntilElementVisible(PIForm.formContainer);
        //verify modal form visibility
        assertEquals(true, PIForm.formBody.isDisplayed());
    }

    @And("Enter Personal Information with First Name {string} and Surame {string} and Email {string} and Phone Number {string} and LineIDorRangeIncome {string}")
    public void enterPersonalInformationWithFirstNameAndSurameAndEmailAndPhoneNumberAndLineIDorRangeIncome(String firstName, String lastName, String email, String phoneNumber, String lineIDorRangeIncome) throws PendingException, InterruptedException {
        THPIForm PIForm = loadPageComponent(THPIForm.class);
        waitUntilElementVisible(PIForm.formBody);

        PIForm.setElement(PIForm.firstNameInput, firstName, seleniumUtil);
        PIForm.setElement(PIForm.lastNameInput, lastName, seleniumUtil);
        PIForm.setElement(PIForm.emailInput, email, seleniumUtil);
        PIForm.setElement(PIForm.phoneInput, phoneNumber, seleniumUtil);
        PIForm.setElement(PIForm.lineIDorRangeIncomeInput, lineIDorRangeIncome, seleniumUtil);

    }

    @And("Check I Agree with Terms and conditions and privacy policy")
    public void checkIAgreeWithTermsAndConditionsAndPrivacyPolicy() throws PendingException {
        THPIForm.checkAgreement(getDriver());
    }

    @And("Click Submit button on PI then wait until PI form disappear")
    public void clickSubmitButtonOnPIThenWaitUntilPIFormDisappear() {
        THPIForm PIForm = loadPageComponent(THPIForm.class);
        assertTrue(PIForm.submitBtn.isEnabled());
        clickElement(PIForm.submitBtn);

        try {
            waitUntilElementGone(PIForm.formContainer);
        } catch (TimeoutException e) {
            log.info("cant wait until PIForm.formContainer to disappear due to element not found thus causing TimeoutException");
        }
    }


}
