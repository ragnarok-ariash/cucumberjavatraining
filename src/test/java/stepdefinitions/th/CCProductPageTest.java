package stepdefinitions.th;

import enums.DisplayType;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;
import pageobjects.th.CCPartnerPage;
import pageobjects.th.CCResultPage;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@Slf4j
public class CCProductPageTest extends AbstractSteps {

    @Autowired
    private CCResultPage ccResultPage;

    @Autowired
    private CCPartnerPage ccPartnerPage;

    private static final String PRODUCT_PAGE_URL = "product-page-url";
    private static final String ENDPOINT_URL = "endpoint-url";
    private static final String URL_GET_PARAM = "url-param-get";

    @Given("Open moneyguru Credit Card page {string}")
    public void openMoneyguruCreditCardPage(String url) throws PendingException {
        navigateInto(url);
    }

    @Given("Open Credit Card Partner page {string}")
    public void openCreditCardPartnerPage(String url) throws PendingException {
        navigateInto(url);
    }

    @Given("Open the Credit Card Product page standalone version")
    public void openTheCreditCardProductPageStandaloneVersion() throws PendingException {
        navigateInto("https://www.moneyguru.co.th/credit-card/product/citibank/citi-ready-credit/?version=standalone");
    }

    @Given("Open the Credit Card Product page augmented version")
    public void openTheCreditCardProductPageAugmentedVersion() throws PendingException {
        navigateInto("https://www.moneyguru.co.th/credit-card/product/citibank/citi-ready-credit/?version=augmented");
    }

    @Given("Open the Credit Card Product page in standalone version {string}")
    public void openTheCreditCardProductPageInStandaloneVersion(String url) throws PendingException {
        navigateInto(url);
    }

    @Given("Open the Credit Card Product page in augmented version {string}")
    public void openTheCreditCardProductPageInAugmentedVersion(String url) throws PendingException {
        navigateInto(url);
    }

    @Given("Open the Credit Card Product page {string}")
    public void openTheCreditCardProductPage(String url) throws PendingException {
        navigateInto(url);
    }

    @When("Click on any Product Name")
    public void clickOnAnyProductName() throws PendingException {
        clickElement(ccResultPage.getAnyProductName());
        //clicking will trigger new tab
        seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(2));
        getDriver().switchTo().window(((List<String>) new ArrayList<>(getDriver().getWindowHandles())).get(1));
    }

    @When("Click on any Product Image")
    public void clickOnAnyProductImage() throws PendingException {
        clickElement(ccPartnerPage.getAnyProductImage());
        //clicking will trigger new tab
        seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(2));
        getDriver().switchTo().window(((List<String>) new ArrayList<>(getDriver().getWindowHandles())).get(1));
    }

    @When("Click on Apply CTA")
    public void clickOnApplyCTA() throws PendingException {
        clickElement(ccPartnerPage.getApplyBtn());
        waitUntilElementVisible(ccPartnerPage.getPersonalInfoView());
    }

    @When("Scroll down until sticky bar appears")
    public void scrollDownUntilStickyBarAppears() throws PendingException {
        scrollElementToMiddle(ccPartnerPage.productImage);
        waitUntilElementVisible(ccPartnerPage.stickyNavBar);
    }

    @When("Scroll down into Special Promotion section")
    public void scrollDownIntoSpecialPromotionSection() throws PendingException {
        seleniumUtil.scrollElementToBelowElement(ccPartnerPage.specialPromotionTitle, ccPartnerPage.stickyNavBar);
        seleniumUtil.waitUntilElementsGetCloseWithSpecificYOffset(ccPartnerPage.specialPromotionTitle,
                ccPartnerPage.stickyNavBar, 10);
    }

    @When("Hover pointer into Credit Card Provider logo")
    public void hoverPointerIntoCreditCardProviderLogo() throws PendingException {
        seleniumUtil.hoverToElement(ccPartnerPage.providerLogo);
    }

    @When("Click Credit Card Information tab")
    public void clickCreditCardInformationTab() throws PendingException {
        clickOnNavbarTab(ccPartnerPage.stickyNavBarCCInfo, ccPartnerPage.productSummmaryView);
    }

    @When("Click Credit Card applicant Qualifications tab")
    public void clickCreditCardApplicantQualificationsTab() throws PendingException {
        clickOnNavbarTab(ccPartnerPage.stickyNavBarQualification, ccPartnerPage.productEligibilityView);
    }

    @When("Click Fees & Expenses tab")
    public void clickFeesExpensesTab() throws PendingException {
        clickOnNavbarTab(ccPartnerPage.stickyNavBarFeeExpense, ccPartnerPage.productFeeAndExpenseView);
    }

    @When("Click Frequently asked questions tab")
    public void clickFrequentlyAskedQuestionsTab() throws PendingException {
        clickOnNavbarTab(ccPartnerPage.stickyNavBarFAQ, ccPartnerPage.productFAQView);
    }

    private void clickOnNavbarTab(WebElement tabMenu, WebElement menuContent) {
        clickElement(tabMenu);
        seleniumUtil.waitUntilElementAppearOnViewPort(menuContent, false);
    }

    @When("Scroll into Other credit cards you might be interested in")
    public void scrollIntoOtherCreditCardsYouMightBeInterestedIn() throws PendingException {
        scrollIntoView(ccPartnerPage.otherCCView);
    }

    @When("Clicks any related product box")
    public void clicksAnyRelatedProductBox() throws PendingException {
        //click on title
        WebElement titleProduct = ccPartnerPage.getAnyRelatedProduct()
                .findElement(By.xpath(".//p[@class = 'text-primary related-text-title']"));

        String url = titleProduct.findElement(By.tagName("a")).getAttribute("href");
        String[] urlSegments = url.split("\\?", 2);

        if (urlSegments.length == 2) {
            pageData.setCustomData(ENDPOINT_URL, urlSegments[0]);
            pageData.setCustomData(URL_GET_PARAM, urlSegments[1]);
            pageData.setCustomData(PRODUCT_PAGE_URL, null);
        } else
            pageData.setCustomData(PRODUCT_PAGE_URL, url);

        clickElement(titleProduct);

        seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(2));
        List<String> tabHandles = new ArrayList<>(getDriver().getWindowHandles());
        assertNotEquals(0, tabHandles.size());
        getDriver().switchTo().window(tabHandles.get(1));

        waitUntilFinishRedirectToURLContains(urlSegments.length == 2 ? urlSegments[0] : url);
    }

    @When("Scroll into Follow us section")
    public void scrollIntoFollowUsSection() throws PendingException {
        scrollIntoView(ccPartnerPage.followUsView);
    }

    @When("Input email with the invalid email format {string}")
    public void inputEmailWithTheInvalidEmailFormat(String emailAddress) throws PendingException {
        inputSubscribeEmail(emailAddress);
    }

    @When("Input email with the valid email format {string}")
    public void inputEmailWithTheValidEmailFormat(String emailAddress) throws PendingException {
        inputSubscribeEmail(emailAddress);
    }

    private void inputSubscribeEmail(String emailAddress) {
        if (globalManager.display == DisplayType.MOBILE) {
            scrollElementToMiddle(ccPartnerPage.getSubscribeInput());
            seleniumUtil.waitUntilElementAppearOnViewPort(ccPartnerPage.getSubscribeInput());
        }
        enterInputWithTextByElement(emailAddress, ccPartnerPage.getSubscribeInput());
    }

    @When("Hover over the the Free to apply CTA")
    public void hoverOverTheTheFreeToApplyCTA() throws PendingException {
        seleniumUtil.hoverToElement(ccPartnerPage.getSubscribeBtn());
        waitUntilElementVisible(ccPartnerPage.getSubscribeValidationPopup());
    }

    @When("Click on Free to apply CTA")
    public void clickOnFreeToApplyCTA() throws PendingException {
        clickElement(ccPartnerPage.getSubscribeBtn());
    }

    @Then("Verify the augmented version shown in the url")
    public void verifyTheAugmentedVersionShownInTheUrl() throws PendingException {
        verifyPageVersionInTheUrl("?version=augmented");
    }

    @Then("Verify the standalone version shown in the url")
    public void verifyTheStandaloneVersionShownInTheUrl() throws PendingException {
        verifyPageVersionInTheUrl("?version=standalone");
    }

    private void verifyPageVersionInTheUrl(String urlSlug) throws PendingException {
        seleniumUtil.verifyPageUrl(urlSlug, SeleniumUtil.VerifyMethod.SOFT);
    }

    @Then("Verify the Personal Information Form is displayed")
    public void verifyThePersonalInformationFormIsDisplayed() throws PendingException {
        assertTrue(ccPartnerPage.getPersonalInfoView().isDisplayed());
    }

    @Then("Verify the following special promotion detail:")
    public void verifyTheFollowingSpecialPromotionDetail(DataTable dataTable) throws PendingException {
        dataTable.asList().forEach(detail -> {
            switch (detail) {
                case "Headings: \"Special Promotion\"":
                    assertEquals("โปรโมชั่นพิเศษ", ccPartnerPage.specialPromotionTitle.getText());
                    break;
                case "Promotion Banner":
                    assertTrue(ccPartnerPage.promotionBannerView.isDisplayed());
                    break;
                case "Title":
                    scrollIntoView(ccPartnerPage.productSummmaryView);
                    assertTrue(ccPartnerPage.productTitle.isDisplayed());
                    break;
                case "Descriptions":
                    scrollIntoView(ccPartnerPage.productSummmaryView);
                    assertTrue(ccPartnerPage.productDescDescribed());
                    break;
            }
        });
    }

    private void scrollIntoView(WebElement sectionView) throws PendingException {
        seleniumUtil.scrollElementToBelowElement(sectionView, ccPartnerPage.stickyNavBar);
        seleniumUtil.waitUntilElementsGetCloseWithSpecificYOffset(sectionView,
                ccPartnerPage.stickyNavBar, 10);
    }

    @Then("Verify The following Product Detail Credit card Information are displayed correctly:")
    public void verifyTheFollowingProductDetailCreditCardInformationAreDisplayedCorrectly(DataTable dataTable) throws PendingException {
        dataTable.asList().forEach(information -> {
            switch (information) {
                case "Product description title":
                    assertTrue(ccPartnerPage.productDescTitle.isDisplayed());
                    break;
                case "Product description paragraph":
                    assertTrue(ccPartnerPage.getProductDescParagraph().isDisplayed());
                    break;
                case "Provider logo":
                    assertTrue(ccPartnerPage.providerLogo.isDisplayed());
                    break;
                case "Minimum monthly income":
                    assertTrue(ccPartnerPage.minMonthlyIncome.isDisplayed());
                    break;
                case "Interest per year":
                    assertTrue(ccPartnerPage.interestPerYear.isDisplayed());
                    break;
                case "Annual fee":
                    assertTrue(ccPartnerPage.annualFee.isDisplayed());
                    break;
            }
        });
    }

    @Then("Verify the Detail content on sticky bar is displayed")
    public void verifyTheDetailContentOnStickyBarIsDisplayed() throws PendingException {
        assertTrue(ccPartnerPage.stickyNavBar.isDisplayed());
    }

    @Then("Verify The following Details sticky content are displayed correctly:")
    public void verifyTheFollowingDetailsStickyContentAreDisplayedCorrectly(DataTable dataTable) throws PendingException {
        dataTable.asList().forEach(content -> {
            switch (content) {
                case "Credit Card Information tab Section":
                    assertTrue(ccPartnerPage.stickyNavBarCCInfo.isDisplayed());
                    break;
                case "Qualifications tab Section":
                    assertTrue(ccPartnerPage.stickyNavBarQualification.isDisplayed());
                    break;
                case "Fees and expenses tab Section":
                    assertTrue(ccPartnerPage.stickyNavBarFeeExpense.isDisplayed());
                    break;
                case "FAQs tab Section":
                    assertTrue(ccPartnerPage.stickyNavBarFAQ.isDisplayed());
                    break;
                case "Card Logo":
                    assertTrue(ccPartnerPage.stickyNavBarCardLogo.isDisplayed());
                    break;
                case "Apply Now CTA":
                    assertTrue(ccPartnerPage.stickyNavBarApplyBtn.isDisplayed());
                    break;
            }
        });
    }

    @Then("Verify should be navigated to Credit Card Information section")
    public void verifyShouldBeNavigatedToCreditCardInformationSection() throws PendingException {
        verifyNavigatedInto(ccPartnerPage.productSummmaryView);
    }

    @Then("Verify should be navigated to Credit Card applicant Qualifications section")
    public void verifyShouldBeNavigatedToCreditCardApplicantQualificationsSection() throws PendingException {
        verifyNavigatedInto(ccPartnerPage.productEligibilityView);
    }

    @Then("Verify should be navigated to Fees & Expenses section")
    public void verifyShouldBeNavigatedToFeesExpensesSection() throws PendingException {
        verifyNavigatedInto(ccPartnerPage.productFeeAndExpenseView);
    }

    @Then("Verify should be navigated to Frequently asked questions section")
    public void verifyShouldBeNavigatedToFrequentlyAskedQuestionsSection() throws PendingException {
        verifyNavigatedInto(ccPartnerPage.productFAQView);
    }

    private void verifyNavigatedInto(WebElement sectionView) {
        seleniumUtil.isElementVisibleOnViewPort(sectionView, false);
    }

    @Then("Verify Credit Card Information as section titles is displayed")
    public void verifyCreditCardInformationAsSectionTitlesIsDisplayed() throws PendingException {
        verifySectionTitleIsDisplayed(ccPartnerPage.productSummmaryView,
                ccPartnerPage.productSummmaryView.findElement(By.xpath(".//h2[@class='title font-bold-300 set-ml20']")),
                "ข้อมูลบัตรเครดิต");
    }

    @Then("Verify Property as section title is displayed")
    public void verifyPropertyAsSectionTitleIsDisplayed() throws PendingException {
        verifySectionTitleIsDisplayed(ccPartnerPage.productEligibilityView,
                ccPartnerPage.productEligibilityView.findElement(By.xpath(".//span[@class='accord-title']")),
                "คุณสมบัติ");
    }

    @Then("Verify Fees & Expenses as section titles is displayed")
    public void verifyFeesExpensesAsSectionTitlesIsDisplayed() throws PendingException {
        verifySectionTitleIsDisplayed(ccPartnerPage.productFeeAndExpenseView,
                ccPartnerPage.productFeeAndExpenseView.findElement(By.xpath(".//span[@class='accord-title']")),
                "ค่าธรรมเนียมและค่าใช้จ่าย");
    }

    @Then("Verify Frequently asked questions as section titles is displayed")
    public void verifyFrequentlyAskedQuestionsAsSectionTitlesIsDisplayed() {
        verifySectionTitleIsDisplayed(ccPartnerPage.productFAQView,
                ccPartnerPage.productFAQView.findElement(By.xpath(".//h2[contains(@class,'title')]")),
                "คำถามที่พบบ่อย");
    }

    private void verifySectionTitleIsDisplayed(WebElement sectionView, WebElement titleElement, String titleText) {
        assertTrue(sectionView.isDisplayed());
        assertTrue(titleElement.isDisplayed());
        assertEquals(titleText, titleElement.getText());
    }

    @Then("Verify The following Details Credit Card Information :")
    public void verifyTheFollowingDetailsCreditCardInformation(DataTable dataTable) throws PendingException {
        dataTable.asList().forEach(content -> {
            switch (content) {
                case "Information Title":
                    assertTrue(ccPartnerPage.informationTitle.isDisplayed());
                    break;
                case "Information Description":
                    assertTrue(ccPartnerPage.informationDesc.isDisplayed());
                    break;
            }
        });
    }

    @Then("Verify The following Details Credit Card applicant Qualifications:")
    public void verifyTheFollowingDetailsCreditCardApplicantQualifications(DataTable dataTable) throws PendingException {
        assertFalse(ccPartnerPage.eligibilityTableRows.isEmpty());
        WebElement rowElem = null;
        String text = null;
        for (String content : dataTable.asList()) {
            switch (content) {
                case "Minimum age requirement":
                    rowElem = ccPartnerPage.eligibilityTableRows.get(0);
                    text = "เกณฑ์อายุขั้นต่ำ";
                    break;
                case "Minimum age requirement (supplementary card)":
                    rowElem = ccPartnerPage.eligibilityTableRows.get(1);
                    text = "เกณฑ์อายุขั้นต่ำ(บัตรเสริม)";
                    break;
                case "Minimum monthly income":
                    rowElem = ccPartnerPage.eligibilityTableRows.get(2);
                    text = "รายได้ต่อเดือนขั้นต่ำ";
                    break;
                case "Landline number":
                    rowElem = ccPartnerPage.eligibilityTableRows.get(3);
                    text = "หมายเลขโทรศัพท์พื้นฐาน";
                    break;
            }
            assertNotNull(rowElem);
            assertNotNull(text);
            WebElement textElem = rowElem.findElement(By.xpath(".//td[@class='product-summary-text-left']"));
            log.info("content: " + content + ", text: " + textElem.getText() + ", expected: " + text);
            assertTrue(textElem.isDisplayed());
            assertEquals(text, textElem.getText());
        }

    }

    @Then("Verify The following Details Fees & Expenses :")
    public void verifyTheFollowingDetailsFeesExpenses(DataTable dataTable) throws PendingException {
        assertFalse(ccPartnerPage.feeExpenseTableRows.isEmpty());
        WebElement rowElem = null;
        String text = null;
        boolean contains;
        for (String content : dataTable.asList()) {
            contains = false;
            switch (content) {
                case "Entrance fee":
                    rowElem = ccPartnerPage.feeExpenseTableRows.get(0);
                    text = "ค่าธรรมเนียมแรกเข้า";
                    break;
                case "Annual fee":
                    rowElem = ccPartnerPage.feeExpenseTableRows.get(1);
                    text = "ค่าธรรมเนียมรายปี";
                    contains = true;
                    break;
                case "Interest (per year)":
                    rowElem = ccPartnerPage.feeExpenseTableRows.get(2);
                    text = "ดอกเบี้ย (ต่อปี)";
                    break;
                case "Cash withdrawal fee":
                    rowElem = ccPartnerPage.feeExpenseTableRows.get(3);
                    text = "ค่าธรรมเนียมในการกดเงินสด";
                    break;
                case "Minimum payment":
                    rowElem = ccPartnerPage.feeExpenseTableRows.get(4);
                    text = "ยอดชำระขั้นต่ำ";
                    break;
                case "Annual fee waived When having spending amount":
                    rowElem = ccPartnerPage.feeExpenseTableRows.get(5);
                    text = "ยกเว้นค่าธรรมเนียมรายปี เมื่อมียอดใช้จ่าย";
                    break;
            }
            assertNotNull(rowElem);
            assertNotNull(text);
            WebElement textElem = rowElem.findElement(By.xpath(".//td[@class='product-summary-text-left']"));
            log.info("content: " + content + ", text: " + textElem.getText() + ", expected: " + text);
            assertTrue(textElem.isDisplayed());
            if (contains)
                assertTrue(textElem.getText().contains(text));
            else
                assertEquals(text, textElem.getText().trim());
        }
    }

    @Then("Verify Question Title and Answer Description on Frequently asked questions")
    public void verifyQuestionTitleAndAnswerDescriptionOnFrequentlyAskedQuestions() throws PendingException {
        for (WebElement faq : ccPartnerPage.getFaqs()) {
            WebElement faqAccordionBtn = faq.findElement(By.xpath(".//button"));
            if (!faqAccordionBtn.getAttribute("class").contains("active")) {
                clickElementUsingScript(faqAccordionBtn);
                waitUntilAttributeElementContains(faqAccordionBtn, "class", "active");
            }
            assertTrue(faqAccordionBtn.findElement(By.tagName("span"))
                    .isDisplayed());
            assertTrue(faq.findElement(By.xpath(".//blockquote[@class = 'accord-blockquote faq-accord-blockquote']"))
                    .isDisplayed());
        }
    }

    @Then("Verify for more related product option are displayed:")
    public void verifyForMoreRelatedProductOptionAreDisplayed(DataTable dataTable) throws PendingException {
        assertEquals(4, ccPartnerPage.relatedCards.size());
        Map<String, String> thTextMap = Map.ofEntries(
                Map.entry("Citi Grab Credit Card", "บัตรเครดิตซิตี้ แกร็บ"),
                Map.entry("Citi Mercedes credit card", "บัตรเครดิตซิตี้ เมอร์เซเดส"),
                Map.entry("KTC Cashback Platinum MasterCard credit card", "บัตรเครดิตเคทีซี แคชแบ็ค แพลทินัม มาสเตอร์การ์ด"),
                Map.entry("UOB Preferred Platinum credit card", "บัตรเครดิต ยูโอบี พรีเฟอร์ แพลทินัม")
        );
        dataTable.asList().forEach(relatedProduct -> {
            String thText = thTextMap.get(relatedProduct);
            assertNotNull(thText);
            WebElement relatedProductName = ccPartnerPage.otherCCView
                    .findElement(By.xpath(".//div[contains(@class, 'related-card')]//p[.='" + thText + "']"));
            if (globalManager.display == DisplayType.MOBILE && !relatedProductName.isDisplayed()) {
                seleniumUtil.scrollElementWithinScrollViewIntoView(relatedProductName);
                seleniumUtil.waitUntilElementAppearOnViewPort(relatedProductName);
            }
            assertTrue(relatedProductName.isDisplayed());
        });
    }

    @Then("Verify the details on each related product box Product title, Minimum monthly income, Annual Fee")
    public void verifyTheDetailsOnEachRelatedProductBoxProductTitleMinimumMonthlyIncomeAnnualFee() throws PendingException {
        ccPartnerPage.relatedCards.forEach(relatedCC -> {
            //product title
            assertTrue(relatedCC.findElement(By.xpath(".//p[@class = 'text-primary related-text-title']")).isDisplayed());
            //min. monthly income
            assertTrue(relatedCC.findElement(By.xpath(".//div[@class = 'related-prod-gp-docs-type']")).isDisplayed());
            assertTrue(relatedCC.findElement(By.xpath(".//div[@class = 'related-prod-gp-docs-type']//div[@class = 'value related-value']"))
                    .isDisplayed());
            //annual fee
            assertTrue(relatedCC.findElement(By.xpath(".//div[@class = 'related-prod-cc-fees-type']")).isDisplayed());
            assertTrue(relatedCC.findElement(By.xpath(".//div[@class = 'related-prod-cc-fees-type']//div[@class = 'value related-value']"))
                    .isDisplayed());
        });
    }

    @Then("Verify the details of each related product box:")
    public void verifyTheDetailsOfEachRelatedProductBox(DataTable dataTable) throws PendingException {
        ccPartnerPage.relatedCards.forEach(relatedCC -> {

        });
        dataTable.asList().forEach(content -> {
            switch (content) {
                case "Product title":
                    assertTrue(ccPartnerPage.informationTitle.isDisplayed());
                    break;
                case "Minimum monthly income":
                    assertTrue(ccPartnerPage.informationDesc.isDisplayed());
                    break;
                case "Annual Fee":
                    assertTrue(ccPartnerPage.informationDesc.isDisplayed());
                    break;
            }
        });
    }

    @Then("Verify it can navigate properly to the related product page when clicking on it")
    public void verifyItCanNavigateProperlyToTheRelatedProductPageWhenClickingOnIt() throws PendingException {
        if (pageData.getCustomData(PRODUCT_PAGE_URL) == null) {
            //url target has GET params (marked by ?)
            String[] urlSegments = getDriver().getCurrentUrl().split("\\?", 2);
            assertTrue(urlSegments[0].matches("^" + pageData.getCustomData(ENDPOINT_URL) + "\\/?$"));
            assertEquals(urlSegments[1], pageData.getCustomData(URL_GET_PARAM));
        } else
            seleniumUtil.verifyPageUrl(pageData.getCustomData(PRODUCT_PAGE_URL), SeleniumUtil.VerifyMethod.HARD);
    }

    @Then("Verify Please enter a valid email address message is displayed")
    public void verifyPleaseEnterAValidEmailAddressMessageIsDisplayed() throws PendingException {
        assertEquals("กรุณาใส่อีเมล์ให้ถูกต้อง", ccPartnerPage.getSubscribeValidationText().getText());
    }

    @Then("Verify {string} message is displayed")
    public void verifyMessageIsDisplayed(String subscribeMessage) throws PendingException {
        assertEquals(subscribeMessage, ccPartnerPage.getSubscribeValidationText().getText());
    }

    @Then("Verify {string} message displayed after submit")
    public void verifyMessageDisplayedAfterSubmit(String message) throws PendingException {
        seleniumUtil.waitFor(ExpectedConditions.textToBePresentInElement(ccPartnerPage.getSubscribeValidationText(), message));
        assertEquals(message, ccPartnerPage.getSubscribeValidationText().getText());
    }


}
