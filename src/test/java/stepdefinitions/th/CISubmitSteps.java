package stepdefinitions.th;

import enums.DisplayType;
import pageobjects.THPIForm;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@Slf4j
public class CISubmitSteps extends AbstractSteps {

    @And("Click on Get a free quote button on subsequent page")
    public void clickOnGetAFreeQuoteButtonOnSubsequentPage() throws PendingException {
        verifyPageUrl("/car-insurance", SeleniumUtil.VerifyMethod.HARD);
        log.info("curl: " + getDriver().getCurrentUrl());
        clickElementByLocator(By.cssSelector(".btn"));
    }

    @And("Verify the funnel Page appears")
    public void verifyTheFunnelPageAppears() throws PendingException {
        verifyPageUrl("/car-insurance/new-quote", SeleniumUtil.VerifyMethod.HARD);
    }

    @And("Enter your car's details with click the dropdown: {string}, {string}, {string} and {string}")
    public void enterYourCarSDetailsWithClickTheDropdownAnd(String carBrand, String carModel, String carYear, String carVariant) throws PendingException, InterruptedException {
        if (globalManager.display == DisplayType.DESKTOP) {
            selectDropdownWithValueByLocator(carBrand, By.xpath("//select[@class='form-control car-select2']"));
            selectDropdownWithValueByLocator(carModel, By.xpath("//select[@class='form-control car-type2']"));
            selectDropdownWithValueByLocator(carYear, By.cssSelector("#car_year"));
            selectDropdownWithValueByLocator(carVariant, By.xpath("//select[@id='trim_car']"));

        } else {
            //select car brand
            seleniumUtil.waitFor(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//ul[@id='car-brand']/child::label"), 1));
            String carBrandXpath = "//ul[@id='car-brand']/child::label[contains(.,'" + carBrand + "')]";
            //check if elements exist on list, otherwise use dropdown
            if (elementExist(By.xpath(carBrandXpath)))
                clickElementByLocator(By.xpath(carBrandXpath));
            else
                selectDropdownWithValueByLocator(carBrand, By.cssSelector(".car-select2-mobile"));

            //car model
            seleniumUtil.waitFor(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//ul[@class='list-group radio-button visible-xs car-type2-mobile-list']/child::label"), 1));
            String carModelXpath = "//ul[@class='list-group radio-button visible-xs car-type2-mobile-list']/child::label[contains(.,'" + carModel + "')]";
            //check if elements exist on list, otherwise use dropdown
            if (elementExist(By.xpath(carModelXpath))) {
                By carModelLocator = By.xpath(carModelXpath);
                handleOutOfViewPortElement(getDriver().findElement(carModelLocator));
                clickElementByLocator(carModelLocator);
            } else {
                By carModelLocator = By.cssSelector(".car-type2-mobile");
                handleOutOfViewPortElement(getDriver().findElement(carModelLocator));
                selectDropdownWithValueByLocator(carModel, carModelLocator);
            }

            By carYearLocator = By.xpath("//select[@id='car_year']");
            handleOutOfViewPortElement(getDriver().findElement(carYearLocator));
            selectDropdownWithValueByLocator(carYear, carYearLocator);

            By carVariantLocator = By.xpath("//select[@id='trim_car']");
            handleOutOfViewPortElement(getDriver().findElement(carVariantLocator));
            selectDropdownWithValueByLocator(carVariant, carVariantLocator, 2);//the last parameter indicating the number of non-candidate option
        }
    }

    private void handleOutOfViewPortElement(WebElement element) {
        if (!seleniumUtil.isElementVisibleOnViewPort(element)) {
            seleniumUtil.scrollIntoElement(element);
            seleniumUtil.waitUntilElementAppearOnViewPort(element);
        }
    }

    @And("Enter your specify the nature of the car used by select {string} as Car usage characteristic question and {string} as The type of car insurance you need")
    public void enterYourSpecifyTheNatureOfTheCarUsedBySelectAsCarUsageCharacteristicQuestionAndAsTheTypeOfCarInsuranceYouNeed(String personalOrCommercial, String carClass) throws PendingException {
        clickElementByLocator(By.xpath("//label[contains(.,'" + personalOrCommercial + "')]"));
        selectDropdownWithValueByLocator(carClass, By.xpath("//select[@id='car_class']"));
    }

    @And("Click on To roceed button")
    public void clickOnToRoceedButton() throws PendingException {
        clickElementByLocator(By.id("button-submit-ci"));
    }

    @And("Click Apply Now button on any data in the given list")
    public void clickApplyNowButtonOnAnyDataInTheGivenList() throws PendingException {
        //check invisibility of PI Form
        THPIForm PIForm = loadPageComponent(THPIForm.class);
        assertEquals(false, PIForm.isFormDisplayed());

        WebElement listContainer = getDriver().findElement(By.xpath("//div[@id='list-data' and @class='list-data-insurance']"));
        List<WebElement> carInsuranceDataList = listContainer.findElements(By.xpath("child::div[contains(concat(' ',@class,' '), ' card-product ')]"));
        //verify there is any insurance data to select apply button on
        assertNotEquals(0, carInsuranceDataList.size());
        WebElement firstDataList = carInsuranceDataList.get(0);

        By applyBtnLocator = By.xpath(globalManager.display == DisplayType.DESKTOP ?
                "descendant::div[@class = 'hidden-xs hidden-sm text-center']/descendant::button[contains(@class, 'btn-apply')]" :
                "descendant::div[@class = 'row visible-xs visible-sm']/descendant::button[contains(@class, 'btn-apply')]");

        seleniumUtil.waitFor(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//div[@id='list-data']/child::*"), 0));
        clickElement(firstDataList.findElement(applyBtnLocator));
    }

    @Then("the application success page will be displayed")
    public void theApplicationSuccessPageWillBeDisplayed() throws PendingException {
        verifyPageUrl("/car-insurance/application-success", SeleniumUtil.VerifyMethod.HARD);
    }
}
