package stepdefinitions.th;

import enums.DisplayType;
import globalmanagers.URLSlugs;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
public class CCSubmitSteps extends AbstractSteps {
    private String bankSiteRedirectUrl;

    @And("Verify the Credit Card Page appears")
    public void verifyTheCreditCardPageAppears() throws PendingException {
        verifyPageUrl(URLSlugs.CC_ALL, SeleniumUtil.VerifyMethod.HARD);
    }

    @And("click Apply Now on any data in the given list")
    public void clickApplyNowOnAnyDataInTheGivenList() throws PendingException {
        WebElement firstDataList = checkAndGetDataListWithIndex(
                "//div[@id='list-data']/child::*", 0);
        By applyBtnLocator = By.xpath(globalManager.display == DisplayType.DESKTOP ?
                "child::div[@class='panel-body']/descendant::a[contains(@class,'btn-apply')]" :
                "child::div[@class='panel-body']/descendant::a[contains(@class,'apply-now')]");

        WebElement applyBtn = firstDataList.findElement(applyBtnLocator);
        bankSiteRedirectUrl = applyBtn.getAttribute("data-redirect-url");
        log.info("redirect URL: " + bankSiteRedirectUrl);
        clickElement(applyBtn);
    }

    @Then("verify redirect site")
    public void verifyRedirectSite() throws PendingException {
        seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(2));

        List<String> tabHandles = new ArrayList<>(getDriver().getWindowHandles());
        log.info("tabHandle size: " + tabHandles.size());
        getDriver().switchTo().window(tabHandles.get(1));

        seleniumUtil.verifyPageUrl("https://www.citibank.co.th/", SeleniumUtil.VerifyMethod.SOFT);
        log.info("redirect URL: " + bankSiteRedirectUrl);

        getDriver().switchTo().window(tabHandles.get(0));
        assertURL(getBaseUrl() + URLSlugs.CC_SUCCESS);
    }

    private WebElement checkAndGetDataListWithIndex(String cardListXpath, int index) {
        waitUntilElementVisible(By.xpath(cardListXpath));
        List<WebElement> creditCardDataList = getDriver().findElements(By.xpath(cardListXpath));
        //verify there is any cc data to select apply button on
        assertNotEquals(0, creditCardDataList.size());
        assertTrue(creditCardDataList.size() > index);
        return creditCardDataList.get(index);
    }
}
