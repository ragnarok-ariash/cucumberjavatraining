package stepdefinitions.th;

import enums.DisplayType;
import pageobjects.THPIForm;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@Slf4j
public class PLSubmitSteps extends AbstractSteps {

    @And("Click on Personal Loans Comparison button on subsequent page")
    public void clickOnPersonalLoansComparisonButtonOnSubsequentPage() throws PendingException {
        //verify page url to ensure function is done in correct page
        assertEquals(getBaseUrl() + "/personal-loan", getDriver().getCurrentUrl());
        log.info("curl: " + getDriver().getCurrentUrl());
        clickElementByLocator(By.cssSelector(".btn"));
    }

    @And("Verify the Personal Loans funnel page appears")
    public void verifyThePersonalLoansFunnelPageAppears() throws PendingException {
        verifyPageUrl("/personal-loan/loan-details", SeleniumUtil.VerifyMethod.HARD);
    }

    @And("select {string} as career question with click the dropdown")
    public void selectAsCareerQuestionWithClickTheDropdown(String employerStatus) throws PendingException {
        selectDropdownWithValueByLocator(employerStatus, By.xpath("//select[@id='employer_status']"));
    }

    @And("select {string} as Working age question")
    public void selectAsWorkingAgeQuestion(String workingAge) throws PendingException {
        clickElementByLocator(By.xpath("//label[contains(.,'" + workingAge + "')]"));
    }

    @And("fill {string} in Loan amount needed")
    public void fillInLoanAmountNeeded(String loanAmount) throws PendingException {
        enterInputWithTextByLocator(loanAmount, By.id("loan-amount"));
    }

    @And("select {string} as Loan borrowing period with click the dropdown")
    public void selectAsLoanBorrowingPeriodWithClickTheDropdown(String loanTenure) throws PendingException {
        selectDropdownWithValueByLocator(loanTenure, By.id("loan-tenure"));
    }

    @And("fill {string} in Monthly income field")
    public void fillInMonthlyIncomeField(String monthlyIncome) throws PendingException {
        enterInputWithTextByLocator(monthlyIncome, By.id("monthly-income"));
    }

    @And("Click on To Proceed button then wait for it to change to the Loan Result page")
    public void clickOnToProceedButtonThenWaitForItToChangeToTheLoanResultPage() throws PendingException {
        clickElementByLocator(By.id("button-submit-loan"));
    }

    @And("click Register Now on any data in the given list")
    public void clickRegisterNowOnAnyDataInTheGivenList() throws PendingException {
        String cardListXpath = "//div[@id='list-data']/child::*";
        waitUntilElementVisible(By.xpath(cardListXpath));
        List<WebElement> creditCardDataList = getDriver().findElements(By.xpath(cardListXpath));
        //verify there is any cc data to select apply button on
        assertNotEquals(0, creditCardDataList.size());
        WebElement firstDataList = creditCardDataList.get(0);

        By applyBtnLocator = By.xpath(globalManager.display == DisplayType.DESKTOP ?
                ".//div[@class='data-box panel panel-default card-product sponsored-border']//div[@class='hidden-xs hidden-sm text-center']/button[@class='btn btn-warning btn-apply btn-lg']" :
                ".//div[@class='data-box panel panel-default card-product sponsored-border']//div[@class='col-xs-12 col-sm-12']//button[@class='btn btn-warning btn-apply btn-lg']");

        WebElement applyBtn = firstDataList.findElement(applyBtnLocator);
        log.info("redirect URL: " + applyBtn.getAttribute("data-redirect-url"));
        clickElement(applyBtn);
    }

    @And("Check I agree with Terms and conditions and privacy policy")
    public void checkIAgreeWithTermsAndConditionsAndPrivacyPolicy() throws PendingException {
        THPIForm.checkAgreementPL(getDriver());
    }

    @Then("verify loan redirect site")
    public void verifyLoanRedirectSite() throws PendingException {
        seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(2));

        List<String> tabHandles = new ArrayList<>(getDriver().getWindowHandles());
        log.info("tabHandle size: " + tabHandles.size());
        getDriver().switchTo().window(tabHandles.get(1));

        seleniumUtil.verifyPageUrl("https://www.citibank.co.th/citi/personalloan/th/apply/plonline-campaign/index.html", SeleniumUtil.VerifyMethod.SOFT);
        log.info("redirect URL: " + getDriver().getCurrentUrl());

        getDriver().switchTo().window(tabHandles.get(0));
        assertURL(getBaseUrl() + "/personal-loan/application-success");
    }
}
