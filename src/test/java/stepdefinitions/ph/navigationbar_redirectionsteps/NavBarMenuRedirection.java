package stepdefinitions.ph.navigationbar_redirectionsteps;

import enums.DeploymentType;
import enums.DisplayType;
import globalmanagers.GlobalManager;
import globalmanagers.URLSlugs;
import pageobjects.ph.PHHomePage;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class NavBarMenuRedirection extends AbstractSteps {
    PHHomePage homePage;

    @Autowired
    public NavBarMenuRedirection(GlobalManager globalManager, SeleniumUtil seleniumUtil) {
        this.globalManager = globalManager;
        this.seleniumUtil = seleniumUtil;
        this.homePage = loadPageComponent(PHHomePage.class);
    }

    // CI Nav Bar Menu Redirection
    @When("Hover pointer over Car Insurance menu in the navigation bar")
    public void hoverCINavBarMenu() throws PendingException, InterruptedException {
        homePage.hoverNavBarMenu("CI", getDriver());
    }

    @When("Click on Car Insurance dropdown option")
    public void clickCIDropDownMenu() throws PendingException {
        if (globalManager.display == DisplayType.MOBILE)
            clickElementByLocator(By.xpath("//li[contains(@class,'dropdown') and contains(@class, 'dropdown-large') and contains(@class, 'open')]/ul[@class='dropdown-menu dropdown-menu-large row']//a[.='Car Insurance']"));
        else
            clickElement(homePage.CIDropDownMenu);
    }

    @Then("Verify Car Insurance page is displayed")
    public void verifyCIPageIsDisplayed() throws PendingException {
        verifyPageUrl(URLSlugs.CI, SeleniumUtil.VerifyMethod.HARD);
        log.info("curl: " + getDriver().getCurrentUrl());
        waitUntilElementVisible(By.xpath("//a[@type='button' and contains(.,'Get a free quote')]"));
    }

    // CC Nav Bar Menu Redirection
    @When("Hover pointer over Credit Card menu in the navigation bar")
    public void hoverCCNavBarMenu() throws PendingException, InterruptedException {
        homePage.hoverNavBarMenu("CC", getDriver());
    }

    @When("Click on Credit Card dropdown option")
    public void clickCCDropDownMenu() throws PendingException {
        if (globalManager.display == DisplayType.MOBILE)
            clickElementByLocator(By.xpath("//li[contains(@class,'dropdown') and contains(@class, 'dropdown-large') and contains(@class, 'open')]//li[@class='col-md-3 col-sm-3 first-col-menu']//a[contains(.,'Credit Cards')]"));
        else
            clickElement(homePage.CCDropDownMenu);
    }

    @Then("Verify Credit Card page is displayed")
    public void verifyCCPageIsDisplayed() throws PendingException {
        verifyPageUrl(URLSlugs.CC, SeleniumUtil.VerifyMethod.HARD);
        log.info("curl: " + getDriver().getCurrentUrl());
        waitUntilElementVisible(By.xpath("//a[contains(.,'Compare Credit Cards')]"));
    }

    // PL Nav Bar Menu Redirection
    @When("Hover pointer over Loans menu in the navigation bar")
    public void hoverPLNavBarMenu() throws PendingException, InterruptedException {
        homePage.hoverNavBarMenu("PL", getDriver());
    }

    @When("Click on Personal Loans dropdown option")
    public void clickPLDropDownMenu() throws PendingException {
        if (globalManager.display == DisplayType.MOBILE)
            clickElementByLocator(By.xpath("//li[contains(@class,'dropdown') and contains(@class, 'dropdown-large') and contains(@class, 'open')]//a[contains(.,'Personal Loans')]"));
        else
            homePage.PLDropDownMenu.click();
    }

    @Then("Verify Personal Loans page is displayed")
    public void verifyPLPageIsDisplayed() throws PendingException {
        verifyPageUrl(URLSlugs.PL, SeleniumUtil.VerifyMethod.HARD);
        log.info("curl: " + getDriver().getCurrentUrl());
        waitUntilElementVisible(By.xpath("//span[.='Compare Personal Loans']"));
    }

    // GDP Nav Bar Menu Redirection
    @When("Hover pointer over Gadget Protect menu in the navigation bar")
    public void hoverGDPNavBarMenu() throws PendingException, InterruptedException {
        homePage.hoverNavBarMenu("GDP", getDriver());
    }

    @When("Click on Gadget Protect dropdown option")
    public void clickGDPDropDownMenu() throws PendingException {
        if (globalManager.display == DisplayType.MOBILE)
            clickElementByLocator(By.xpath("//li[contains(@class,'dropdown') and contains(@class, 'dropdown-large') and contains(@class, 'open')]/ul[@class='dropdown-menu dropdown-menu-large row']//a[contains(.,'Gadget Protect')]"));
        else
            clickElement(homePage.GDPDropDownMenu);
    }

    @Then("Verify Gadget Protect page is displayed")
    public void verifyGDPPageIsDisplayed() throws PendingException {
        verifyPageUrl(URLSlugs.GDP, SeleniumUtil.VerifyMethod.HARD);
        log.info("curl: " + getDriver().getCurrentUrl());
        waitUntilElementVisible(By.xpath(globalManager.display == DisplayType.DESKTOP ?
                "//div[@class='ctas_hero']/a[.=\"Let's Do It\"]" :
                "//div[@class='ctas_hero mmx-margin-top-1']//a[.=\"Let's Do It\"]"));
    }

    // Contact Us Navigation bar
    @When("Click on Talk To Us navigation bar menu")
    public void clicKTalkToUsNavBarMenu() throws PendingException, InterruptedException {
        homePage.hoverNavBarMenu("talkToUs", getDriver());
        waitUntilElementVisible(By.xpath(globalManager.display == DisplayType.DESKTOP ?
                "//ul[@class='dropdown-menu']" :
                "//div[@id='contact']"));
    }

    @Then("Verify Talk To Us Section is displayed")
    public void verifyTalkToUsUsIsDisplayed() throws PendingException {
        String scheduleTime = globalManager.deployEnvironment == DeploymentType.PROD ?
                "'Mon - Fri 9:00 AM - 6:00 PM'" :
                "'Mon - Fri 9:00am - 8:00pm'";

        if (globalManager.display == DisplayType.DESKTOP) {
            waitUntilElementVisible(By.xpath("//li[@class='row col-sm-6']//b[contains(.,'Landline:')]"));
            waitUntilElementVisible(By.xpath("//li[@class='row col-sm-6']//b[contains(.,'Globe:')]"));
            waitUntilElementVisible(By.xpath("//li[@class='col-sm-6']//b[contains(.,'Smart:')]"));
            waitUntilElementVisible(By.xpath("//a[contains(.," + scheduleTime + ")]"));
        } else {
            waitUntilElementVisible(By.xpath("//div[@class='modal-dialog']"));
            waitUntilElementVisible(By.xpath("//div[@class='modal-body']//b[contains(.,'Landline:')]"));
            waitUntilElementVisible(By.xpath("//div[@class='modal-body']//b[contains(.,'Globe:')]"));
            waitUntilElementVisible(By.xpath("//div[@class='modal-body']//b[contains(.,'Smart:')]"));
            waitUntilElementVisible(By.xpath("//div[@class='modal-body']//li[contains(.," + scheduleTime + ")]"));
        }

    }
}