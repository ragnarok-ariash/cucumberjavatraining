package stepdefinitions.ph;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import globalmanagers.URLSlugs;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;
import pageobjects.ph.AboutUsPage;
import pageobjects.ph.PHHomePage;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Slf4j
public class AboutUsTest extends AbstractSteps {
    private PHHomePage homePage;
    private AboutUsPage aboutUsPage;

    @Autowired
    public AboutUsTest(GlobalManager globalManager, SeleniumUtil seleniumUtil) {
        this.globalManager = globalManager;
        this.seleniumUtil = seleniumUtil;
        homePage = loadPageComponent(PHHomePage.class);
        aboutUsPage = loadPageComponent(AboutUsPage.class);
    }

    // About Us nav bar redirection
    @When("Click on About Us navigation bar menu")
    public void clickAboutUsNavBarMenu() throws PendingException, InterruptedException {
        if (globalManager.display == DisplayType.MOBILE) 
            homePage.openMobileNavbar();
        
        clickElement(homePage.aboutUsNavBarMenu);
    }

    @Then("Verify About page is displayed")
    public void verifyAboutUsIsDisplayed() throws PendingException {
        waitUntilFinishRedirectToURL(getBaseUrl() + URLSlugs.ABOUTUS);
        //Verify About Us Page is displayed
        assertEquals(getBaseUrl() + URLSlugs.ABOUTUS, getDriver().getCurrentUrl());
        log.info("curl: " + getDriver().getCurrentUrl());
        waitUntilElementVisible(By.xpath("//li[.='ABOUT US']"));
    }

    @When("Click on Our Team")
    public void clickOnOurTeam() throws PendingException {
        clickOnFooterTab(aboutUsPage.ourTeamTab, aboutUsPage.ourTeamTabContent);
    }

    @When("Click on How we earn")
    public void clickOnHowWeEarn() throws PendingException {
        clickOnFooterTab(aboutUsPage.howWeEarnTab, aboutUsPage.howWeEarnTabContent);
    }

    @When("Click on Press")
    public void clickOnPress() throws PendingException {
        clickOnFooterTab(aboutUsPage.pressTab, aboutUsPage.pressTabContent);
    }

    @When("Click on Careers")
    public void clickOnCareers() throws PendingException {
        clickOnFooterTab(aboutUsPage.careerTab, aboutUsPage.careerTabContent);
    }

    @Then("Verify Our Team navigation page is displayed")
    public void verifyOurTeamNavigationPageIsDisplayed() throws PendingException {
        String expectedText = "";
        switch (globalManager.deployEnvironment) {
            case DEV:
                expectedText = "Moneymax is part of CompareAsiaGroup, an organization that operates online comparison platforms for financial, telco, and utility products across Asia.";
                break;
            case STAGING:
                expectedText = "Moneymax is part of CompareAsiaGroup, an organization that operates online comparison platforms across Southeast Asia.";
                break;
            case PROD:
                expectedText = "Moneymax is part of CompareAsiaGroup, an organization that operates online comparison platforms across Southeast Asia.";
        }
        verifyAboutUsFooterNavigation(aboutUsPage.ourTeamTab, aboutUsPage.ourTeamTabContent, expectedText);
    }

    @Then("Verify How we earn navigation page is displayed")
    public void verifyHowWeEarnNavigationPageIsDisplayed() throws PendingException {
        verifyAboutUsFooterNavigation(aboutUsPage.howWeEarnTab, aboutUsPage.howWeEarnTabContent,
                "Our service is and will always be free for our users.");
    }

    @Then("Verify Press navigation page is displayed")
    public void verifyPressNavigationPageIsDisplayed() throws PendingException {
        verifyAboutUsFooterNavigation(aboutUsPage.pressTab, aboutUsPage.pressTabContent,
                null);
    }

    @Then("Verify Careers navigation page is displayed")
    public void verifyCareersNavigationPageIsDisplayed() throws PendingException {
        String expectedText = "";
        switch (globalManager.deployEnvironment) {
            case DEV:
                expectedText = "We're hiring!";
                break;
            case STAGING:
                expectedText = "Be part of the Moneymax Team!";
                break;
            case PROD:
                expectedText = "Be part of the Moneymax Team!";
        }
        verifyAboutUsFooterNavigation(aboutUsPage.careerTab, aboutUsPage.careerTabContent,
                expectedText);
    }

    private void clickOnFooterTab(WebElement tab, WebElement tabContent) {
        assertEquals(getBaseUrl() + URLSlugs.ABOUTUS, getDriver().getCurrentUrl());
        clickElementUsingScript(tab);
        waitUntilElementVisible(tabContent);
        log.info("footer tab parent tag: "+tab.findElement(By.xpath("./..")).getTagName());
        log.info("parent content: "+tab.findElement(By.xpath("./..")).getText());
        log.info("parent class: "+tab.findElement(By.xpath("./..")).getAttribute("class"));
        waitUntilAttributeElementTobe(tab.findElement(By.xpath("./..")), "class", "active");
        waitUntilAttributeElementContains(tabContent, "class", "active");
    }

    private void verifyAboutUsFooterNavigation(WebElement tab, WebElement tabContent, String expectedHeaderText) {
        verifyPageUrl( URLSlugs.ABOUTUS, SeleniumUtil.VerifyMethod.SOFT);
        if (globalManager.display == DisplayType.MOBILE)
            scrollElementToBelowNavbarHeader(tab);
        else
            scrollElementToMiddle(tab);

        assertEquals(tab.findElement(By.xpath("./..")).getAttribute("class"), "active");
        assertTrue(tabContent.getAttribute("class").contains("active"));

        if (expectedHeaderText != null) {
            WebElement tabHeader = tabContent.findElement(By.tagName("h4"));
            seleniumUtil.waitFor(ExpectedConditions.textToBePresentInElement(tabHeader, expectedHeaderText));
            assertEquals(expectedHeaderText, tabHeader.getText());
        }

    }

}
