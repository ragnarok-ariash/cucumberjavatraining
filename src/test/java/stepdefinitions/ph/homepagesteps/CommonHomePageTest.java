package stepdefinitions.ph.homepagesteps;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import globalmanagers.URLSlugs;
import pageobjects.ph.PHHomePage;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;

public class CommonHomePageTest extends AbstractSteps {
    private PHHomePage phHomePage;

    @Autowired
    public CommonHomePageTest(GlobalManager globalManager, SeleniumUtil seleniumUtil) {
        this.globalManager = globalManager;
        this.seleniumUtil = seleniumUtil;
        this.phHomePage = loadPageComponent(PHHomePage.class);
    }

    @When("Scroll into More menu in the Moneymax footer")
    public void scrollDownOverMoreMenuInTheMoneymaxFooter() throws PendingException, InterruptedException {
        if (globalManager.display == DisplayType.MOBILE) {
            phHomePage.openMobileFooterMoreMenu(seleniumUtil);
            waitUntilElementVisible(phHomePage.privacyPolicyMenuMobile);
            scrollIntoElement(phHomePage.privacyPolicyMenuMobile);
        } else
            scrollIntoElement(phHomePage.privacyPolicyMenu);
    }

    @When("Click on Privacy Policy")
    public void clickOnPrivacyPolicy() throws PendingException {
        clickHomePageMenu(phHomePage.privacyPolicyMenu, phHomePage.privacyPolicyMenuMobile, URLSlugs.PRIVACY_POLICY);
    }

    @When("Click on Terms & Conditions")
    public void clickOnTermsConditions() throws PendingException {
        clickHomePageMenu(phHomePage.termConditionMenu, phHomePage.termConditionMenuMobile, URLSlugs.TERM_AND_CONDITION);
    }

    @Then("Verify Privacy Policy page is displayed")
    public void verifyPrivacyPolicyPageIsDisplayed() throws PendingException {
        String expectedText;
        switch (globalManager.deployEnvironment) {
            case STAGING:
                expectedText = "'Moneymax.ph Privacy Policy'";
                break;
            case DEV:
                expectedText = "'Moneymax.ph Privacy Policy'";
                break;
            case PROD:
            default:
                expectedText = "'Moneymax Privacy Policy'";
                break;
        }
        verifyUrlAndElement(URLSlugs.PRIVACY_POLICY, By.xpath("//h2[contains(.," + expectedText + ")]"));
    }

    @Then("Verify Terms & Conditions page is displayed")
    public void verifyTermsConditionsPageIsDisplayed() throws PendingException {
        verifyUrlAndElement(URLSlugs.TERM_AND_CONDITION, By.xpath("//h2[contains(.,'Moneymax Terms and Conditions')]"));
    }

    private void verifyUrlAndElement(String expectedUrlSegment, By locator) {
        assertURLWithOptionalTrailingSlash(getBaseUrl() + expectedUrlSegment);
        waitUntilElementVisible(locator);
    }

    private void clickHomePageMenu(WebElement desktopVersionElement, WebElement mobileVersionElement, String expectedUrlSegment) {
        clickElement(globalManager.display == DisplayType.DESKTOP ? desktopVersionElement : mobileVersionElement);
        waitUntilFinishRedirectToURLWithOptionalTrailingSlash(getBaseUrl() + expectedUrlSegment);
    }
}
