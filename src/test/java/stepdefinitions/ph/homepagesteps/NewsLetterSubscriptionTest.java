package stepdefinitions.ph.homepagesteps;

import globalmanagers.GlobalManager;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import pageobjects.ph.ArticlePage;
import pageobjects.ph.PHHomePage;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;

import java.util.Map;

public class NewsLetterSubscriptionTest extends AbstractSteps {
    PHHomePage homePage;
    ArticlePage articlePage;

    @Autowired
    public NewsLetterSubscriptionTest(GlobalManager globalManager, SeleniumUtil seleniumUtil) {
        this.globalManager = globalManager;
        this.seleniumUtil = seleniumUtil;
        this.homePage = loadPageComponent(PHHomePage.class);
        this.articlePage = loadPageComponent(ArticlePage.class);
    }

    @When("Verify newsletter subscription is displayed")
    public void verifyNewsletterSection() throws PendingException {
        scrollElementToMiddle(homePage.newsletterSection);
        waitUntilElementVisible(homePage.newsletterSection);
        verifyElementContent(Map.ofEntries(
                Map.entry(By.xpath("//p[@class='subscribe-widget-content1']"), "Never miss out on promos and finance tips!")
        ));
     }

    @When("Submit a valid email address")
    public void clickCCArticleCategoryOption() throws PendingException {
        if (homePage.isCookiePolicyDisplayed(getDriver())) {
            clickElement(homePage.cookieAcceptBtn);
            waitUntilElementGone(homePage.cookiePolicyWrapper);
        }
        enterInputWithTextByElement("mariztest2020@gmail.com", homePage.emailInputField);
        clickElement(homePage.subscribeButton);
    }

    @When("Submit {string} as an email address on newsletter subscription in All Article page")
    public void subScribeEmail(String emailAddress) throws PendingException {
        waitUntilElementVisible(articlePage.emailInputForSubscription);

        scrollElementToMiddle(articlePage.emailInputForSubscription);
        enterInputWithTextByElement(emailAddress, articlePage.emailInputForSubscription);

        clickElement(articlePage.emailSubscriptionBtn);
    }

    @Then("Newsletter subscription successful message is displayed")
    public void verifySuccessfulNewsLetterSubscriprion() throws PendingException {
        waitUntilElementVisible(homePage.newsLetterSuccessfulMessage);
//        if (elementExist(By.xpath("//p[.='Thank you for subscribing to the moneymax newsletter']")))
//            waitUntilElementGone(homePage.newsLetterSuccessfulMessage);
    }
}


