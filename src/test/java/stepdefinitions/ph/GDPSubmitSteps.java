package stepdefinitions.ph;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import globalmanagers.URLSlugs;
import pageobjects.ph.GDPForm;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GDPSubmitSteps extends AbstractSteps {
    private GDPForm gdpForm;

    @Autowired
    public GDPSubmitSteps(GlobalManager globalManager, SeleniumUtil seleniumUtil) {
        this.globalManager = globalManager;
        this.seleniumUtil = seleniumUtil;
        this.gdpForm = loadPageComponent(GDPForm.class);
    }

    @When("Click on Let's Do It button on GDP landing page")
    public void clickLetsDoItBtn() throws PendingException {
        assertURL(getBaseUrl() + "/gadget-protect");
        clickElementByLocator(By.linkText("Let's Do It"));
        waitUntilElementVisible(By.cssSelector("#piFormModal .modal-content"));
    }

    @When("Verify Purchase Info modal form appearance")
    public void verifyModalFormAppearance() throws PendingException {
        //verify GDP Form
        verifyElementContent(Map.ofEntries(
                Map.entry(By.xpath("//p[@class='mmx-blue-text mmx-pi-mobile-h1 text-center form-title']"), "Purchase Info")
        ));

        assertTrue(gdpForm.step1Form.isDisplayed());
        assertTrue(gdpForm.purchaseDateInput.isDisplayed());
        assertTrue(gdpForm.localCheckSwitch.isDisplayed());
        assertTrue(gdpForm.nextBtn.isDisplayed());
    }

    @When("Enter Purchase Info with data {string} and {string}")
    public void enterPurchaseInfo(String date, String isDevicePurchasedInPhilippines) throws PendingException {
        waitUntilElementVisible(By.id("step1-form-pi"));
        enterInputWithTextUsingJavaScriptByElement(date, gdpForm.purchaseDateInput);
        //click outside input to dismiss datepicker UI
        clickElement(globalManager.display == DisplayType.DESKTOP ? gdpForm.formContainer : gdpForm.formMobilePurchaseInfoLabel);
        waitUntilElementGone(globalManager.display == DisplayType.DESKTOP ? gdpForm.datepickerUI : gdpForm.datepickerUIMobile);
        clickElement(isDevicePurchasedInPhilippines.equalsIgnoreCase("yes") ? gdpForm.localCheckSwitchYes : gdpForm.localCheckSwitchNo);
    }

    @When("Click on Next button on gdp form")
    public void clickOnNextBtn() throws PendingException {
        clickElement(gdpForm.nextBtn);
    }

    @When("Enter Device Info: {string}, {string}, {string}")
    public void enterDeviceInfo(String brand, String model, String price) throws PendingException {
        waitUntilElementVisible(gdpForm.step2Form);
        selectDropdownWithValueByElement(brand, gdpForm.deviceBrandDropdown);
        selectDropdownWithValueByElement(model, gdpForm.deviceModelDropdown);
        enterInputWithTextByElement(price, gdpForm.purchasePriceInput);
    }

    @When(("Fill Personal Info with First Name {string}, Last Name {string}, mobile number {string}, and Email address {string}"))
    public void fillPersonalInfo(String firstName, String lastName, String mobileNum, String emailAddress) throws PendingException {
        waitUntilElementVisible(gdpForm.step3Form);
        enterInputWithTextByElement(firstName, gdpForm.firstNameInput);
        enterInputWithTextByElement(lastName, gdpForm.lastNameInput);
        enterInputWithTextByElement(mobileNum, gdpForm.mobileNumberInput);
        enterInputWithTextByElement(emailAddress, gdpForm.emailInput);

    }

    @When("Agree on Term and Condition and Personal Data Notice of AmTrust Mobile Solutions and consent on both Moneymax Privacy Policy and marketting offers")
    public void checkAllCheckboxAgreement() throws PendingException {
        List<WebElement> agreementCheckboxes = getDriver().findElements(By.xpath("//*[@id='policy-consent']/descendant::input"));
        agreementCheckboxes.stream()
                .filter(elem -> !elem.isSelected())
                .forEach(WebElement::click);
    }

    @When("Click on Submit button on gdp form")
    public void clickOnSubmitBtn() throws PendingException {
        assertTrue(gdpForm.submitBtn.isEnabled());
        clickElement(gdpForm.submitBtn);
        waitUntilElementGone(gdpForm.step3Form);
    }

    @When("Verify OTP Page")
    public void verifyOTPPage() throws PendingException {
        waitUntilElementVisible(globalManager.display == DisplayType.DESKTOP ? gdpForm.emailInputOTP : gdpForm.emailInputOTPMobile);
        //verify page url to ensure function is done in correct page
        assertURL(getBaseUrl() + "/gadget-protect");
        assertFalse(gdpForm.step3Form.isDisplayed());
        assertTrue((globalManager.display == DisplayType.DESKTOP ? gdpForm.featuresTable : gdpForm.featuresTableMobile).isDisplayed());
    }

    @When("Check to confirm have proof of purchase on OTP Page")
    public void checkHaveProofOfPurchase() throws PendingException {
        waitUntilElementGone(By.cssSelector(".modal-backdrop.fade"));
        clickElementByLocator(By.xpath(globalManager.display == DisplayType.DESKTOP ?
                "//div[contains(@class, 'mmx-pi-desktop')]//section[@class='phone-insurance-result']/child::section[contains(@class,'visible-lg')]/descendant::span[@class='input-checkmark']" :
                "//div[@class='mmx-pi-mobile visible-xs']//section[@class='phone_result row visible-xs']//span[@class='input-checkmark']"));
    }

    @When("Enter IMEI {string} on OTP Page")
    public void enterIMEI(String imei) throws PendingException {
        if (globalManager.display == DisplayType.MOBILE) {
            scrollElementToMiddle(gdpForm.emailInputOTPMobile);
            seleniumUtil.waitUntilElementAppearOnViewPort(gdpForm.emailInputOTPMobile);
            enterInputWithTextByElement(imei, gdpForm.emailInputOTPMobile);
        } else
            enterInputWithTextByElement(imei, gdpForm.emailInputOTP);

    }

    @When("Click Get it now button on OTP Page")
    public void clickGetItNowBtn() throws PendingException {
        clickElementByLocator(By.xpath(globalManager.display == DisplayType.DESKTOP ?
                "//div[@class='mmx-pi-desktop hidden-md hidden-xs hidden-sm']//section[@class='phone_result row visible-lg']//button[@class='btn btn-success result-pi-apply-desktop btn-get-it-now-pi']" :
                "//div[@class='mmx-pi-mobile visible-xs']//section[@class='phone_result row visible-xs']//button[@class='btn btn-success result-pi-apply-desktop btn-apply-result-pi']"));
    }

    @Then("Verify Paygate Web Payment")
    public void verifyPaymentPage() throws PendingException {
        String paygateUrl;
        switch (globalManager.deployEnvironment) {
            case DEV:
                paygateUrl = URLSlugs.GDP_PAYGATE_DEV;
                break;
            case STAGING:
                paygateUrl = URLSlugs.GDP_PAYGATE_STAGING;
                break;
            case PROD:
            default:
                paygateUrl = URLSlugs.GDP_PAYGATE_PROD;
                break;
        }
        seleniumUtil.verifyPageUrl(paygateUrl, SeleniumUtil.VerifyMethod.HARD);
    }
}
