package stepdefinitions.ph;

import enums.DisplayType;
import globalmanagers.URLSlugs;
import pageobjects.ph.PHPIForm;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CISubmitSteps extends AbstractSteps {

    @When("Click on Get a free quote button on CI landing page")
    public void clickOnFreeQuoteBtn() throws PendingException {
        //verify page url to ensure function is done in correct page
        assertURL(getBaseUrl() + URLSlugs.CI);
        clickElementByLocator(By.xpath("//a[@type='button' and contains(.,'Get a free quote')]"));
    }

    @When("Verify Car Information Page")
    public void verifyFunnelPagePresence() throws PendingException {
        verifyPageUrl(URLSlugs.CI_NEW_QUOTE, SeleniumUtil.VerifyMethod.IGNORETRAILING);
    }

    @When("Through dropdown, Enter your car details: {string}, {string}, {string}, and {string}")
    public void enterCarDetails(String carBrand, String carModel, String carYear, String carVariant) throws PendingException {
        if (globalManager.display == DisplayType.DESKTOP) {
            selectDropdownWithValueByLocator(carBrand, By.cssSelector("#car-select[data-selector-id]"));
            selectDropdownWithValueByLocator(carModel, By.cssSelector("#car_type[data-selector-id]"));
            selectDropdownWithValueByLocator(carYear, By.cssSelector("#car_year[data-selector-id]"));
            selectDropdownWithValueByLocator(carVariant, By.cssSelector("#trim_car[data-selector-id]"));
        } else {
            //select car brand
            seleniumUtil.waitFor(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//ul[@id='car-brand']/child::label"), 1));
            String carBrandXpath = "//ul[@id='car-brand']/child::label[contains(.,'" + carBrand + "')]";
            //check if elements exist on list, otherwise use dropdown
            if (elementExist(By.xpath(carBrandXpath)))
                clickElementByLocator(By.xpath(carBrandXpath));
            else
                selectDropdownWithValueByLocator(carBrand, By.cssSelector(".car-select2-mobile"));

            By carModelLabel = By.xpath("//div[@id='step2']//label[contains(.,'Car Model')]");
            seleniumUtil.waitFor(ExpectedConditions.and(
                    ExpectedConditions.visibilityOf(getMobileNavbarHeader()),
                    ExpectedConditions.visibilityOfElementLocated(carModelLabel)));
            waitUntilElementScrolledToNavbarGetSpecificYOffset(getDriver().findElement(carModelLabel), 15);

            //select car model
            seleniumUtil.waitFor(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//ul[@class='list-group radio-button visible-xs car-type2-mobile-list']/child::label"), 1));
            String carModelXpath = "//ul[@class='list-group radio-button visible-xs car-type2-mobile-list']/child::label[contains(.,'" + carModel + "')]";
            //check if elements exist on list, otherwise use dropdown
            if (elementExist(By.xpath(carModelXpath))) {
                clickElementByLocator(By.xpath(carModelXpath));
            } else
                selectDropdownWithValueByLocator(carModel, By.cssSelector(".car-type2-mobile"));

            By carYearLabel = By.xpath("//div[@id='step3']//label[contains(.,'Car Year')]");
            seleniumUtil.waitFor(ExpectedConditions.and(
                    ExpectedConditions.visibilityOf(getMobileNavbarHeader()),
                    ExpectedConditions.visibilityOfElementLocated(carYearLabel)));
            waitUntilElementScrolledToNavbarGetSpecificYOffset(getDriver().findElement(carYearLabel), 180);

            selectDropdownWithValueByLocator(carYear, By.xpath("//select[@id='car_year']"));
            selectDropdownWithValueByLocator(carVariant, By.xpath("//select[@id='trim_car']"));

        }
    }

    @When("Enter your car usage information by select {string} as What is the primary use of your car and {string} as Are you paying off a car loan and {string} as Include coverage for Acts of Nature and {string} as Do you have an existing car insurance")
    public void enterCarUsageInfo(String primaryUse, String payingLoan, String shallIncludeCoverageForActOfNature, String haveExistingCarInsurance) throws PendingException {
        if (globalManager.display == DisplayType.DESKTOP) {
            clickElementByLocator(By.xpath("//div[contains(@class, 'row') and contains(@class, 'car')]//label[contains(text(), '" + primaryUse + "')]"));
            clickElementByLocator(By.xpath("//div[@id = 'loan']//label[contains(text(), '" + payingLoan + "')]"));
            clickElementByLocator(By.xpath("//div[contains(@class, 'row') and contains(@class, 'frame-coverage')]//label[contains(text(), '" + shallIncludeCoverageForActOfNature + "')]"));
            clickElementByLocator(By.xpath("//div[@id = 'car_insurance']//label[contains(text(), '" + haveExistingCarInsurance + "')]"));
        } else {
            clickElementByLocator(By.xpath("//label[.='" + primaryUse + "']"));
            clickElementByLocatorUsingScript(By.xpath("//div[@id='loan']//label[.='" + payingLoan + "']"));
            clickElementByLocatorUsingScript(By.xpath("//div[@class='row left-detail frame-coverage hidden-mobile']/descendant::label[.='" + shallIncludeCoverageForActOfNature + "']"));
            clickElementByLocatorUsingScript(By.xpath("//div[@id='car_insurance']/descendant::label[.='" + haveExistingCarInsurance + "']"));
        }

    }

    @When("Click on Get result button on Car Information Page")
    public void clickOnGetResultBtn() throws PendingException {
        clickElementByLocatorUsingScript(By.xpath("//button[@id = 'button-submit-ci']"));
    }

    @When("Click Apply Now button on any car insurance data list")
    public void clickOnApplyNowBtn() throws PendingException {
        //check invisibility of PI Form
        PHPIForm PIForm = loadPageComponent(PHPIForm.class);
        waitUntilElementGone(PIForm.formBody);

        seleniumUtil.waitFor(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//div[@id='list-data']/child::*"), 0));
        clickElementByLocator(By.xpath(globalManager.display == DisplayType.DESKTOP ?
                "//div[@id='list-data']/div[1]//div[@class='hidden-xs hidden-sm text-center']/button[@class='btn btn-lg btn-apply ci-product-button']" :
                "//div[@id='list-data']/div[1]//div[@class='row visible-xs visible-sm']//button[@class='btn btn-lg btn-apply ci-product-button']"));
    }

    @Then("Verify CI application success page")
    public void checkThankyouPage() throws PendingException {
        verifyPageUrl(URLSlugs.CI_SUCCESS, SeleniumUtil.VerifyMethod.HARD);
//        wait.until(ExpectedConditions.textMatches(By.tagName("body"), Pattern.compile("thank you",CASE_INSENSITIVE)));
    }

    @When("Click on Get Quote button on CI partner page")
    public void clickOnGetQuoteButton() throws PendingException {
        clickElementByLocator(By.linkText("Get Quote"));
        waitUntilFinishRedirectToURL(getBaseUrl() + URLSlugs.CI_NEW_QUOTE);
    }

    @When("Click on Submit Claim button")
    public void clickOnSubmitClaimButton() throws PendingException {
        clickElementByLocator(By.linkText("Submit Claim"));
    }

}
