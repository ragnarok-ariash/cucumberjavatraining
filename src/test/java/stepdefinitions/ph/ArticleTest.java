package stepdefinitions.ph;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import globalmanagers.URLSlugs;
import pageobjects.ph.ArticlePage;
import pageobjects.ph.PHHomePage;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static org.junit.Assert.*;

@Slf4j
public class ArticleTest extends AbstractSteps {
    PHHomePage phHomePage;
    ArticlePage articlePage;
    private String articleLink;
    private String articleTitle;

    @Autowired
    public ArticleTest(GlobalManager globalManager, SeleniumUtil seleniumUtil) {
        this.globalManager = globalManager;
        this.seleniumUtil = seleniumUtil;
        phHomePage = loadPageComponent(PHHomePage.class);
        articlePage = loadPageComponent(ArticlePage.class);
    }

    // Article Main Page nav bar redirection
    @When("Hover pointer over articles menu in the navigation bar")
    public void hoverArticleNavBarMenu() throws PendingException, InterruptedException {
        phHomePage.hoverNavBarMenu("articles", getDriver());
    }

    @When("Click on All Articles dropdown option")
    public void clickArticlesDropDownMenu() throws PendingException {
        clickOnHomepageMenu(phHomePage.allArticlesDropDownMenu, URLSlugs.ALL_ARTICLE);
    }

    // CI Article Category Redirection
    @When("Click on Car Insurance article category drop down option")
    public void clickCIArticleCategoryOption() throws PendingException {
        clickOnHomepageMenu(phHomePage.carInsuranceArticleCategory, URLSlugs.ARTICLE_CI);
    }

    // CC Article Category Redirection
    @When("Click on Credit Card article category drop down option")
    public void clickCCArticleCategoryOption() throws PendingException {
        clickOnHomepageMenu(phHomePage.creditCardArticleCategory, URLSlugs.ARTICLE_CC);
    }

    // PL Article Category Redirection
    @When("Click on Personal Loan article category drop down option")
    public void clickPLArticleCategoryOption() throws PendingException {
        clickOnHomepageMenu(phHomePage.personalLoanArticleCategory, URLSlugs.ARTICLE_PL);
    }

    // Personal Finance Article Category Redirection
    @When("Click on Personal Finance article category drop down option")
    public void clickPersonalFinanceArticleCategoryOption() throws PendingException {
        clickOnHomepageMenu(phHomePage.personalFinanceArticleCategory, URLSlugs.ARTICLE_PERSONAL_FINANCE);
    }

    // GDP Article Category Redirection
    @When("Click on Gadget Protect article category drop down option")
    public void clickGDPArticleCategoryOption() throws PendingException {
        clickOnHomepageMenu(phHomePage.gadgetProtectArticleCategory, URLSlugs.ARTICLE_GADGET_PROTECT);
    }

    // Government Services Category Redirection
    @When("Click on Government Services article category drop down option")
    public void clickGovernmentArticleCategoryOption() throws PendingException {
        //There is no government service menu under article category dropdown on mobile version,
        //therefore need to access it through footer menu: more > gov. services articles
        if (globalManager.display == DisplayType.MOBILE) {
            phHomePage.openMobileFooterMoreMenu(seleniumUtil);
            clickElementUsingScript(phHomePage.govServiceArticleMenuMobile);
        } else
            clickOnHomepageMenu(phHomePage.governmentArticleCategory, URLSlugs.ARTICLE_GOV_SERVICE);
    }

    // Lifestyle Article Category Redirection
    @When("Click on Lifestyle article category drop down option")
    public void clickLifestyleArticleCategoryOption() throws PendingException {
        clickOnHomepageMenu(phHomePage.lifestyleArticleCategory, URLSlugs.ARTICLE_LIFESTYLE);
    }

    //open article by title
    @When("Click on any articles title form the article list")
    public void clickOnAnyArticlesTitleFormTheArticleList() throws PendingException {
        verifyArticleUrlAndWaitForArticleList();
        clickOnArticleTitle(articlePage.listData.get(0), true);
    }

    //open article by image
    @When("Click on any articles image form the article list")
    public void clickOnAnyArticlesImageFormTheArticleList() throws PendingException {
        verifyArticleUrlAndWaitForArticleList();
        clickOnArticleTitle(articlePage.listData.get(0), false);
    }

    @Then("Verify All Articles page is displayed")
    public void verifyArticlesPageIsDisplayed() throws PendingException {
        //Verify Article Main Page is displayed

        checkForUrlAndVerifyArticleContent(Map.ofEntries(
                Map.entry(By.xpath("//span[@class='content1']"), "Our Blog."),
                Map.entry(By.xpath("//h2[@class='main-title']"), "All Articles")
        ), URLSlugs.ALL_ARTICLE);
    }

    @Then("Verify Car Insurance article category page is displayed")
    public void verifyCIArticleCategoryPage() throws PendingException {
        //Verify CI article category page is displayed

        checkForUrlAndVerifyArticleContent(Map.ofEntries(
                Map.entry(By.xpath("//h1[@class='content1']"), "Car Insurance"),
                Map.entry(By.xpath("//h2[@class='main-title']"), "All Articles With Category : Car Insurance")
        ), URLSlugs.ARTICLE_CI);
    }

    @Then("Verify Credit Card article category page is displayed")
    public void verifyCCArticleCategoryPage() throws PendingException {
        //Verify CC article category page is displayed

        checkForUrlAndVerifyArticleContent(Map.ofEntries(
                Map.entry(By.xpath("//h1[@class='content1']"), "Credit Card"),
                Map.entry(By.xpath("//h2[@class='main-title']"), "All Articles With Category : Credit Card")
        ), URLSlugs.ARTICLE_CC);
    }

    @Then("Verify Personal Loan article category page is displayed")
    public void verifyPLArticleCategoryPage() throws PendingException {
        //Verify PL article category page is displayed

        checkForUrlAndVerifyArticleContent(Map.ofEntries(
                Map.entry(By.xpath("//h1[@class='content1']"), "Personal Loan"),
                Map.entry(By.xpath("//h2[@class='main-title']"), "All Articles With Category : Personal Loan")
        ), URLSlugs.ARTICLE_PL);
    }

    @Then("Verify Personal Finance article category page is displayed")
    public void verifyPersonalFinanceArticleCategoryPage() throws PendingException {
        //Verify Personal Finance article category page is displayed

        checkForUrlAndVerifyArticleContent(Map.ofEntries(
                Map.entry(By.xpath("//h1[@class='content1']"), "Personal Finance"),
                Map.entry(By.xpath("//h2[@class='main-title']"), "All Articles With Category : Personal Finance")
        ), URLSlugs.ARTICLE_PERSONAL_FINANCE);
    }

    @Then("Verify Gadget Protect article category page is displayed")
    public void verifyGDPArticleCategoryPage() throws PendingException {
        //Verify Gadget Protect article category page is displayed

        checkForUrlAndVerifyArticleContent(Map.ofEntries(
                Map.entry(By.xpath("//h1[@class='content1']"), "Gadget Protect"),
                Map.entry(By.xpath("//h2[@class='main-title']"), "All Articles With Category : Gadget Protect")
        ), URLSlugs.ARTICLE_GADGET_PROTECT);
    }

    @Then("Verify Government Services article category page is displayed")
    public void verifyGovernmentArticleCategoryPage() throws PendingException {
        //Verify Government Services article category page is displayed

        checkForUrlAndVerifyArticleContent(Map.ofEntries(
                Map.entry(By.xpath("//h1[@class='content1']"), "Government Services"),
                Map.entry(By.xpath("//h2[@class='main-title']"), "All Articles With Category : Government Services")
        ), URLSlugs.ARTICLE_GOV_SERVICE);
    }

    @Then("Verify Lifestyle article category page is displayed")
    public void verifyLifestyleArticleCategoryPage() throws PendingException {
        //Verify Lifestyle article category page is displayed

        checkForUrlAndVerifyArticleContent(Map.ofEntries(
                Map.entry(By.xpath("//h1[@class='content1']"), "Lifestyle"),
                Map.entry(By.xpath("//h2[@class='main-title']"), "All Articles With Category : Lifestyle")
        ), URLSlugs.ARTICLE_LIFESTYLE);
    }

    @Then("Verify article details page is displayed")
    public void verifyArticleDetailsPageIsDisplayed() throws PendingException {
        assertNotNull(articleLink);
        assertNotNull(articleTitle);
        assertURL(articleLink);
        assertEquals(articleTitle, getDriver().findElement(By.xpath("//h1[@class='main-title']")).getText().trim());
    }

    private void clickOnHomepageMenu(WebElement menu, String destinationUrlSegment) {
        clickElement(menu);
        waitForUrlAndGivenCondition(destinationUrlSegment, ExpectedConditions.or(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='content1']")),
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[@class='main-title']"))));
    }

    private void clickOnArticleTitle(WebElement article, boolean clickOnTitle) {
        selectArticleToOpenByElementPart(article, clickOnTitle ? articlePage.getArticleTitleAsLink(article) : articlePage.getArticleImage(article));
    }

    private void verifyArticleUrlAndWaitForArticleList() {
        assertURL(getBaseUrl() + URLSlugs.ALL_ARTICLE);

        waitUntilChildElementVisible(articlePage.listContainer, By.tagName("div"));
        assertNotEquals("No article to click", 0, articlePage.listData.size());
    }

    private void selectArticleToOpenByElementPart(WebElement articleElement, WebElement articleElementPartToClick) {
        articleLink = articlePage.getArticleLink(articleElement).getAttribute("href");
        articleTitle = articlePage.getArticleTitle(articleElement).getText().trim();
        log.info("article to open, link: "+articleLink+", title: "+articleTitle);
        clickElement(articleElementPartToClick);
        waitUntilFinishRedirectToURL(articleLink);
    }

    //since article content may appear differently, any possible locator need to be checked in the form of map
    private void checkForUrlAndVerifyArticleContent(Map<By, String> elementContentMap, String pageSegment) {
        assertURL(getBaseUrl() + pageSegment);
        verifyElementContent(elementContentMap);
    }

}
