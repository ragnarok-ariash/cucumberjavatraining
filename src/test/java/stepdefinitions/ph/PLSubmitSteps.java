package stepdefinitions.ph;

import enums.DisplayType;
import globalmanagers.URLSlugs;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@Slf4j
public class PLSubmitSteps extends AbstractSteps {

    private final Map<String, String> loanNameToIDMap = Map.ofEntries(
            Map.entry("GDFI", "PH.MMX.PL.GDFI0001.GDFIOFWPersonalLoan"),
            Map.entry("EastWest", "PH.MMX.PL.EWBX0001.EastWestBankPersonalLoan"),
            Map.entry("SB Finance", "PH.MMX.PL.SBFN0001.SBFinancePersonalLoan"),
            Map.entry("TALA", "PH.MMX.PL.TALA0001.TALAPersonalLoan"),
            Map.entry("EasyRFC Multi-Purpose Loan", "PH.MMX.PL.RFCX0001.EasyRFCMultiPurposeLoan")

    );

    @When("Click on Compare Personal Loan button on PL landing page")
    public void clickOnComparePLbtn() throws PendingException {
        assertURL(getBaseUrl() + URLSlugs.PL);

        clickElementByLocator(By.xpath("//a[@class='btn btn-success btn-lg' and contains(.,'Compare Personal Loans')]"));
    }

    @When("Verify PL Funnel Page")
    public void verifyPLFunnelPagePresence() throws PendingException {
        verifyPageUrl(URLSlugs.PL_DETAILS, SeleniumUtil.VerifyMethod.HARD);
    }

    @When("Select {string} for employment status")
    public void selectEmploymentStatus(String answer) throws PendingException {
        selectDropdownWithValueByLocator(answer, By.id("employer_status"));
    }

    @When("Select {string} for how long have been in current job")
    public void timeInCurrentJob(String answer) throws PendingException {
        clickElementByLocator(By.xpath(("//*[@id='current_job']//label[@class='btn btn-default btn-job' and text() = '" + answer + "']")));
    }

    @When("Enter {string} for how much to borrow")
    public void enterLoanAmount(String answer) throws PendingException {
        enterInputWithTextByLocator(answer, By.id("loan-amount"));
    }

    @When("Select {string} for how long to return")
    public void selectLoanTenure(String answer) throws PendingException {
        selectDropdownWithValueByLocator(answer, By.id("loan-tenure"));
    }

    @When("Enter {string} for monthly income")
    public void enterMonthlyIncome(String answer) throws PendingException {
        enterInputWithTextByLocator(answer, By.id("monthly-income"));
    }

    @When("Select {string} for having an active card")
    public void selectActiveCardTime(String answer) throws PendingException {
        if (globalManager.display == DisplayType.MOBILE) {
            clickElementByLocatorUsingScript(By.xpath("//div[@id='step4_mb']//label[contains(.,'" + answer + "')]"));
        } else
            clickElementByLocator(By.xpath("//*[@id='step4']//label[contains(@data-selector-input,'active_cc_status') and contains(.,'" + answer + "')]"));

    }

    @When("Click on Get Results button in PL Funnel Page")
    public void clickGetResultsBtn() throws PendingException {
        clickElementByLocatorUsingScript(By.id("button-submit-loan"));
    }

    @When("Verify PL Result Page")
    public void verifyPLResultPage() throws PendingException {
        verifyPageUrl(URLSlugs.PL_RESULT, SeleniumUtil.VerifyMethod.HARD);
    }

    @When("Click Apply Now button on any pl data list")
    public void applyAnyPersonalLoan() throws PendingException {
        WebElement firstDataList = checkAndGetAnyDataList("//div[@id='list-data']/child::*", By.id("list-data"));

        String applyBtnXpath = globalManager.display == DisplayType.DESKTOP
                ? ".//div[@class='hidden-xs hidden-sm text-center']/button[@class='btn btn-apply btn-lg pl-product-button']"
                : ".//div[@class='row visible-xs visible-sm']//button[@class='btn btn-apply btn-lg pl-product-button']";
        WebElement applyBtn = firstDataList.findElement(By.xpath(applyBtnXpath));
        setProductRedirectUrl(applyBtn.getAttribute("data-redirect-url"));
        clickElement(applyBtn);
    }

    @When("Click {string} Product Image from loan list")
    public void clickSpecificProductImageFromLoanList(String loanName) throws PendingException {
        checkAndGetAnyDataList("//div[@id='list-data']/child::*", By.id("list-data"));

        String anchorXpath = globalManager.display == DisplayType.DESKTOP ?
                ".//div[@id='list-data']//div[@class='panel-body card-product-body']//a[@data-ppageproductid = '" + loanNameToIDMap.get(loanName) + "']" :
                ".//div[@id='list-data']//div[@class='panel-heading']//a[@data-ppageproductid = '" + loanNameToIDMap.get(loanName) + "']/img";

        waitUntilElementVisible(By.xpath(anchorXpath));
        setProductPageUrl(getDriver().findElement(By.xpath(anchorXpath)).getAttribute("href"));

        clickElementByLocator(By.xpath(anchorXpath + "/img"));
        seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(getDriver().getWindowHandles().size() + 1));
    }

    @When("Click {string} Product Name from loan list")
    public void clickProductNameFromLoanList(String loanName) throws PendingException {
        checkAndGetAnyDataList("//div[@id='list-data']/child::*", By.id("list-data"));

        String anchorXpath = globalManager.display == DisplayType.DESKTOP ?
                ".//div[@id='list-data']//div[@class='panel-heading']/a[@data-ppageproductid = '" + loanNameToIDMap.get(loanName) + "']" :
                ".//div[@id='list-data']//div[@class='panel-heading']//a[@data-ppageproductid = '" + loanNameToIDMap.get(loanName) + "']/img";

        waitUntilElementVisible(By.xpath(anchorXpath));
        setProductPageUrl(getDriver().findElement(By.xpath(anchorXpath)).getAttribute("href"));

        clickElementByLocator(By.xpath(anchorXpath + "//b"));
        seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(getDriver().getWindowHandles().size() + 1));
    }

    private WebElement checkAndGetAnyDataList(String cardListXpath, By listContainerLocator) {
        waitUntilElementVisible(By.xpath(cardListXpath));
        seleniumUtil.waitFor(ExpectedConditions.presenceOfNestedElementLocatedBy(By.id("list-data"), By.xpath(cardListXpath)));
        List<WebElement> personalLoanDataList = getDriver().findElements(By.xpath(cardListXpath));
        //verify there is any cc data to select apply button on
        assertNotEquals(0, personalLoanDataList.size());
        return personalLoanDataList.get(0);
    }

    @When("Click Apply Now button on PL partner page")
    public void clickApplyNowButtonOnPartnerPage() throws PendingException {
        By applyBtnLocator;
        switch (globalManager.deployEnvironment) {
            case DEV:
                applyBtnLocator = By.xpath("//div[contains(@class,'apply-now-button')]/a[.='Apply Now']");
                break;
            case STAGING:
                applyBtnLocator = By.xpath("//div[contains(@class,'apply-now-button')]/a[.='Apply Now']");
                break;
            case PROD:
            default:
                applyBtnLocator = By.xpath("//div[@class='col-lg-5 col-lg-offset-6 apply-now-button padding-top-2 padding-bottom-2']/a[.='Apply Now']");
                break;
        }
        waitUntilElementVisible(applyBtnLocator);
        WebElement linkBtn = getDriver().findElement(applyBtnLocator);
        setPartnerDataId(linkBtn.getAttribute("data-id"));
        log.info("partner data ID: " + getPartnerDataId());
        clickElementUsingScript(linkBtn);
        waitUntilFinishRedirectToURL(getBaseUrl() + URLSlugs.PL_DETAILS_PRODUCT + getPartnerDataId());
    }

    @When("Verify PL Funnel Page of applied partner")
    public void verifyPLFunnelPageOfAppliedPartner() throws PendingException {
        assertNotNull(getPartnerDataId());
        assertURL(getBaseUrl() + URLSlugs.PL_DETAILS_PRODUCT + getPartnerDataId());
    }

    @Then("Verify PL application success page")
    public void verifyPLApplicationSuccessPage() throws PendingException {
        verifyPageUrl(URLSlugs.PL_SUCCESS, SeleniumUtil.VerifyMethod.HARD);
    }

    @When("Click Compare Other Product on success page")
    public void clickCompareOtherProductOnSuccessPage() throws PendingException {
        clickElementByLocator(By.xpath("//span[.='Compare Other Products']"));
        waitUntilFinishRedirectToURL(getBaseUrl() + URLSlugs.PL_RESULT);
    }

    @When("Verify {string} detail product page appearance")
    public void verifyDetailProductPage(String loanName) throws PendingException {
        assertNotNull(getProductPageUrl());

        List<String> tabHandles = new ArrayList<>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabHandles.get(getDriver().getWindowHandles().size() - 1));//switch to last tab

        assertURL(getProductPageUrl());
        By applyBtnLocator = By.xpath("//button[@class='btn btn-success btn-main btn-lg btn-ppage-apply btn-apply-pl']");
        waitUntilElementVisible(applyBtnLocator);
        assertEquals(loanNameToIDMap.get(loanName), getDriver().findElement(applyBtnLocator).getAttribute("data-id"));
    }

    @When("Click Apply Now on detail product page")
    public void clickApplyNowOnPartnerPage() throws PendingException {
        clickElementByLocator(By.xpath("//button[@class='btn btn-success btn-main btn-lg btn-ppage-apply btn-apply-pl']"));
    }

}
