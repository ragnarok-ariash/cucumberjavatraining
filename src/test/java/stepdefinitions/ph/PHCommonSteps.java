package stepdefinitions.ph;

import globalmanagers.URLSlugs;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.ph.PHPIForm;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Slf4j
public class PHCommonSteps extends AbstractSteps {

    private static final Map<String, String> partnerLinkMap = Map.ofEntries(
            Map.entry("Stronghold Insurance", "/car-insurance/partners/stronghold-insurance"),
            Map.entry("Malayan Insurance", "/car-insurance/partners/malayan-insurance"),
            Map.entry("The Mercantile Insurance Corporation", "/car-insurance/partners/mercantile-insurance"),
            Map.entry("MAPFRE", "/car-insurance/partners/mapfre-insular"),
            Map.entry("CITI", "/credit-card/partners/citibank"),
            Map.entry("Maybank", "/credit-card/partners/maybank"),
            Map.entry("Metrobank", "/credit-card/partners/metrobank"),
            Map.entry("AUB", "/credit-card/partners/aub"),
            Map.entry("GDFI", "/personal-loan/partners/gdfi"),
            Map.entry("TALA", "/personal-loan/partners/tala"),
            Map.entry("EastWest", "/personal-loan/partners/eastwest-bank"),
            Map.entry("SB Finance", "/personal-loan/partners/sb-finance")
    );

    @When("Verify PI pop up form appearance")
    public void verifyPIForm() throws PendingException {
        PHPIForm PIForm = loadPageComponent(PHPIForm.class);
        waitUntilElementVisible(PIForm.formContainer);
        assertTrue(PIForm.formBody.isDisplayed());
    }

    @When("Enter Personal Information with First Name {string} and Last Name {string} and Email {string} and Phone Number {string}")
    public void fillInPIForm(String firstName, String lastName, String email, String phoneNumber) throws PendingException {
        PHPIForm PIForm = loadPageComponent(PHPIForm.class);
        waitUntilElementVisible(PIForm.formBody);

        PIForm.setElement(PIForm.firstNameInput, firstName, seleniumUtil);
        PIForm.setElement(PIForm.lastNameInput, lastName, seleniumUtil);
        PIForm.setElement(PIForm.emailInput, email, seleniumUtil);
        PIForm.setElement(PIForm.phoneInput, phoneNumber, seleniumUtil);
    }

    @When("Select I Agree to Terms and Conditions and Privacy Policy and I Consent to Moneymax to call me for their marketing offers")
    public void agreeOnTNCAndConsentOnPIForm() throws PendingException {
        PHPIForm PIForm = loadPageComponent(PHPIForm.class);
        PIForm.checkAgreement(seleniumUtil);
    }

    @When("Click on PI Submit button")
    public void clickOnPISubmitBtn() throws PendingException, URISyntaxException {
        //capture segment of current url
        setFirstUrlSegmentWhenSubmitPI(new URI(getDriver().getCurrentUrl())
                .getPath().split("/")[1]);

        PHPIForm PIForm = loadPageComponent(PHPIForm.class);
        assertTrue(PIForm.submitBtn.isEnabled());
        setNumberOfWindowWhenSubmitPI(getDriver().getWindowHandles().size());
        clickElementUsingScript(PIForm.submitBtn);
    }

    @When("Wait until PI form disappear")
    public void waitDisappearanceOfPIForm() throws PendingException {
        PHPIForm PIForm = loadPageComponent(PHPIForm.class);
        waitUntilElementGone(PIForm.formContainer);
    }

    @Then("Verify redirect site")
    public void verifyRedirectSite() throws PendingException {
        if (pageData.redirectToPartnerProduct()) {
            seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(getNumberOfWindowWhenSubmitPI() + 1));

            //switch to last tab
            getDriver().switchTo().window(new ArrayList<>(getDriver().getWindowHandles())
                    .get(getNumberOfWindowWhenSubmitPI()));//numberOfWindowWhenSubmitPI can be taken as last index, since it's 0-based
            verifyPageUrl(URLSlugs.PARTNER_PRODUCT_REDIRECT, SeleniumUtil.VerifyMethod.SOFT);
        } else
            verifyPageUrl("/" + getFirstUrlSegmentWhenSubmitPI() + URLSlugs.APPLICATION_SUCCESS, SeleniumUtil.VerifyMethod.HARD);


    }

    @Then("Close other tabs")
    public void closeOtherTabs() throws PendingException {
        List<String> tabHandles = new ArrayList<>(getDriver().getWindowHandles());
        IntStream.range(1, tabHandles.size()).forEach(value -> {
            log.info("value:" + value);
            getDriver().switchTo().window(tabHandles.get(value)).close();
            seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(tabHandles.size() - value));
        });
        getDriver().switchTo().window(tabHandles.get(0));

    }

    @When("Click on partner logo {string} on Our partners bar")
    public void clickOnPartnerLogoOnOurPartnersBar(String partner) throws PendingException, InterruptedException {
        String insuranceLogoXpath = "//a[@href='" + getBaseUrl() + partnerLinkMap.get(partner) + "']";
        seleniumUtil.waitFor(ExpectedConditions.presenceOfElementLocated(By.xpath(insuranceLogoXpath)));
        if (!getDriver().findElement(By.xpath(insuranceLogoXpath)).isDisplayed()) {
            log.info("logo not visible, need scroll");
            seleniumUtil.scrollElementWithinScrollViewIntoView(getDriver().findElement(By.xpath(insuranceLogoXpath)));
            waitUntilElementVisible(By.xpath(insuranceLogoXpath));
        }
        clickElementByLocatorUsingScript(By.xpath(insuranceLogoXpath));
    }

    @When("Verify {string} partner page")
    public void verifyPartnerPage(String partner) throws PendingException {
        verifyPageUrl(partnerLinkMap.get(partner), SeleniumUtil.VerifyMethod.HARD);
    }

    //non trailing check
    @Given("Open the browser")
    public void openTheBrowser() throws PendingException {
        assertNotNull(getDriver());
    }

    @When("visit {string} on the address bar")
    public void visitUrl(String trailingUrl) throws PendingException {
        navigateInto(trailingUrl);
        waitUntilFinishRedirectToURLWithOptionalTrailingSlash(trailingUrl);
    }

    @Then("Verify the url will display {string} on the address bar of the destination page")
    public void verifyTheUrlAsNonTraling(String nonTrailingUrl) throws PendingException {
        log.info("current URL: " + getDriver().getCurrentUrl());
        assertURL(nonTrailingUrl);
    }
}