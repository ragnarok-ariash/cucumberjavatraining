package stepdefinitions.ph;

import enums.DisplayType;
import globalmanagers.URLSlugs;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
public class CCSubmitSteps extends AbstractSteps {

    @When("Click on Compare Credit Cards button on CC landing page")
    public void clickOnCompareCCbtn() throws PendingException {
        //verify page url to ensure function is done in correct page
        assertURL(getBaseUrl() + URLSlugs.CC);
        clickElementByLocator(By.xpath("//a[@class='btn btn-success btn-lg' and contains(.,'Compare Credit Cards')]"));
    }

    @When("Verify CC Funnel Page")
    public void verifyCCFunnelPagePresence() throws PendingException {
        assertURL(getBaseUrl() + URLSlugs.CC_FUNNEL);
    }

    @When("Select {string} for Do you have an active credit card question")
    public void answerActiveCCQuestion(String answer) throws PendingException {
        clickElementByLocatorUsingScript(By.xpath("//label[@data-selector-input = 'active_cc_status' and text() = '" + answer + "']"));
    }

    @When("Select {string} for What is your employment status question")
    public void answerEmploymentStatusQuestion(String answer) throws PendingException {
        selectDropdownWithValueByLocator(answer,
                By.cssSelector(globalManager.display == DisplayType.DESKTOP ? "select#employer_status" : "select#employment_status2"));
        if (globalManager.display == DisplayType.MOBILE) {
            //wait for scroll animation
            waitUntilElementScrolledToNavbarGetSpecificYOffset(
                    getDriver().findElement(By.xpath("//div[@id='documents_required']/div[1]//button[@class='tooltip-button']")),
                    20);
        }
    }

    @When("Select {string} for Which of these documents could you provide question")
    public void answerDocumentProvideAble(String answer) throws PendingException {
        waitUntilElementVisible(By.cssSelector("#documents_required > div"));

        By elementLocator = By.xpath(globalManager.display == DisplayType.DESKTOP ?
                ".//label[contains(@class,'btn btn-job btn-size') and text() = '" + answer + "']" :
                ".//ul[@class='list-group radio-button visible-xs']/label[.='" + answer + "']");

        List<WebElement> divs = getDriver().findElements(By.cssSelector("#documents_required > div"));
        divs.stream()
                .filter(WebElement::isDisplayed)
                .forEach(div -> clickElementUsingScript(div.findElement(elementLocator)));

        if (globalManager.display == DisplayType.MOBILE)
            waitUntilElementVisible(By.id("current_job_cc"));

    }

    @When("Select {string} for How long have you been in your current job question")
    public void answerHowLongInCurrentJobQuestion(String answer) throws PendingException {
        clickElementByLocatorUsingScript(By.xpath(globalManager.display == DisplayType.DESKTOP ?
                "//*[@id='current_job_cc']/descendant::div[@class = 'hidden-xs hidden-sm']/child::label[text() = '" + answer + "']" :
                "//div[@id='current_job_cc']//ul[@class='list-group radio-button visible-xs']/label[.='" + answer + "']"));
    }

    @When("Enter {string} for What is your monthly income question")
    public void enterMonthlyIncome(String answer) throws PendingException {
        enterInputWithTextByLocator(answer, By.id("range-income"));
        seleniumUtil.waitFor(new ExpectedCondition<Boolean>() {

            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                String text = driver.findElement(By.id("range-income")).getAttribute("value");
                log.info("range-income text: " + text);
                log.info("range-income number only: " + text.replaceAll("[^\\d]", ""));
                return text.replaceAll("[^\\d]", "").equals(answer);
            }
        });
        log.info("range-income text: " + getDriver().findElement(By.id("range-income")).getAttribute("value"));
    }

    @When("Click on Submit button in CC Funnel Page")
    public void clickOnSubmitInCCFunnelPage() throws PendingException {
        clickElementByLocator(By.id("button-submit"));
    }

    @When("Verify CC Result Page")
    public void verifyCCResultPage() throws PendingException {
        //verify the url
        verifyPageUrl(URLSlugs.CC_RESULT, SeleniumUtil.VerifyMethod.HARD);
        By locator;
        String contentToCheck;
        if (globalManager.display == DisplayType.DESKTOP) {
            locator = By.xpath("//div[@class='breadcrumb-content hidden-xs text-center']//h2");
            contentToCheck = "See all credit cards in the Philippines ?";
        } else {
            locator = By.xpath("//div[@class='visible-xs text-left']//b");
            contentToCheck = "Credit Card";
        }
        assertEquals(contentToCheck, getDriver().findElement(locator).getText().trim());
    }

    @When("Click Apply Now button on any credit card list")
    public void applyAnyCreditCard() throws PendingException {
        WebElement firstDataList = checkAndGetDataListWithIndex("//div[@id='list-data']/child::*", 0);
        seleniumUtil.scrollElementWithinScrollViewIntoView(firstDataList);

        By applyBtnLocator = By.xpath(globalManager.display == DisplayType.DESKTOP ?
                ".//div[@class='col-sm-3 text-right card-box']/a[contains(.,'Apply Now')]" :
                "descendant::div[contains(@class, 'col-sm-9')]//a[contains(.,'Apply Now')]");

        WebElement applyBtn = firstDataList.findElement(applyBtnLocator);
        setProductRedirectUrl(applyBtn.getAttribute("data-redirect-url"));

        log.info("redirect URL: " + getProductRedirectUrl());

        clickElement(applyBtn);
    }

    @When("Click Apply Now button on any credit card list on the bottom of page")
    public void clickApplyNowButtonOnAnyCreditCardListOnTheBottomOfPage() throws PendingException {
        WebElement firstDataList = checkAndGetDataListWithIndex(
                "//div[@class='credit-card-provider right-content credit-card-list-by-provider']/child::*", 0);

        By applyBtnLocator = By.xpath(globalManager.display == DisplayType.DESKTOP ?
                ".//div[@class='col-sm-3 text-right card-box']/a[contains(.,'Apply Now')]" :
                "descendant::div[contains(@class, 'col-sm-9')]//a[contains(.,'Apply Now')]");

        WebElement applyBtn = firstDataList.findElement(applyBtnLocator);
        setProductRedirectUrl(applyBtn.getAttribute("data-redirect-url"));
        setPartnerDataId(applyBtn.getAttribute("data-id"));

        log.info("redirect URL: " + getProductRedirectUrl());
        log.info("partner data-id: " + getPartnerDataId());

        clickElementUsingScript(applyBtn);
    }

    @When("Click on any credit card image from the list at the bottom of page")
    public void clickOnAnyCreditCardImage() throws PendingException {
        WebElement firstDataList = checkAndGetDataListWithIndex(
                "//div[@class='credit-card-provider right-content credit-card-list-by-provider']/child::*", 0);

        String ccImageXpath;
        String hyperLinkXpath;
        if (globalManager.display == DisplayType.DESKTOP) {
            ccImageXpath = ".//div[@class='panel-body']//div[@class = 'col-sm-3 text-right card-box']//img";
            hyperLinkXpath = ccImageXpath + "/parent::a";
        } else {
            ccImageXpath = ".//div[@class='panel-heading']//img";
            hyperLinkXpath = ccImageXpath + "/ancestor::a[@title = 'product url']";
        }

        WebElement hyperLink = firstDataList.findElement(By.xpath(hyperLinkXpath));
        setProductPageUrl(hyperLink.getAttribute("href"));

        log.info("product page URL: " + getProductPageUrl());

        clickElementUsingScript(firstDataList.findElement(By.xpath(ccImageXpath)));
    }

    @When("Click the product name on any credit card from the list at the bottom of page")
    public void clickTheProductNameOnAnyCreditCardFromTheListAtTheBottomOfPage() throws PendingException {
        WebElement firstDataList = checkAndGetDataListWithIndex(
                "//div[@class='credit-card-provider right-content credit-card-list-by-provider']/child::*", 0);

        String ccLabelXpath = globalManager.display == DisplayType.DESKTOP ?
                ".//div[@class='panel-heading']//div[@class = 'hidden-xs hidden-sm']/b" :
                ".//div[@class='visible-xs visible-sm']//b";

        WebElement hyperLink = firstDataList.findElement(By.xpath(ccLabelXpath + "/ancestor::a[@title = 'product url']"));
        setProductPageUrl(hyperLink.getAttribute("href"));

        log.info("product page URL: " + getProductPageUrl());

        clickElementUsingScript(firstDataList.findElement(By.xpath(ccLabelXpath)));
    }

    @When("Click on Compare Now button on CC partner page")
    public void clickOnCompareNowButtonOnCCLandingPage() throws PendingException {
        clickElementByLocator(By.id("compare-button"));
    }

    private WebElement checkAndGetDataListWithIndex(String cardListXpath, int index) {
        waitUntilElementVisible(By.xpath(cardListXpath));
        List<WebElement> creditCardDataList = getDriver().findElements(By.xpath(cardListXpath));
        //verify there is any cc data to select apply button on
        assertNotEquals(0, creditCardDataList.size());
        assertTrue(creditCardDataList.size() > index);
        return creditCardDataList.get(index);
    }

    @When("Verify CC Funnel Page of applied partner")
    public void verifyCCFunnelPageOfAppliedPartner() throws PendingException {
        assertNotNull(getPartnerDataId());
        verifyPageUrl(URLSlugs.CC_FUNNEL_PARTNER + getPartnerDataId(), SeleniumUtil.VerifyMethod.HARD);
    }

    @Then("Verify CC application success page")
    public void verifySuccessPage() throws PendingException {
        verifyPageUrl(URLSlugs.CC_SUCCESS, SeleniumUtil.VerifyMethod.SOFT);
    }

    @When("Verify detail credit card product page")
    public void verifyDetailCreditCardProductPage() throws PendingException {
        seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(2));

        List<String> tabHandles = new ArrayList<>(getDriver().getWindowHandles());
        log.info("tabHandle size: " + tabHandles.size());
        getDriver().switchTo().window(tabHandles.get(1));

        log.info("productPageUrl: " + getProductPageUrl());
        assertNotNull(getProductPageUrl());

        waitUntilFinishRedirectToURL(getProductPageUrl());
        assertEquals(getDriver().getCurrentUrl(), getProductPageUrl());
    }

    @When("Click on Apply Now on CC detail page")
    public void clickOnApplyNowOnCCDetailPage() throws PendingException {
        log.info("ppage_version cookie value: "+getCookieValue("ppage_version"));
        By applyBtnLocator = By.xpath("//div[@class='col-xl-6 col-xl-offset-3 col-sm-10 col-sm-offset-1 col-lg-10 col-lg-offset-1 col-xs-12']/descendant::*[@class = 'btn btn-success btn-main btn-lg btn-ppage-apply btn-apply-cc']");
        waitUntilElementVisible(applyBtnLocator);

        WebElement applyBtn = getDriver().findElement(applyBtnLocator);
        String dataRedirectUrl = applyBtn.getAttribute("data-redirect-url");
        if (dataRedirectUrl != null) {
            setProductRedirectUrl(dataRedirectUrl);
            log.info("redirect URL: " + getProductRedirectUrl());
        }
        clickElement(applyBtn);
    }

    @When("Click {string} product filter")
    public void clickProductFilter(String productNameToFilter) throws PendingException {
        String productFilterViewXpath = "//div[@class='form-group cc_change credit_card_filter_provider']";
        seleniumUtil.waitFor(ExpectedConditions.presenceOfNestedElementLocatedBy(By.xpath(productFilterViewXpath), By.tagName("div")));

        String productFilterXpath = productFilterViewXpath + "//div[contains(., '" + productNameToFilter + "')]";
        clickElementByLocatorUsingScript(By.xpath(productFilterXpath+"//span"));

        seleniumUtil.waitFor(ExpectedConditions.elementSelectionStateToBe(By.xpath(productFilterXpath + "//input"), true));
    }

    @When("Click {string} Product Name from credit card list")
    public void clickProductNameFromCreditCardList(String productName) throws PendingException {
        checkAndGetDataListWithIndex(
                "//div[@id='list-data']/child::div", 0);
        String productNameXpath = "//div[@id='list-data']//div[@class='panel-heading']//div[@class = 'hidden-xs hidden-sm']/b[.='"+productName+"']";

        setProductPageUrl(getDriver().findElement(By.xpath(productNameXpath+"/ancestor::a")).getAttribute("href"));

        clickElementByLocator(By.xpath(productNameXpath));
    }

    @When("Click {string} credit card image from credit card list")
    public void clickCreditCardImageFromCreditCardList(String productName) throws PendingException {
        checkAndGetDataListWithIndex(
                "//div[@id='list-data']/child::div", 0);
        String productImageXpath = "//div[@id='list-data']//div[@data-productname = '"+productName+"']//div[@class='col-sm-3 text-right card-box']//img";

        setProductPageUrl(getDriver().findElement(By.xpath(productImageXpath+"/ancestor::a")).getAttribute("href"));

        clickElementByLocator(By.xpath(productImageXpath));
    }

}
