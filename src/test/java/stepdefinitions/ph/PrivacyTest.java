package stepdefinitions.ph;

import globalmanagers.GlobalManager;
import globalmanagers.URLSlugs;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;
import pageobjects.ph.PrivacyPage;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PrivacyTest extends AbstractSteps {
    private PrivacyPage privacyPage;

    @Autowired
    public PrivacyTest(GlobalManager globalManager, SeleniumUtil seleniumUtil) {
        this.globalManager = globalManager;
        this.seleniumUtil = seleniumUtil;
        privacyPage = loadPageComponent(PrivacyPage.class);
    }

    @When("Click How it Works")
    public void clickHowItWorks() throws PendingException {
        navigateIntoPrivacyMenu(privacyPage.howItWorksMenu, "/car-insurance");
    }

    @When("Click Frequently Asked Questions")
    public void clickFrequentlyAskedQuestions() throws PendingException {
        navigateIntoPrivacyMenu(privacyPage.faqMenu, "/car-insurance");
    }

    @When("Click Related Articles")
    public void clickRelatedArticles() throws PendingException {
        navigateIntoPrivacyMenu(privacyPage.relatedArticleMenu, "/articles/car-insurance");
    }

    @When("Click MAPFRE Insular")
    public void clickMAPFREInsular() throws PendingException {
        navigateIntoPrivacyMenu(privacyPage.mafreInsularMenu, "/car-insurance/partners/mapfre-insular");
    }

    @When("Click Mercantile Car Insurance")
    public void clickMercantileCarInsurance() throws PendingException {
        navigateIntoPrivacyMenu(privacyPage.mercantileCIMenu, "/car-insurance/partners/mercantile-insurance");
    }

    private void navigateIntoPrivacyMenu(WebElement privacyMenu, String targetUrlSegment) {
        waitUntilElementVisible(By.className("list-group"));
        waitUntilChildElementVisible(getDriver().findElement(By.className("list-group")), By.tagName("a"));
        clickElementUsingScript(privacyMenu);
        waitUntilFinishRedirectToURLContains(targetUrlSegment);
    }

    @Then("Verify the Guide page is displayed")
    public void verifyTheGuidePageIsDisplayed() throws PendingException {
        //guide page url might have #guide or not on url bar
        assertURLWithRegex("^" + getBaseUrl() + "\\/car-insurance\\/?#guide$");
        String guideTabXpath = "//li[@id='indentifyguide']";
        waitUntilElementVisible(By.xpath(guideTabXpath));
        WebElement guideTab = getDriver().findElement(By.xpath(guideTabXpath));
        waitUntilAttributeElementContains(guideTab, "class", "active");
        assertTrue(guideTab.getAttribute("class").contains("active"));
        assertEquals("Guides", guideTab.findElement(By.tagName("a")).getText());
        assertTrue(getDriver().findElement(By.xpath("//div[@id='accordion_cc']")).isDisplayed());

    }

    @Then("Verify the FAQ page is displayed")
    public void verifyTheFAQPageIsDisplayed() throws PendingException {
        assertURLWithRegex("^" + getBaseUrl() + "\\/car-insurance\\/?#faq");
        String faqTabXpath = "//li[@id='indentifyfaq']";
        waitUntilElementVisible(By.xpath(faqTabXpath));
        WebElement guideTab = getDriver().findElement(By.xpath(faqTabXpath));
        waitUntilAttributeElementContains(guideTab, "class", "active");
        assertTrue(guideTab.getAttribute("class").contains("active"));
        assertEquals("FAQs", guideTab.findElement(By.tagName("a")).getText());
        assertTrue(getDriver().findElement(By.xpath("//div[@id='accordion_faq_cc']")).isDisplayed());
    }

    @Then("Verify the Articles page is displayed")
    public void verifyTheArticlesPageIsDisplayed() throws PendingException {
        assertURL(getBaseUrl() + URLSlugs.ARTICLE_CI);
        verifyElementContent(Map.ofEntries(
                Map.entry(By.xpath("//h1[@class='content1']"), "Car Insurance"),
                Map.entry(By.xpath("//h2[@class='main-title']"), "All Articles With Category : Car Insurance")
        ));

    }

    @Then("Verify the MAPFRE Insular page is displayed")
    public void verifyTheMAPFREInsularPageIsDisplayed() throws PendingException {
        assertURL(getBaseUrl() + URLSlugs.CI_PARTNER + "/mapfre-insular");

        String headerTitleXpath = "//h1[@class='text-primary provider-title']";
        waitUntilElementVisible(By.xpath(headerTitleXpath));

        seleniumUtil.waitFor(ExpectedConditions.textToBe(By.xpath(headerTitleXpath), "MAPFRE Insular"));

        assertEquals("MAPFRE Insular", getDriver().findElement(By.xpath(headerTitleXpath)).getText());
    }

    @Then("Verify the Mercantile Car Insurance page is displayed")
    public void verifyTheMercantileCarInsurancePageIsDisplayed() throws PendingException {
        assertURL(getBaseUrl() + URLSlugs.CI_PARTNER + "/mercantile-insurance");

        String headerTitleXpath = "//h1[@class='text-primary provider-title']";
        waitUntilElementVisible(By.xpath(headerTitleXpath));

        seleniumUtil.waitFor(ExpectedConditions.textToBe(By.xpath(headerTitleXpath), "The Mercantile Insurance Corporation"));

        assertEquals("The Mercantile Insurance Corporation", getDriver().findElement(By.xpath(headerTitleXpath)).getText());
    }
}
