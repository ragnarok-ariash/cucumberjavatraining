package stepdefinitions.my;

import globalmanagers.URLSlugs;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import stepdefinitions.base.AbstractSteps;

@Slf4j
public class MYCommonSteps extends AbstractSteps {

    @When("Select {string} for actived Nationality")
    public void selectForActivedNationality(String nationality) throws PendingException {
        clickElementByLocator(By.xpath("//ul[@class='work-period-wrap flex no-pad']/li[contains(.,'"+nationality+"')]"));
    }

    @When("Select {string} for State of Residence")
    public void selectForStateOfResidence(String stateOfResidence) throws PendingException {
        selectDropdownWithValueByLocator(stateOfResidence, By.id("state_of_staying"));
    }

    @When("Select I agree to the Terms & Conditions and Privacy Policy")
    public void selectIAgreeToTheTermsConditionsAndPrivacyPolicy() throws PendingException {
        checkCheckBoxElement(By.cssSelector(".tnc-checkbox"), true);
    }

    @When("Select {string} for Date of Birth")
    public void selectForDateOfBirth(String dateOfBrith) throws PendingException {
        By inputLocator = By.id("stractical-date-validate");
        clickElementByLocator(inputLocator);
        if (elementExist(By.id("ui-datepicker-div"))) handlePopupDate(dateOfBrith);
        else enterInputWithTextByLocator(dateOfBrith, inputLocator);
    }

    @Given("Open the Personal Loan page")
    public void openThePersonalLoanResultPage() throws PendingException {
        navigateInto(getBaseUrl() + URLSlugs.PL);
        waitUntilElementVisible(By.xpath("//div[@class='header-page']/h1"));
    }

    private void handlePopupDate(String date) throws PendingException {
        String[] dateComponents = date.split("/");//day[0]. month[1]. year[2]
        log.info(dateComponents[2]);
        Select selectYear = new Select(getDriver().findElement(By.cssSelector(".ui-datepicker-year")));
        selectYear.selectByValue(dateComponents[2]);

        int month = (Integer.parseInt(dateComponents[1])) - 1;
        Select selectMonth = new Select(getDriver().findElement(By.cssSelector(".ui-datepicker-month")));
        selectMonth.selectByValue(String.valueOf(month));
        clickElementByLocator(By.xpath("//a[.='"+ dateComponents[0] +"']"));
    }
}
