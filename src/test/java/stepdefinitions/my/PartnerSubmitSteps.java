package stepdefinitions.my;

import enums.DisplayType;
import globalmanagers.URLSlugs;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.And;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import stepdefinitions.base.AbstractSteps;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotEquals;

@Slf4j
public class PartnerSubmitSteps extends AbstractSteps {


    @And("Verify Partner Page")
    public void verifyPartnerPage() throws PendingException {
        assertURL(getBaseUrl() + URLSlugs.PARTNER);
    }

    @And("Click Tab {string} Partner in Partner Page")
    public void clickTabPartnerInPartnerPage(String tabPartners) throws PendingException {
        clickElementByLocator(By.id("pills-" + tabPartners + "-tab"));
    }

    @And("click {string} {string} Logo button")
    public void clickLogoButton(String CTA, String logoBank) throws PendingException {
        By logoBankXpath = By.xpath("//div[@id='" + CTA + "-provider']//a[contains(.,'" + logoBank + "')]");
        waitUntilElementVisible(logoBankXpath);
        getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        scrollElementToBelowNavbarHeader(getDriver().findElement(logoBankXpath));
        clickElementByLocator(logoBankXpath);
    }


    @And("Verify {string} {string} Partner Page")
    public void verifyPartnerPage(String CTA, String bankName) throws PendingException {
        assertURL(getBaseUrl() + "/" + CTA + URLSlugs.PARTNER + "/" + bankName);
    }

    @And("Click Apply Now button on Partner {string} with name {string}")
    public void clickApplyNowButtonOnPartnerWithName(String CTA, String productName) throws PendingException, InterruptedException {
        String classProvider = (CTA.equals("CC")) ? "credit-card-provider right-content credit-card-list-by-provider" : "list-data-loan";
        By locator = (CTA.equals("CC")) ? (globalManager.display.equals(DisplayType.DESKTOP)) ? By.cssSelector(".hidden-xs[data-productname='" + productName + "']") : By.xpath("//div[@class='" + classProvider + "']/descendant::div[@class='col-xs-6 visible-xs']/a[contains(@data-productname, '" + productName + "') and contains(.,'Apply Now')]") : (globalManager.display.equals(DisplayType.DESKTOP)) ? By.xpath("//div[@class='hidden-xs hidden-sm text-center']/a[contains(.,'Apply Now')]") : By.xpath("//div[@class='col-xs-6 col-sm-12']/a[contains(.,'Apply Now')]");

        String cardListXpath = "//div[@class='" + classProvider + "']/child::*";
        waitUntilElementVisible(By.xpath(cardListXpath));

        seleniumUtil.waitFor(ExpectedConditions.presenceOfNestedElementLocatedBy(By.xpath("//div[@class='" + classProvider + "']"), By.xpath(cardListXpath)));
        List<WebElement> personalLoanDataList = getDriver().findElements(By.xpath(cardListXpath));
        //verify there is any cc data to select apply button on
        assertNotEquals(0, personalLoanDataList.size());

        if (globalManager.display.equals(DisplayType.MOBILE))
            scrollElementToBelowNavbarHeader(getDriver().findElement(locator));
        clickElementByLocator(locator);
    }

    @And("Click Here To Apply on Partner TI")
    public void clickHereToApplyOnPartnerTI() throws PendingException {
        scrollElementToMiddle(getDriver().findElement(By.cssSelector(".btn")));
        seleniumUtil.waitUntilElementAppearOnViewPort(getDriver().findElement(By.cssSelector(".btn")));
        clickElementByLocator(By.cssSelector(".btn"));
    }

    @And("Click {string} Partner Product name {string}")
    public void clickPartnerProductName(String textOrLogo, String productName) throws PendingException, InterruptedException {
        String classProvider = "credit-card-provider right-content credit-card-list-by-provider";
        By locator = (globalManager.display.equals(DisplayType.DESKTOP)) ? By.xpath("//div[@data-productname='" + productName + "']//div[@class='col-sm-3 text-right card-box']/a[1]") : By.xpath("//div[@class='" + classProvider + "']//div[@class='visible-xs visible-sm']//a[contains(.,'" + productName + "')]");
        if (globalManager.display.equals(DisplayType.MOBILE)) {
            scrollElementToBelowNavbarHeader(getDriver().findElement(locator));
            seleniumUtil.waitUntilElementAppearOnViewPort(getDriver().findElement(locator));
        }
        String cardListXpath = "//div[@class='" + classProvider + "']/child::*";
        waitUntilElementVisible(By.xpath(cardListXpath));

        seleniumUtil.waitFor(ExpectedConditions.presenceOfNestedElementLocatedBy(By.xpath("//div[@class='" + classProvider + "']"), By.xpath(cardListXpath)));
        List<WebElement> personalLoanDataList = getDriver().findElements(By.xpath(cardListXpath));
        //verify there is any cc data to select apply button on
        assertNotEquals(0, personalLoanDataList.size());

        clickElementByLocator(locator);
    }
}
