package stepdefinitions.my;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import globalmanagers.URLSlugs;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;
import pageobjects.MYHomePage;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

@Slf4j
public class PLSubmitSteps extends AbstractSteps {
    private final MYHomePage myHomePage;

    @Autowired
    public PLSubmitSteps(GlobalManager globalManager, SeleniumUtil seleniumUtil) {
        this.globalManager = globalManager;
        this.seleniumUtil = seleniumUtil;
        myHomePage = loadPageComponent(MYHomePage.class);
    }

    @When("Verify PL Funnel Page")
    public void verifyPLFunnelPage() throws PendingException {
        assertURL(getBaseUrl() + URLSlugs.PL);
    }

    @When("Enter with Loan Amount {string}")
    public void enterWithLoanAmount(String loanAmount) throws PendingException {
        By inputLocator = By.id("loan-amount");
        waitUntilElementClickable(inputLocator);

        String cleanValue = getDriver().findElement(By.id("loan-amount")).getAttribute("value").replace(",", "");
        for (int i = 0; i < cleanValue.length(); i++) {
            enterInputWithTextByLocator("" + Keys.BACK_SPACE, inputLocator);
        }
        enterInputWithTextByLocator(loanAmount, inputLocator);
    }

    @When("Click Button Next on PL Application Form")
    public void clickButtonNext() throws PendingException {
        clickElementByLocator(By.cssSelector(".btn-personal-info-next"));
    }

    @When("Select {string} for Loan Tenure")
    public void selectForLoanTenure(String loanTenure) throws PendingException {
        selectDropdownWithValueByLocator(loanTenure, By.id("ddl-loan-tenure"));
    }

    @When("Click Get Result Button")
    public void clickGetResultButton() throws PendingException {
        By locator = By.id("button-submit-loan");
        if (globalManager.display.equals(DisplayType.MOBILE)) scrollElementToMiddle(getDriver().findElement(locator));
        clickElementByLocator(locator);
    }

    @When("Verify PL Result Page")
    public void verifyPLResultPage() throws PendingException {
        verifyPageUrl(URLSlugs.PL_RESULT, SeleniumUtil.VerifyMethod.SOFT);
    }

    @When("Click Filter {string} checkbox")
    public void clickFilterCheckbox(String checkboxName) throws PendingException {
        if (globalManager.display.equals(DisplayType.DESKTOP)) {
            checkCheckBoxElement(By.xpath("//div[@class='col-sm-4 col-md-3 hidden-xs left-content']//label[contains(.,'" + checkboxName + "')]"), true);
        } else {
            clickElementByLocator(By.xpath("//i[@class='fa fa-filter']"));
            waitUntilElementVisible(By.xpath("//div[@id='filter']//div[@class='modal-body']"));

            String checkboxXpath = "//div[@id='filter']//label[contains(.,'" + checkboxName + "')]";
            scrollIntoElement(getDriver().findElement(By.xpath(checkboxXpath)));
            checkCheckBoxElement(By.xpath(checkboxXpath), true);

            clickElementByLocator(By.xpath("//div[@id='filter']//button[@class='close']"));
            waitUntilElementGone(By.xpath("//div[@id='filter']//div[@class='modal-body']"));
        }

    }

    @When("Click Apply Now button on loan with name {string}")
    public void clickApplyNowButtonOnLoanList(String loanName) throws PendingException {
        String cardListXpath = "//div[@id='list-data']/child::*";
        waitUntilElementVisible(By.xpath(cardListXpath));

        seleniumUtil.waitFor(ExpectedConditions.presenceOfNestedElementLocatedBy(By.id("list-data"), By.xpath(cardListXpath)));
        List<WebElement> personalLoanDataList = getDriver().findElements(By.xpath(cardListXpath));
        //verify there is any cc data to select apply button on
        assertNotEquals(0, personalLoanDataList.size());
        clickElementByLocator(By.xpath((globalManager.display.equals(DisplayType.DESKTOP)) ? "//div[@id='list-data']/descendant::div[@class='hidden-xs hidden-sm text-center']/a[contains(@data-productname, '" + loanName + "') and contains(.,'Apply Now')]" : "//div[@id='list-data']/descendant::div[@class='col-xs-6 col-sm-12 visible-xs']/a[contains(@data-productname, '" + loanName + "') and contains(.,'Apply Now')]"));
    }

    @When("Verify PL Application Form Step {int}")
    public void verifyPLApplicationFormStep(int step) throws PendingException {
        switch (step) {
            case 1:
                verifyPersonalDetailsPage();
                break;
            case 2:
                verifyIncomeInformation();
                myHomePage.closeSurveyOnly();
                break;
        }
    }

    @When("Enter PL Personal Information with Full Name {string} and Phone Number {string} and Email {string}")
    public void enterPLPersonalInformationWithFullNameAndPhoneNumberAndEmail(String fullName, String phoneNumber, String emailAddress) throws PendingException {
        enterInputWithTextByLocator(fullName, By.id("full-name-loan"));
        enterInputWithTextByLocator(phoneNumber, By.id("phone-loan"));
        enterInputWithTextByLocator(emailAddress, By.id("email-loan"));
    }

    @When("Select {string} for Monthly Base Salary")
    public void selectForMonthlyBaseSalary(String salaryOption) throws PendingException {
        selectDropdownWithValueByLocator(salaryOption, By.xpath("//select[@name='monthly_base_salary']"));
    }

    @When("Select {string} for long have you been working")
    public void selectForLongHaveYouBeenWorking(String workingOption) {
        // <<append>> /preceding-sibling::input to get input
        clickElementByLocator(By.xpath("//label[contains(.,'How long have you been working?')]/following-sibling::*/descendant::label[.='" + workingOption + "']"));
    }

    @When("Select {string} for Employment Type as PL Info")
    public void selectForEmploymentType(String employmentType) throws PendingException {
        selectDropdownWithValueByLocator(employmentType, By.xpath("//select[@name='employment_type']"));
    }

    @When("Select {string} for do you have an existing Credit Card or Loan")
    public void selectForDoYouHaveAnExistingCreditCardOrLoan(String answer) throws PendingException {
        clickElementByLocator(By.xpath("//label[contains(.,'Do you have an existing Credit Card or Loan?')]/following-sibling::*/descendant::label[.='" + answer + "']"));
    }

    @When("Select {string} for Any overdue payment in the past 12 months")
    public void selectForAnyOverduePaymentInThePastMonths(String answer) throws PendingException {
        clickElementByLocator(By.xpath("//label[contains(.,'Any overdue payment in the past 12 months?')]/following-sibling::*/descendant::label[.='" + answer + "']"));
    }

    @When("Click on PL Submit button")
    public void clickOnPLSubmitButton() {
        clickElementByLocator(By.cssSelector(".submit-text"));
    }

    @Then("Verify Thank you Page PL")
    public void verifyThankYouPage() {
        verifyPageUrl(URLSlugs.PL_SUCCESS, SeleniumUtil.VerifyMethod.SOFT);
    }

    private void verifyPersonalDetailsPage() throws PendingException {
        verifyPageUrl(URLSlugs.PL_INFO, SeleniumUtil.VerifyMethod.SOFT);
        waitUntilElementVisible(By.id("full-name-loan"));
    }

    private void verifyIncomeInformation() throws PendingException {
        waitUntilElementVisible(By.cssSelector(".step-detail-container"));
        assertTrue(getDriver().findElement(By.xpath("//div[@class='step-detail-container slight-right']")).isDisplayed());
    }

    @Then("Verify Thank you Page Redirect")
    public void verifyThankYouPagePLRedirect() throws PendingException {
        seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(2));

        List<String> tabHandles = new ArrayList<>(getDriver().getWindowHandles());
        log.info("tabHandle size: " + tabHandles.size());
        getDriver().switchTo().window(tabHandles.get(1));

        verifyPageUrl(URLSlugs.PARTNER_PRODUCT_REDIRECT, SeleniumUtil.VerifyMethod.IGNORETRAILING);
    }
}
