package stepdefinitions.my;

import enums.DisplayType;
import globalmanagers.GlobalManager;
import globalmanagers.URLSlugs;
import pageobjects.MYHomePage;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

@Slf4j
public class CCSubmitSteps extends AbstractSteps {
    private final MYHomePage myHomePage;

    @Autowired
    public CCSubmitSteps(GlobalManager globalManager, SeleniumUtil seleniumUtil) {
        this.globalManager = globalManager;
        this.seleniumUtil = seleniumUtil;
        myHomePage = loadPageComponent(MYHomePage.class);
    }

    @When("Verify CC Result Page")
    public void verifyCCResultPage() throws PendingException {
        assertURL(getBaseUrl() + URLSlugs.CC);
    }

    @When("Select Category {string}")
    public void clickCategory(String category) throws PendingException {
        clickElementByLocator(By.xpath((globalManager.display.equals(DisplayType.MOBILE)) ? "//div[@class='archive-content']//a[contains(.,'" + category + "')]" : "//div[@class='archive-box' and contains(.,'" + category + "')]"));

    }

    @When("Select Filter By Checkbox {string}")
    public void clickOnFilterByCheckbox(String checkboxLabel) throws PendingException, InterruptedException {
        waitUntilElementVisible(By.xpath("//div[@id='list-data']/child::*"));
        myHomePage.closeSurveyOnly();
        if (elementExist(By.className("popup-overlay"))) {
            while (myHomePage.isScammerWarningPopupDisplayed()) {
                myHomePage.closeScammerPopup();
                getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            }
        }
        if (globalManager.display.equals(DisplayType.DESKTOP)) {
            checkCheckBoxElement(By.xpath("//div[@id='collapse1']/form//div[@class = 'radio check' and contains(.,'" + checkboxLabel + "')]"), true);
        } else {
            clickElementByLocator(By.xpath("//i[@class='fa fa-filter']"));

            waitUntilElementVisible(By.xpath("//div[@id='filter']//div[@class='modal-body']"));

            String checkboxXpath = "//div[@id='filter']//label[contains(.,'" + checkboxLabel + "')]";
            scrollIntoElement(getDriver().findElement(By.xpath(checkboxXpath)));
            checkCheckBoxElement(By.xpath(checkboxXpath), true);

            scrollIntoElement(getDriver().findElement(By.xpath("//button[@class='btn btn-warning btn-main btn-lg']")));

            clickElementByLocator(By.xpath("//button[@class='btn btn-warning btn-main btn-lg']"));
            waitUntilElementGone(By.xpath("//div[@id='filter']//div[@class='modal-body']"));
        }

    }

    @When("Click Apply Now button on credit card with name {string}")
    public void clickApplyNowButtonOnSpecificCC(String ccName) throws PendingException {
        String cardListXpath = "//div[@id='list-data']/child::*";
        waitUntilElementVisible(By.xpath(cardListXpath));
        seleniumUtil.waitFor(ExpectedConditions.presenceOfNestedElementLocatedBy(By.id("list-data"), By.xpath(cardListXpath)));
        List<WebElement> creditCardDataList = getDriver().findElements(By.xpath(cardListXpath));
        //verify there is any cc data to select apply button on
        assertNotEquals(0, creditCardDataList.size());

        clickElementByLocator(By.xpath((globalManager.display.equals(DisplayType.DESKTOP)) ? "//div[@class='col-sm-3 text-right card-box']/a[contains(@data-productname, '" + ccName + "') and contains(.,'Apply Now')]" : "//div[@class='col-xs-6 visible-xs']/a[contains(@data-productname, '" + ccName + "') and contains(.,'Apply Now')]"));

    }

    @When("Verify CC Application Form Step {int}")
    public void verifyCCApplicationFormStep(int step) throws PendingException {
        switch (step) {
            case 1:
                verifyPersonalDetailsPage();
                break;
            case 2:
                verifyIncomeInformation();
                myHomePage.closeSurveyOnly();
                break;
        }
    }

    @When("Enter CC Personal Information with Full Name {string} and Phone Number {string} and Email {string}")
    public void enterPersonalInformationWithFullNameAndPhoneNumberAndEmail(String fullName, String phoneNumber, String emailAddress) throws PendingException {
        enterInputWithTextByLocator(fullName, By.id("full-name-cc"));
        enterInputWithTextByLocator(phoneNumber, By.id("phone-cc"));
        enterInputWithTextByLocator(emailAddress, By.id("email-cc"));
    }

    @When("Enter with IC Number {string}")
    public void enterWithICNumber(String icNumber) throws PendingException {
        enterInputWithTextByLocator(icNumber, By.id("passport-cc-input"));
    }

    @When("Select {string} for Employment Type as CC Info")
    public void selectForEmploymentType(String employmentType) throws PendingException {
        selectDropdownWithValueByLocator(employmentType, By.id("employment_status_info"));
    }

    @When("Click Button Next on CC Application Form")
    public void clickButtonNext() throws PendingException {
        clickElementByLocator(By.cssSelector(".btn-next-cc"));
    }

    @When("Select {string} for Credit History \\(> 12 months)")
    public void selectForCreditHistoryMonths(String creditHistoryAnswer) throws PendingException {
        clickElementByLocator(By.xpath("//li[contains(.,'" + creditHistoryAnswer + "')]/label"));
    }

    @When("Enter with Range Income {string}")
    public void enterWithRangeIncome(String incomeAmount) throws PendingException {
        enterInputWithTextByLocator(incomeAmount, By.id("range-income"));
    }

    @When("Click on CC Submit button")
    public void clickOnCCSubmitButton() throws PendingException {
        clickElementByLocator(By.cssSelector(".submit-text"));
    }

    @Then("Verify success page")
    public void verifySuccessPage() throws PendingException {
        verifyPageUrl(URLSlugs.CC_SUCCESS, SeleniumUtil.VerifyMethod.SOFT);
    }

    private void verifyPersonalDetailsPage() throws PendingException {
        verifyPageUrl(URLSlugs.CC_FUNNEL, SeleniumUtil.VerifyMethod.SOFT);
        waitUntilElementVisible(By.id("full-name-cc"));
    }

    private void verifyIncomeInformation() throws PendingException {
        waitUntilElementVisible(By.cssSelector(".step-detail-container"));
        assertTrue(getDriver().findElement(By.xpath("//div[@class='step-detail-container slight-right']")).isDisplayed());
    }

    @When("Click {string} Product name {string}")
    public void clickTextProductName(String textOrLogo, String productName) throws PendingException, InterruptedException {
        String cardListXpath = "//div[@id='list-data']/child::*";
        waitUntilElementVisible(By.xpath(cardListXpath));

        seleniumUtil.waitFor(ExpectedConditions.presenceOfNestedElementLocatedBy(By.id("list-data"), By.xpath(cardListXpath)));
        List<WebElement> creditCardDataList = getDriver().findElements(By.xpath(cardListXpath));
        //verify there is any cc data to select apply button on
        assertNotEquals(0, creditCardDataList.size());

        String locator = (globalManager.display.equals(DisplayType.DESKTOP)) ?
                (textOrLogo.equals("text")) ?
                        "//div[@class='panel-heading theBox ']/div[@class='hidden-xs hidden-sm']/a[contains(.,'" + productName + "')]" :
                        "//div[@data-productname='" + productName + "']//div[@class='col-sm-3 text-right card-box']/a[1]" :
                (textOrLogo.equals("text")) ?
                        "//div[contains(@class,'panel-heading theBox')]/div[@class='visible-xs visible-sm']//b[.='" + productName + "']" :
                        "//div[contains(@class,'panel-heading theBox')]/div[@class='visible-xs visible-sm']//b[.='" + productName + "']";
        if (globalManager.display.equals(DisplayType.MOBILE)) {
            WebElement clickTarget = getDriver().findElement(By.xpath(locator));
            scrollElementToMiddle(clickTarget);
            seleniumUtil.waitUntilElementAppearOnViewPort(clickTarget);
        }
        clickElementByLocator(By.xpath(locator));
    }

    @When("Click Apply Now button on credit card Product Page with name {string}")
    public void clickApplyNowButtonOnCreditCardProductPageWithName(String linkProduct) throws PendingException {
        assertURL(getBaseUrl() + URLSlugs.CC_PRODUCT + "/" + linkProduct);
        clickElementByLocator(By.xpath((globalManager.display.equals(DisplayType.DESKTOP)) ? "//div[@id='btn-apply-desktop']/a[contains(.,'Apply now')]" : "//div[@id='btn-apply-mobile']/a[contains(.,'Apply now')]"));

    }
}
