package stepdefinitions.my;

import enums.DisplayType;
import globalmanagers.URLSlugs;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

@Slf4j
public class TISubmitSteps extends AbstractSteps {
    private String mainTabHandle;
    private long startDate;
    private long endDate;

    @When("Verify TI Funnel Page")
    public void verifyTIFunnelPage() {
        assertURL(getBaseUrl() + URLSlugs.TI);
    }

    @When("Select {string} for Travelling from Malaysia to")
    public void selectForTravellingFromMalaysiaTo(String destination) throws PendingException {

        String dropdownIconLocator = (globalManager.display.equals(DisplayType.DESKTOP)) ? "//div[@class='hidden-xs-down']//div[@class=' css-1wy0on6']" : "//div[@class='hidden-xs-up']//div[@class=' css-1wy0on6']";
        if (globalManager.display == DisplayType.MOBILE) {
            waitUntilElementVisible(By.xpath(dropdownIconLocator));
            scrollElementToBelowNavbarHeader(getDriver().findElement(By.xpath(dropdownIconLocator)));
            getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        }
        //click the dropdown button to display city list as popup
        clickElementByLocator(By.xpath(dropdownIconLocator));

        //wait for the city list popup
        waitUntilElementVisible(By.xpath("//*[@class = ' css-26l3qy-menu']"));

        //select value based on @param destination
        clickElementByLocator(By.xpath("//*[@class = ' css-26l3qy-menu']/descendant::div[. = '" + destination + "']"));
    }

    @When("Select {string} for Start Date")
    public void selectForStartDate(String startDate) throws PendingException {
        this.startDate = LocalDate.parse(startDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                .atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().getEpochSecond();

        waitUntilElementGone(By.xpath("//*[@class = ' css-26l3qy-menu']"));

        clickElementByLocator(By.xpath((globalManager.display.equals(DisplayType.DESKTOP)) ? "//div[@class='hidden-xs-down']//input[@id='startDate']" : "//div[@class='hidden-xs-up']//input[@id='startDate']"));
        handlePopupDate(startDate, new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
    }

    @When("Select {string} for End Date")
    public void selectForEndDate(String endDate) throws PendingException {
        this.endDate = LocalDate.parse(endDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                .atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().getEpochSecond();

        assertTrue(this.endDate >= this.startDate);
        clickElementByLocator(By.xpath((globalManager.display.equals(DisplayType.DESKTOP)) ? "//div[@class='hidden-xs-down']//input[@id='endDate']" : "//div[@class='hidden-xs-up']//input[@id='endDate']"));
        handlePopupDate(endDate, Instant.ofEpochSecond(this.startDate).atZone(ZoneId.systemDefault()).toLocalDate());
    }

    private void handlePopupDate(String date, LocalDate referenceDate) throws PendingException {
        waitUntilElementVisible(By.xpath("//div[@class='DateRangePicker_picker DateRangePicker_picker_1 DateRangePicker_picker__directionLeft DateRangePicker_picker__directionLeft_2']"));

        String[] dateComponents = date.split("/");//day[0]. month[1]. year[2]
        int month = Integer.parseInt(dateComponents[1]);
        int referenceMonth = referenceDate.getMonthValue();
        assertTrue(referenceMonth <= month);
        int diff = month - referenceMonth;
        for (int i = 0; i < diff; i++) {
            clickElementByLocator(By.xpath("//div[@class='DayPickerNavigation DayPickerNavigation_1 DayPickerNavigation__horizontal DayPickerNavigation__horizontal_2']/child::div[2]"));
            getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        }
        WebElement testElement = getDriver().findElement(By.xpath("//div[@class='CalendarMonthGrid_month__horizontal CalendarMonthGrid_month__horizontal_1']/div[@class = 'CalendarMonth CalendarMonth_1']"));
        log.info("data-visible: "+testElement.getAttribute("data-visible"));
        clickElementByLocator(By.xpath("//div[@class='CalendarMonthGrid_month__horizontal CalendarMonthGrid_month__horizontal_1']/div[@class = 'CalendarMonth CalendarMonth_1' and @data-visible = 'true']/table[@class='CalendarMonth_table CalendarMonth_table_1']/descendant::td[. = '"+ dateComponents[0] +"']"));
    }

    @When("Select {string} for This policy is to insure")
    public void selectForThisPolicyIsToInsure(String insureOption) {
        selectDropdownWithValueByLocator(insureOption, By.xpath((globalManager.display.equals(DisplayType.DESKTOP)) ? "//div[@class='hidden-xs-down']//select[@class='form-control index__selection-placeholder__2d4da']" : "//div[@class='hidden-xs-up']//select[@class='form-control index__selection-placeholder__2d4da']"));
    }

    @When("Click Button Lest Go")
    public void clickButtonLestGo() throws PendingException {
        clickElementByLocator(By.xpath((globalManager.display.equals(DisplayType.DESKTOP)) ? "//div[@class='hidden-xs-down']//button[@class='index__btn-filter__1PXWO']" : "//div[@class='hidden-xs-up']//button[@class='index__btn-filter__1PXWO']"));
    }

    @When("Verify TI Result Page")
    public void verifyTIResultPage() throws PendingException {
        verifyPageUrl(URLSlugs.TI_RESULT, SeleniumUtil.VerifyMethod.SOFT);
    }

    @When("Click TI Filter {string} checkbox")
    public void clickFilterCheckbox(String filterName) throws PendingException, InterruptedException {
        waitUntilElementGone(By.className("index__loading-wrap__2fqeu"));
        if (globalManager.display.equals(DisplayType.DESKTOP)) {
            checkCheckBoxElement(By.xpath("//div[@class='hidden-xs-down index__filter-wrap__8Mdvy']/ul[@class='hidden-xs-down']/descendant::label[@class = 'flex align-center index__label-wrap__ZVp-e' and . = '" + filterName + "']"), true);
        } else {
            clickElementByLocator(By.xpath("//i[@class='fa fa-filter']"));

            String modalViewXpath = "//div[@class='index__sidebar-wrap__2iy7F index__sidebar-wrap-right__3YWmZ index__full-width__35ApC']//div[@class='index__filter-item-wrap__1_hnZ index__inner-container__2d8Id']";
            waitUntilElementVisible(By.xpath(modalViewXpath));

            String checkboxXpath = "//div[@class='index__sidebar-wrap__2iy7F index__sidebar-wrap-right__3YWmZ index__full-width__35ApC']/descendant::label[@class='flex align-center index__label-wrap__ZVp-e'  and contains(.,'"+filterName+"')]/input";
            scrollIntoElement(getDriver().findElement(By.xpath(checkboxXpath)));
            checkCheckBoxElement(By.xpath(checkboxXpath), true);

            clickElementByLocator(By.xpath("//div[@class='index__sidebar-wrap__2iy7F index__sidebar-wrap-right__3YWmZ index__full-width__35ApC']//button[@class='full-width index__sidebar-btn-close__26DgQ']"));
            waitUntilElementGone(By.xpath(modalViewXpath));
        }

    }

    @When("Click Apply Now button on TI Data {string}")
    public void clickApplyNowButtonOnTI(String tiName) throws PendingException {
        String tiListXpath = "//div[@class='index__content-wrap__2IM0c']/child::ul/child::*";
        waitUntilElementVisible(By.xpath(tiListXpath));

        List<WebElement> TIDataList = getDriver().findElements(By.xpath(tiListXpath));
        //verify there is any ti data to select apply button on
        assertNotEquals(0, TIDataList.size());

        clickElementByLocator(By.xpath((globalManager.display.equals(DisplayType.DESKTOP)) ? "//div[@class='index__content-wrap__2IM0c']//div[@class='flex align-center justify-center' and contains(.,'"+tiName+"')]/following-sibling::*/descendant::button[. = 'Apply Now']" : "//div[@class='index__content-wrap__2IM0c']//li[contains(.,'"+tiName+"')]/descendant::div[@class = 'index__padding-container__3FJA3']//button[. = 'Apply Now']"));
    }

    @When("Verify TI Application Form")
    public void verifyTIApplicationForm() throws PendingException {
        waitUntilElementVisible(By.xpath("//div[@class='index__application-form__1fUnU']"));
        assertTrue(getDriver().findElement(By.xpath("//div[@class='index__application-form__1fUnU']")).isDisplayed());
    }

    @When("Enter Personal Information with Full Name {string} and Phone Number {string} and Email {string}")
    public void enterPersonalInformationWithFullNameAndPhoneNumberAndEmail(String fullName, String phoneNum, String emailAddress) throws PendingException {
        enterInputWithTextByLocator(fullName, By.xpath("//input[@name='full_name']"));
        enterInputWithTextByLocator(phoneNum, By.xpath("//input[@name='phone']"));
        enterInputWithTextByLocator(emailAddress, By.xpath("//input[@name='email']"));
    }

    @When("Select I Agree to the Terms & Conditions and Privacy Policy")
    public void selectIAgreeToTheTermsConditionsAndPrivacyPolicy() throws PendingException {
        if (globalManager.display == DisplayType.MOBILE) {
            scrollElementToMiddle(getDriver().findElement(By.id("chkTnC")));
            getDriver().manage().timeouts().implicitlyWait((long) 1.5, TimeUnit.SECONDS);
        }
        checkCheckBoxElement(By.id("chkTnC"), true);
    }

    @When("Click Button Go To Provider Page")
    public void clickButtonGoToProviderPage() throws PendingException {
        clickElementByLocator(By.xpath("//button[. = 'Go To Provider Page']"));
    }

    @Then("Verify redirect site")
    public void verifyRedirectSite() throws PendingException {
        seleniumUtil.waitFor(ExpectedConditions.numberOfWindowsToBe(2));

        List<String> tabHandles = new ArrayList<>(getDriver().getWindowHandles());

        getDriver().switchTo().window(tabHandles.get(1));
        verifyPageUrl(URLSlugs.PARTNER_PRODUCT_REDIRECT, SeleniumUtil.VerifyMethod.SOFT);

        mainTabHandle = tabHandles.get(0);
    }

    @Then("Select Tab on application success")
    public void selectTabOnApplicationSuccess() throws PendingException {
        getDriver().switchTo().window(mainTabHandle);

    }

    @Then("Verify Thank you Page TI")
    public void verifyThankYouPageTI() throws PendingException {
        verifyPageUrl(URLSlugs.TI_SUCCESS, SeleniumUtil.VerifyMethod.SOFT);
    }

    @When("Verify on TI Data {string}")
    public void verifyOnTIData(String tiName) throws PendingException {
        String tiListXpath = "//div[@class='index__content-wrap__2IM0c']/child::ul/child::*";
        waitUntilElementVisible(By.xpath(tiListXpath));

        List<WebElement> TIDataList = getDriver().findElements(By.xpath(tiListXpath));
        //verify there is any ti data to select apply button on
        assertNotEquals(0, TIDataList.size());

        List<WebElement> veriyLocator = getDriver().findElements(By.xpath((globalManager.display.equals(DisplayType.DESKTOP)) ? "//div[@class='index__content-wrap__2IM0c']//div[@class='flex align-center justify-center' and contains(.,'"+tiName+"')]/following-sibling::*/descendant::button[. = 'Apply Now']" : "//div[@class='index__content-wrap__2IM0c']//li[contains(.,'"+tiName+"')]/descendant::div[@class = 'index__padding-container__3FJA3']//button[. = 'Apply Now']"));
        assertNotEquals(0, veriyLocator.size());
    }

    @When("Select {string} for Sort By")
    public void selectForSortBy(String sortBy) throws PendingException {
        if (globalManager.display.equals(DisplayType.DESKTOP)) {
            selectDropdownWithValueByLocator(sortBy, By.xpath("//div[@class='col-sm-6']//select[@class='form-control index__input-form-padding__2_EiT']"));
        } else {
            waitUntilElementGone(By.className("index__loading-wrap__2fqeu"));
            clickElementByLocator(By.xpath("//i[@class='fa fa-filter']"));

            String modalViewXpath = "//div[@class='index__sidebar-wrap__2iy7F index__sidebar-wrap-right__3YWmZ index__full-width__35ApC']//div[@class='index__filter-item-wrap__1_hnZ index__inner-container__2d8Id']";
            waitUntilElementVisible(By.xpath(modalViewXpath));

            selectDropdownWithValueByLocator(sortBy, By.xpath("//div[@class='index__sidebar-wrap__2iy7F index__sidebar-wrap-right__3YWmZ index__full-width__35ApC']//select[@class='form-control index__input-form-padding__2_EiT']"));

            clickElementByLocator(By.xpath("//div[@class='index__sidebar-wrap__2iy7F index__sidebar-wrap-right__3YWmZ index__full-width__35ApC']//button[@class='full-width index__sidebar-btn-close__26DgQ']"));
            waitUntilElementGone(By.xpath(modalViewXpath));
        }
    }
}
