package stepdefinitions.my;

import enums.DisplayType;
import globalmanagers.URLSlugs;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.springframework.beans.factory.annotation.Autowired;
import pageobjects.my.*;
import stepdefinitions.base.AbstractSteps;
import utils.SeleniumUtil;

import java.math.BigInteger;
import java.util.List;

import static org.junit.Assert.*;
import static pageobjects.my.PLProductPage.SECTIONTAB.*;

@Slf4j
public class PLProductPageTest extends AbstractSteps {

    @Autowired
    private PLFormPage plFormPage;

    @Autowired
    private PLResultPage plResultPage;

    @Autowired
    private PLProductPage plProductPage;

    @Autowired
    private PLInfoFormPage plInfoFormPage;

    @Autowired
    private PartnerPage partnerPage;

    @Autowired
    private PLPartnerDetailPage plPartnerDetailPage;

    private static final String PRODUCT_PAGE_URL = "product-page-url";

    @Given("Open the Personal Loan Product page")
    public void openThePersonalLoanProductPage() throws PendingException {
        navigateInto(getBaseUrl() + URLSlugs.PL_PRODUCT);
        waitUntilElementVisible(plProductPage.getApplyNowBtn());
        plProductPage.closeSurvey();
    }

    @Given("Open the Personal Loan Product page and scroll down the page")
    public void openThePersonalLoanProductPageAndScrollDownThePage() throws PendingException {
        openThePersonalLoanProductPage();
        seleniumUtil.scrollIntoElement(plProductPage.footerView);
    }

    @Given("Open comparehero Partner page {string}")
    public void openCompareheroPartnerPage(String partnerPageUrl) throws PendingException {
        navigateInto(partnerPageUrl);
    }

    @Given("Visits the product page url {string}")
    public void visitsTheProductPageUrl(String url) throws PendingException {
        navigateInto(url);
    }

    @When("Click on Get Result button and open the Personal Loan result page")
    public void clickOnGetResultButtonAndOpenThePersonalLoanResultPage() throws PendingException {
        clickElement(plFormPage.getResultBtn);
        waitUntilFinishRedirectToURL(getBaseUrl() + URLSlugs.PL_RESULT);
    }

    @When("Click on any Product Name")
    public void clickOnAnyProductName() throws PendingException {
        WebElement productName = plResultPage.getAnyProductName();
        String urlToBe = productName.getAttribute("href");
        pageData.setCustomData(PRODUCT_PAGE_URL, urlToBe);
        clickElement(productName);
        waitUntilFinishRedirectToURL(urlToBe);
    }

    @When("Click on Apply now CTA")
    public void clickOnApplyNowCTA() throws PendingException {
        String urlToBe = plProductPage.getApplyNowBtnLink().getAttribute("href");
        pageData.setCustomData(PRODUCT_PAGE_URL, urlToBe);
        clickElement(plProductPage.getApplyNowBtn());
        waitUntilFinishRedirectToURL(urlToBe);
    }

    @When("Click on Apply now CTA from bottom sticky bar")
    public void clickOnApplyNowCTAFromBottomStickyBar() throws PendingException {
        String urlToBe = plProductPage.stickyBtmApplyNowBtn.getAttribute("href");
        pageData.setCustomData(PRODUCT_PAGE_URL, urlToBe);
        clickElement(plProductPage.stickyBtmApplyNowBtn);
        waitUntilFinishRedirectToURL(urlToBe);
    }

    @When("Scroll into Limited time promotion")
    public void scrollIntoLimitedTimePromotion() throws PendingException {
        scrollIntoElement(plProductPage.getLimitedTimePromoView());
    }

    @When("Scroll into Product Details")
    public void scrollIntoProductDetails() throws PendingException {
        scrollIntoElement(plProductPage.getProductDetailLabel());
    }

    @When("Click on Features section tab")
    public void clickOnFeaturesSectionTab() throws PendingException {
        clickOnTabMenu(FEATURES);
    }

    @When("Click on Eligibility section tab")
    public void clickOnEligibilitySectionTab() throws PendingException {
        clickOnTabMenu(ELIGIBILITY);
    }

    @When("Click on Fees & Charges section tab")
    public void clickOnFeesChargesSectionTab() throws PendingException {
        clickOnTabMenu(FEE);
    }

    @When("Click on Document required to apply section tab")
    public void clickOnDocumentRequiredToApplySectionTab() throws PendingException {
        clickOnTabMenu(DOCUMENT);
    }

    private void clickOnTabMenu(PLProductPage.SECTIONTAB sectiontab) throws PendingException {
        scrollIntoElement(plProductPage.getProductDetailLabel());
        WebElement tab = plProductPage.getProductDetailTab(sectiontab);
        clickElement(tab);
        WebElement targetElement = tab;
        if (globalManager.display == DisplayType.DESKTOP)
            targetElement = tab.findElement(By.xpath("./..")); //traverse to parent element and wait until it has active class

        log.info("target element tag: " + targetElement.getTagName() + ", target element class: " + targetElement.getAttribute("class"));
        waitUntilAttributeElementContains(targetElement,
                "class", "active");
        seleniumUtil.waitUntilElementAppearOnViewPort(plProductPage.getProductDetailTabContent(sectiontab), false);
    }

    @When("Scroll into Loan repayment table")
    public void scrollIntoLoanRepaymentTable() throws PendingException {
        scrollIntoElement(plProductPage.loanRepaymentTableLabel);
    }

    @When("Scroll into Looking for more options?")
    public void scrollIntoLookingForMoreOptions() throws PendingException {
        scrollIntoElement(plProductPage.lookingForMoreOptionQLabel);
    }

    @When("Clicks any related product box")
    public void clicksAnyRelatedProductBox() throws PendingException {
        WebElement anyProductAnchorElement = plProductPage.getAnyMoreOptionProduct().findElement(By.tagName("a"));
        String targetUrl = anyProductAnchorElement.getAttribute("href");
        if (globalManager.display == DisplayType.MOBILE) {
            String onClickEvent = anyProductAnchorElement.getAttribute("onclick");
            onClickEvent = onClickEvent.replaceFirst("relatedProductClick", "");
            String onClickEventParams[] = onClickEvent.split(",");
            assertTrue(onClickEventParams.length >= 2);
            targetUrl = onClickEventParams[1].trim().replaceAll("'", "");
        }
        pageData.setCustomData(PRODUCT_PAGE_URL, targetUrl);
        clickElement(plProductPage.getAnyMoreOptionProduct());
        waitUntilFinishRedirectToURL(targetUrl);
    }

    @When("Scroll into newsletter section")
    public void scrollIntoNewsletterSection() throws PendingException {
        scrollElementToMiddle(plProductPage.newsletterSectionView);
        seleniumUtil.waitUntilElementAppearOnViewPort(plProductPage.newsletterSectionView);
    }

    @When("Input email with the invalid email format {string}")
    public void inputEmailWithTheInvalidEmailFormat(String emailAddress) throws PendingException {
        enterNewsletterEmail(emailAddress);
        waitUntilElementVisible(plProductPage.getNewsletterSubscribeValidationPopup());
        assertEquals("top-red", plProductPage.getNewsletterSubscribeValidationPopup().getAttribute("class"));
    }

    @When("Input email with the valid email format {string}")
    public void inputEmailWithTheValidEmailFormat(String emailAddress) throws PendingException {
        enterNewsletterEmail(emailAddress);
        waitUntilElementVisible(plProductPage.getNewsletterSubscribeValidationPopup());
    }

    @When("Input email with the existing email format {string}")
    public void inputEmailWithTheExistingEmailFormat(String emailAddress) throws PendingException {
        enterNewsletterEmail(emailAddress);
        waitUntilElementVisible(plProductPage.getNewsletterSubscribeValidationPopup());
    }

    private void enterNewsletterEmail(String emailAddress) {
        if (globalManager.display == DisplayType.DESKTOP)
            scrollIntoElement(plProductPage.getNewsletterEmailInput());
        else {
            scrollElementToMiddle(plProductPage.getNewsletterEmailInput());
            seleniumUtil.waitUntilElementAppearOnViewPort(plProductPage.getNewsletterEmailInput());
        }
        enterInputWithTextByElement(emailAddress, plProductPage.getNewsletterEmailInput());
    }

    @When("Click on Subscribe button")
    public void clickOnSubscribeButton() throws PendingException {
        clickElement(plProductPage.getNewsletterSubscribeBtn());
        waitUntilElementVisible(plProductPage.getNewsletterSubscribeValidationText());
    }

    @When("Scroll into Personal loan provider")
    public void scrollIntoPersonalLoanProvider() throws PendingException {
        scrollIntoElement(partnerPage.plProviderLabel);
    }

    @When("Click on CITI product on Personal Loan Providers")
    public void clickOnCITIProductOnPersonalLoanProviders() throws PendingException {
        seleniumUtil.waitUntilChildElementVisible(partnerPage.plProviderList, By.tagName("div"));
        assertNotEquals(0, partnerPage.plProviderListItems.size());
        WebElement citibank = partnerPage.plProviderList.findElement(By.xpath(".//a[contains(.,'Citibank')]"));
        pageData.setCustomData(PRODUCT_PAGE_URL, citibank.getAttribute("href"));
        clickElement(citibank);
        waitUntilFinishRedirectToURL(pageData.getCustomData(PRODUCT_PAGE_URL));
    }

    @When("Verify redirect to the CITI Product Page detail")
    public void verifyRedirectToTheCITIProductPageDetail() throws PendingException {
        assertURL(pageData.getCustomData(PRODUCT_PAGE_URL));
    }


    @When("Click on any product Image from the list")
    public void whenClickOnAnyProductImageFromTheList() throws PendingException {
        WebElement loanItem = plPartnerDetailPage.getAnyLoanItem();
        pageData.setCustomData(PRODUCT_PAGE_URL, plPartnerDetailPage.getProductUrlFromLoanItem(loanItem));
        clickElement(plPartnerDetailPage.getImageElementFromLoanItem(loanItem));

        waitUntilFinishRedirectToURL(pageData.getCustomData(PRODUCT_PAGE_URL));
    }

    @When("Scroll down the page")
    public void scrollDownThePage() throws PendingException {
        if (globalManager.display == DisplayType.DESKTOP)
            scrollIntoElement(plProductPage.getProductImage());
        else {
            scrollElementToBelowNavbarHeader(plProductPage.getProductImage());
            waitUntilElementScrolledToNavbarGetSpecificYOffset(plProductPage.getProductImage(), 5);
        }
    }

    @Then("Verify the augmented Product page is displayed")
    public void verifyTheAugmentedProductPageIsDisplayed() throws PendingException {
        seleniumUtil.verifyPageUrl(pageData.getCustomData(PRODUCT_PAGE_URL), SeleniumUtil.VerifyMethod.HARD);
        waitUntilElementVisible(plProductPage.getApplyNowBtn());
        assertTrue(plProductPage.getApplyNowBtn().isDisplayed());
    }

    @Then("Verify the url is {string}")
    public void verifyTheUrlIs(String urlToVerify) throws PendingException {
        seleniumUtil.verifyPageUrl(urlToVerify, SeleniumUtil.VerifyMethod.HARD);
    }

    @Then("Verify the Step 1 for Loan Information Form is displayed")
    public void verifyTheStepForLoanInformationFormIsDisplayed() throws PendingException {
        seleniumUtil.verifyPageUrl(pageData.getCustomData(PRODUCT_PAGE_URL), SeleniumUtil.VerifyMethod.HARD);
        assertTrue(plInfoFormPage.formStepLabel.getText().toLowerCase().contains("step 1"));
    }

    @Then("Verify {string} is displayed")
    public void verifyLimitedTimePromotion(String limitedTimePromotion) throws PendingException {
        List<WebElement> divs = plProductPage.getLimitedTimePromoView().findElements(By.xpath("./div"));
        assertTrue(divs.get(0).getText().toLowerCase().contains(limitedTimePromotion.toLowerCase()));
    }

    @Then("Verify all sections tab displayed correctly:")
    public void verifyAllSectionsTabDisplayedCorrectly(DataTable dataTable) throws PendingException {
        dataTable.asList().forEach(tabTitle -> {
            log.info("verifying tab: " + tabTitle);
            By tabLocator = By.xpath(globalManager.display == DisplayType.DESKTOP ?
                    "//a[@class = 'nav-link' and .='" + tabTitle + "']" :
                    "//div[@class='navbar-product-page-mb visible-xs']/a[.='" + tabTitle + "']");
            waitUntilElementVisible(tabLocator);
            assertTrue(getDriver().findElement(tabLocator).isDisplayed());
        });
    }

    @Then("Verify {string} as section titles is displayed")
    public void verifyAsSectionTitlesIsDisplayed(String tabTitle) throws PendingException {
        WebElement targetElement = plProductPage.getProductDetailTab(plProductPage.getEnumForTabTitle(tabTitle));
        if (globalManager.display == DisplayType.DESKTOP)
            targetElement = targetElement.findElement(By.xpath("./.."));
        log.info("tag: " + targetElement.getTagName() + ", class: " + targetElement.getAttribute("class"));
        assertTrue(targetElement.getAttribute("class").contains("active"));
    }

    @Then("Verify all details content Features section is displayed")
    public void verifyAllDetailsContentFeaturesSectionIsDisplayed() throws PendingException {
        assertTrue(plProductPage.productDetailFeatureTabContent.findElement(By.xpath(".//div[contains(@class,'subtitle')]")).getText().trim().equals("Features"));
        assertEquals(plProductPage.productDetailFeatureTabContent.findElements(
                By.xpath(".//div[@class = 'feature-list']")).size(), 3
        );
        assertTrue(plProductPage.productDetailFeatureTabContent.findElement(
                By.xpath(".//div[@class = 'feature-list'][1]//div[@class = 'content']")
        ).getText().trim().equals("No Collateral or Guarantor Required"));
        assertTrue(plProductPage.productDetailFeatureTabContent.findElement(
                By.xpath(".//div[@class = 'feature-list'][2]//div[@class = 'content']")
        ).getText().trim().equals("You will be issued a Standard Chartered Visa Platinum Credit Card with annual fee waiver for life"));
        assertTrue(plProductPage.productDetailFeatureTabContent.findElement(
                By.xpath(".//div[@class = 'feature-list'][3]//div[@class = 'content']")
        ).getText().trim().equals("Consolidate your unsecured debts up to 4 loans and enjoy lower monthly repayment plan"));

    }

    @Then("Verify the Features table content are displayed correctly:")
    public void verifyTheFeaturesTableContentAreDisplayedCorrectly(DataTable dataTable) throws PendingException {
        assertEquals("table", plProductPage.productDetailFeatureTabContent.findElement(
                By.xpath(".//div[@class = 'benefit-list']/child::*[1]")
        ).getTagName());
        assertEquals("table product-page-pl-table", plProductPage.productDetailFeatureTabContent.findElement(
                By.xpath(".//div[@class = 'benefit-list']/child::*[1]")
        ).getAttribute("class"));

        dataTable.asList().forEach(benefit -> {
            log.info("verifying benefit: " + benefit);
            assertTrue(plProductPage.productDetailFeatureTabContent.findElement(
                    By.xpath(".//div[@class = 'benefit-list']/child::*//tr/td[. = '" + benefit + "']")
            ).isDisplayed());
        });
    }

    @Then("Verify the monthly repayment calculator box is displayed")
    public void verifyTheMonthlyRepaymentCalculatorBoxIsDisplayed() throws PendingException {
        seleniumUtil.scrollIntoElement(plProductPage.monthlyRepaymentCalcLeftView, false);
        assertTrue(plProductPage.monthlyRepaymentCalcLeftView.isDisplayed());
        assertTrue(plProductPage.monthlyRepaymentCalcRightView.isDisplayed());
    }

    @Then("Verify the Eligibility table content are displayed correctly:")
    public void verifyTheEligibilityTableContentAreDisplayedCorrectly(DataTable dataTable) throws PendingException {

        assertTrue(plProductPage.getEligibilityTable().isDisplayed());

        dataTable.asList().forEach(eligibility -> {
            log.info("verifying eligibility: " + eligibility);
            assertTrue(plProductPage.getEligibilityTable().findElement(
                    By.xpath(".//tr/td[. = '" + eligibility + "']")
            ).isDisplayed());
        });
    }

    @Then("Verify Late payment fee on the details of Fees & Charges")
    public void verifyLatePaymentFeeOnTheDetailsOfFeesCharges() throws PendingException {
        By tableRowLocator = By.xpath(".//tr/td");
        assertTrue(plProductPage.getFeeAndChargeTable().findElement(tableRowLocator)
                .isDisplayed());
        assertEquals("Late payment Fee", plProductPage.getFeeAndChargeTable()
                .findElement(tableRowLocator).getText());
    }

    @Then("Verify Document required to apply section details are displayed and contains the following:")
    public void verifyDocumentRequiredToApplySectionDetailsAreDisplayedAndContainsTheFollowing(DataTable dataTable) {
        assertNotEquals(0, plProductPage.productDetailDocumentTabContent.findElements(
                By.xpath(".//div[@class='col-md-9 col-xs-12 box-content set-padding-lr-5']/p")
        ).size());

        dataTable.asList().forEach(docCategory -> {
            log.info("verifying document category: " + docCategory);
            assertTrue(plProductPage.productDetailDocumentTabContent.findElement(
                    By.xpath(".//div[@class='col-md-9 col-xs-12 box-content set-padding-lr-5']/p[contains(., '" + docCategory + "')]")
            ).isDisplayed());
        });
    }

    @Then("Verify Monthly repayment are shown in the table from {int} to {int} months")
    public void verifyMonthlyRepaymentAreShownInTheTableFromToMonths(int lowerBound, int upperBound) throws PendingException {
        List<WebElement> tableHeads = plProductPage.getMonthlyRepaymentTableHead();
        assertNotEquals(0, tableHeads.size());
        assertEquals(String.valueOf(lowerBound), tableHeads.get(0).getText());
        assertEquals(String.valueOf(upperBound), tableHeads.get(tableHeads.size() - 1).getText());
    }

    @Then("Verify Loan Amount are shown in the table from {biginteger} to {biginteger} RM")
    public void verifyLoanAmountAreShownInTheTableFromToRM(BigInteger lowerBound, BigInteger upperBound) throws PendingException {
        List<WebElement> tableRows = plProductPage.getLoanAmountTablerow();
        assertNotEquals(0, tableRows.size());

        By loanAmountColLocator = By.xpath(globalManager.display == DisplayType.DESKTOP ?
                "./td[@class = 'repayment-tbody-first ns-repayment-tbody-first']" :
                "./td[1]");
        assertEquals(lowerBound.toString(), tableRows.get(0).findElement(loanAmountColLocator).getText().replaceAll(",", ""));
        assertEquals(upperBound.toString(), tableRows.get(tableRows.size() - 1).findElement(loanAmountColLocator).getText().replaceAll(",", ""));
    }

    @Then("Verify for more related product option are displayed:")
    public void verifyForMoreRelatedProductOptionAreDisplayed(DataTable dataTable) throws PendingException {
        dataTable.asList().forEach(option -> {
            log.info("verify option: " + option);
            switch (option) {
                default:
                    throw new io.cucumber.java.PendingException();
                case "SCB CashOne":
                    assertTrue(plProductPage.getMoreOptionSCB().isDisplayed());
                    break;
                case "JCL Personal Loan":
                    if (globalManager.display == DisplayType.MOBILE) {
                        clickElementByLocatorUsingScript(By.xpath("//a[@class = 'right carousel-control']"));
                        waitUntilAttributeElementContains(plProductPage.getMoreOptionJCLPL(), "class", "active");
                    }
                    assertTrue(plProductPage.getMoreOptionJCLPL().isDisplayed());
                    break;
                case "Hongleong Bank Personal Financing-i":
                    if (globalManager.display == DisplayType.MOBILE) {
                        clickElementByLocatorUsingScript(By.xpath("//a[@class = 'right carousel-control']"));
                        waitUntilAttributeElementContains(plProductPage.getMoreOptionJCLPL(), "class", "active");
                        clickElementByLocatorUsingScript(By.xpath("//a[@class = 'right carousel-control']"));
                        waitUntilAttributeElementTobe(plProductPage.getMoreOptionJCLPL(), "class", "item");
                    }
                    assertTrue(plProductPage.getMoreOptionHongleongBankPFImageView().isDisplayed());
                    assertTrue(plProductPage.getMoreOptionHongleongBankPFLabel().isDisplayed());
                    break;
            }

        });
    }

    @Then("Verify it can navigate properly to the related product page when clicking on it")
    public void verifyItCanNavigateProperlyToTheRelatedProductPageWhenClickingOnIt() throws PendingException {
        assertNotNull(pageData.getCustomData().get(PRODUCT_PAGE_URL));
        assertURL(pageData.getCustomData().get(PRODUCT_PAGE_URL));
    }

    @Then("Verify {string} message is displayed")
    public void verifyMessageIsDisplayed(String message) throws PendingException {
        assertEquals(message, plProductPage.getNewsletterSubscribeValidationText().getText().trim());
    }

    @Then("Verify {string} message displayed after submit")
    public void verifyMessageDisplayedAfterSubmit(String message) throws PendingException {
        waitUntilAttributeElementContains(plProductPage.getNewsletterSubscribeValidationPopup(), "class", "top-green");
        log.info("expected message: " + message.toLowerCase());
        seleniumUtil.waitFor(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                log.info("validation text message: " + plProductPage.getNewsletterSubscribeValidationText().getText());
                if (plProductPage.getNewsletterSubscribeValidationText().getText().trim().isEmpty()) {
                    Actions actions = new Actions(driver);
                    actions.moveToElement(plProductPage.getNewsletterSubscribeValidationText());
                    actions.perform();
                }
                return plProductPage.getNewsletterSubscribeValidationText().getText().trim().toLowerCase().contains(message.toLowerCase());
            }
        });
        assertTrue(plProductPage.getNewsletterSubscribeValidationText().getText().trim().toLowerCase().contains(message.toLowerCase()));
    }

    @Then("Verify the CITI product page augmented version is displayed")
    public void verifyTheCITIProductPageAugmentedVersionIsDisplayed() {
        WebElement headerNavPath = getDriver().findElement(By.xpath(globalManager.display == DisplayType.DESKTOP ?
                "//a[@id='providerNamePL']" :
                "//a[@id='providerNamePLMobile']"));
        assertTrue(headerNavPath.isDisplayed());
        assertEquals("Citibank", headerNavPath.getText());
    }

    @Then("Verify the Personal Loan Product page is displayed with title Personal Loan and CITI logo on the right side page")
    public void verifyThePersonalLoanProductPageIsDisplayedWithTitlePersonalLoanAndCITILogoOnTheRightSidePage() throws PendingException {
        assertTrue(plProductPage.getProductImage().getAttribute("src").contains("citi.png"));
        if (globalManager.display == DisplayType.DESKTOP)
            assertEquals("Personal Loan", plProductPage.mainTitle.getText());
    }

}
