package stepdefinitions.my.navigationbar_redirectionsteps;

import enums.DisplayType;
import stepdefinitions.base.AbstractSteps;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Slf4j
public class NavBarMenuRedirection extends AbstractSteps {

    @When("Hover pointer over {string} menu in the navigation bar")
    public void hoverPointerOverMenuInTheNavigationBar(String navbar) throws PendingException {
        if (globalManager.display.equals(DisplayType.DESKTOP)) {
            Actions actions = new Actions(getDriver());
            actions.moveToElement(getDriver().findElement(By.xpath("//a[.='" + navbar + "']"))).perform();
        } else {
            clickElementByLocator(By.cssSelector(".navbar-toggle"));
            clickElementByLocator(By.xpath("//a[.='"+ navbar +"']"));
        }

    }

    @When("Click on {string} dropdown option")
    public void clickOnDropdownOption(String dropdown) throws PendingException {
        if (globalManager.display.equals(DisplayType.MOBILE)) {
            if (dropdown.equals("Articles")) {
                clickElementByLocator(By.cssSelector(".navbar-toggle"));
                scrollElementToBelowNavbarHeader(getDriver().findElement(By.xpath("//a[@href='#footer12']")));
                clickElementByLocator(By.xpath("//a[@href='#footer12']"));
            }
        }
        clickElementByLocator((globalManager.display.equals(DisplayType.DESKTOP)) ? By.xpath("//ul[@class='nav navbar-nav navbar-custom']//a[contains(.,'"+ dropdown +"')]") : (dropdown.equals("Articles")) ? By.xpath("//div[@class='collapse in']//a[contains(.,'Articles Homepage')]") : By.xpath("//li[@class='col-md-offset-3 col-md-8']//a[contains(.,'"+ dropdown +"')]"));
    }

    @Then("Verify url {string} page is displayed")
    public void verifyUrlPageIsDisplayed(String urlName) throws PendingException {
        waitUntilFinishRedirectToURL(getBaseUrl() + urlName);
        //Verify Page is displayed
        assertEquals(getBaseUrl() + urlName, getDriver().getCurrentUrl());
        log.info("curl: " + getDriver().getCurrentUrl());
    }

    @When("Click on {string} navigation bar menu")
    public void clickOnNavigationBarMenu(String navbar) throws PendingException {
        if (globalManager.display.equals(DisplayType.MOBILE)) clickElementByLocator(By.cssSelector(".navbar-toggle"));
        clickElementByLocator(globalManager.display.equals(DisplayType.DESKTOP) ? By.xpath("//a[.='"+ navbar +"']") : (navbar.equals("TALK TO US")) ? By.cssSelector(".mobile-phone") : By.xpath("//a[.='"+ navbar +"']"));
    }

    @When("Click on {string} article category drop down option")
    public void clickOnArticleCategoryDropDownOption(String dropdown) throws PendingException {
        clickElementByLocator(By.xpath((dropdown.equals("Insurance")) ? "//li[@class='col-md-12']//li[4]/a[contains(.,'"+ dropdown +"')]" : "//li[@class='col-md-12']//a[contains(.,'"+ dropdown +"')]"));
    }

    @When("Verify newsletter subscription page is displayed")
    public void verifyNewsletterSubscriptionPageIsDisplayed() throws PendingException {
        assertEquals(getBaseUrl() + "/", getDriver().getCurrentUrl());
        log.info("curl: " + getDriver().getCurrentUrl());
        scrollElementToMiddle(getDriver().findElement(By.id("newsletter")));
        waitUntilElementVisible(getDriver().findElement(By.id("newsletter")));
    }

    @And("Submit a valid email address {string}")
    public void submitAValidEmailAddress(String email) throws PendingException {
        enterInputWithTextByLocator(email, (globalManager.display.equals(DisplayType.DESKTOP)) ? By.id("subscribe-email-input") : By.id("subscribe-email-input-mobile"));
        clickElementByLocator((globalManager.display.equals(DisplayType.DESKTOP)) ? By.id("mail-button") : By.id("mail-button-mobile"));
    }

    @Then("Newsletter subscription popup successful message is displayed")
    public void newsletterSubscriptionPopupSuccessfulMessageIsDisplayed() throws PendingException, InterruptedException {
        By textLocator = By.id((globalManager.display.equals(DisplayType.DESKTOP)) ? "validation-text" : "validation-text-mobile");
        waitUntilElementVisible(textLocator);
        WebElement validation = getDriver().findElement(textLocator);
        log.info(validation.getText());
        assertEquals(validation.getText(), "You're already subscribed to our newsletter");
    }

    @When("Scroll into More menu in the Comparehero footer")
    public void scrollIntoMoreMenuInTheCompareheroFooter() throws PendingException, InterruptedException {
        scrollIntoElement(getDriver().findElement((globalManager.display.equals(DisplayType.DESKTOP)) ? By.cssSelector("footer") : By.xpath("//a[@href='#footer13']")));
        if (globalManager.display.equals(DisplayType.MOBILE)) {
            By menuLocator = By.xpath("//a[@href='#footer13']");
            clickElementByLocator(menuLocator);
            waitUntilAttributeElementTobe(menuLocator, "aria-expanded", "true");
        }
    }

    @When("Click on {string}")
    public void clickOn(String varFooter) throws PendingException {
        clickElementByLocator(By.xpath((globalManager.display.equals(DisplayType.DESKTOP)) ? "//div[@class='row hidden-xs']//a[contains(.,'"+ varFooter +"')]" : "//div[@class='collapse in']//a[contains(.,'"+ varFooter +"')]"));
//        waitUntilFinishRedirectToURLWithOptionalTrailingSlash(termConditionPageUrl);
    }

    @Then("Verify {string} page is displayed")
    public void verifyPageIsDisplayed(String heading) {
//        assertURLWithOptionalTrailingSlash(termConditionPageUrl);
        waitUntilElementVisible(By.xpath("//strong[contains(.,'"+ heading +"')]"));
    }

    @Then("Verify popup TALK TO US page is displayed")
    public void verifyPopupTALKTOUSPageIsDisplayed() throws PendingException {
        waitUntilElementVisible((globalManager.display.equals(DisplayType.DESKTOP)) ? By.xpath("//ul[@class='dropdown-menu']") : By.id("contact"));
        WebElement contactUs = getDriver().findElement((globalManager.display.equals(DisplayType.DESKTOP)) ? By.xpath("//ul[@class='dropdown-menu']") : By.id("contact"));
        assertTrue(contactUs.isDisplayed());
    }
}
