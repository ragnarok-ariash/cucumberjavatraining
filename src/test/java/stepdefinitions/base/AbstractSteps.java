package stepdefinitions.base;

import enums.BrowserType;
import globalmanagers.GlobalManager;
import io.cucumber.spring.CucumberContextConfiguration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import pageobjects.HomePageView;
import pageobjects.PageData;
import pageobjects.my.PLFormPage;
import utils.SeleniumUtil;
import utils.SpringConfig;

import java.util.Map;

@ContextConfiguration(classes = {SpringConfig.class})
@CucumberContextConfiguration
public class AbstractSteps {

    @Autowired
    protected GlobalManager globalManager;

    @Autowired
    protected SeleniumUtil seleniumUtil;

    @Autowired
    protected PageData pageData;

    protected WebDriver getDriver() {
        return globalManager.driver;
    }

    protected String getBaseUrl() {
        return globalManager.baseUrl;
    }

    protected void navigateInto(String toUrl) {
        getDriver().get(toUrl);
    }

    protected void destroyDriver(boolean quit) {
        if (quit || globalManager.browser == BrowserType.HEADLESS)
            getDriver().quit();
        globalManager.driver = null;
    }

    protected <T> T loadPageComponent(Class<T> tClass) {
        T pageObject = PageFactory.initElements(getDriver(), tClass);
        if (pageObject instanceof HomePageView) {
            ((HomePageView) pageObject).setGlobalManager(globalManager);
            ((HomePageView) pageObject).setSeleniumUtil(seleniumUtil);
        }
        return pageObject;
    }

    protected String getCookieValue(String cookieName) {
        return getDriver().manage().getCookieNamed(cookieName).getValue();
    }

    protected void setProductRedirectUrl(String productRedirectUrl) {
        pageData.productRedirectUrl = productRedirectUrl;
    }

    protected String getProductRedirectUrl() {
        return pageData.productRedirectUrl;
    }

    protected void setFirstUrlSegmentWhenSubmitPI(String firstUrlSegmentWhenSubmitPI) {
        pageData.firstUrlSegmentWhenSubmitPI = firstUrlSegmentWhenSubmitPI;
    }

    protected String getFirstUrlSegmentWhenSubmitPI() {
        return pageData.firstUrlSegmentWhenSubmitPI;
    }

    protected void setProductPageUrl(String productPageUrl) {
        pageData.productPageUrl = productPageUrl;
    }

    protected String getProductPageUrl() {
        return pageData.productPageUrl;
    }

    protected void setPartnerDataId(String partnerDataId) {
        pageData.partnerDataId = partnerDataId;
    }

    protected String getPartnerDataId() {
        return pageData.partnerDataId;
    }

    protected void setNumberOfWindowWhenSubmitPI(int numberOfWindowWhenSubmitPI) {
        pageData.numberOfWindowWhenSubmitPI = numberOfWindowWhenSubmitPI;
    }

    protected int getNumberOfWindowWhenSubmitPI() {
        return pageData.numberOfWindowWhenSubmitPI;
    }

    protected void waitUntilFinishRedirectToURL(String url) {
        seleniumUtil.waitUntilFinishRedirectToURL(url);
    }

    protected void waitUntilFinishRedirectToURLWithOptionalTrailingSlash(String url) {
        seleniumUtil.waitUntilFinishRedirectToURLWithOptionalTrailingSlash(url);
    }

    protected void waitUntilFinishRedirectToURLContains(String url) {
        seleniumUtil.waitUntilFinishRedirectToURLContains(url);
    }

    protected void waitUntilElementVisible(WebElement element) {
        seleniumUtil.waitUntilElementVisible(element);
    }

    protected void waitUntilElementVisible(By locator) {
        seleniumUtil.waitUntilElementVisible(locator);
    }

    protected void waitUntilElementClickable(WebElement element) {
        seleniumUtil.waitUntilElementClickable(element);
    }

    protected void waitUntilElementClickable(By locator) {
        seleniumUtil.waitUntilElementClickable(locator);
    }

    protected void waitUntilElementGone(WebElement element) {
        seleniumUtil.waitUntilElementGone(element);
    }

    protected void waitUntilElementGone(By locator) {
        seleniumUtil.waitUntilElementGone(locator);
    }

    protected void waitUntilAttributeElementTobe(WebElement element, String attribute, String valueToBe) {
        seleniumUtil.waitUntilAttributeElementTobe(element, attribute, valueToBe);
    }

    protected void waitUntilAttributeElementTobe(By locator, String attribute, String valueToBe) {
        seleniumUtil.waitUntilAttributeElementTobe(locator, attribute, valueToBe);
    }

    protected void waitUntilAttributeElementContains(WebElement element, String attribute, String valueToContain) {
        seleniumUtil.waitUntilAttributeElementContains(element, attribute, valueToContain);
    }

    protected void waitUntilAttributeElementContains(By locator, String attribute, String valueToContain) {
        seleniumUtil.waitUntilAttributeElementContains(locator, attribute, valueToContain);
    }

    protected void waitUntilChildElementVisible(WebElement parentElement, By childLocator) {
        seleniumUtil.waitUntilChildElementVisible(parentElement, childLocator);
    }

    protected void waitUntilTextPresentInElement(WebElement element, String expectedTextToPresent) {
        seleniumUtil.waitUntilTextPresentInElement(element, expectedTextToPresent);
    }

    protected void waitForUrlAndGivenCondition(String pageSegment, ExpectedCondition<Boolean> waitCondition) {
        seleniumUtil.waitFor(ExpectedConditions.and(
                ExpectedConditions.urlToBe(getBaseUrl() + pageSegment),
                waitCondition));
    }

    protected void verifyPageUrl(String pageSegment, SeleniumUtil.VerifyMethod verifyMethod) {
        seleniumUtil.verifyPageUrl(getBaseUrl() + pageSegment, verifyMethod);
    }

    protected void clickElement(WebElement element) {
        seleniumUtil.clickElement(element);
    }

    protected void clickElementByLocator(By locator) {
        seleniumUtil.clickElementByLocator(locator);
    }

    protected void clickElementByLocatorUsingScript(By locator) {
        seleniumUtil.clickElementByLocatorUsingScript(locator);
    }

    protected void clickElementUsingScript(WebElement element) {
        seleniumUtil.clickElementUsingScript(element);
    }

    protected void checkCheckBoxElement(By cbLocator, boolean check) {
        seleniumUtil.checkCheckBoxElement(cbLocator, check);
    }

    protected void enterInputWithTextByLocator(String text, By locator) {
        seleniumUtil.enterInputWithTextByLocator(text, locator, false, false);
    }

    protected void enterInputWithTextByElement(String text, WebElement element) {
        seleniumUtil.enterInputWithTextByElement(text, element, false, false);
    }

    protected void enterInputWithTextUsingJavaScriptByLocator(String text, By locator) {
        seleniumUtil.enterInputWithTextByLocator(text, locator, true, false);
    }

    protected void enterInputWithTextUsingJavaScriptByElement(String text, WebElement element) {
        seleniumUtil.enterInputWithTextByElement(text, element, true, false);
    }

    protected void selectDropdownWithValueByLocator(String text, By locator, int numOfElementMoreThan) {
        seleniumUtil.selectDropdownWithValueByLocator(text, locator, numOfElementMoreThan);
    }

    protected void selectDropdownWithValueByLocator(String text, By locator) {
        seleniumUtil.selectDropdownWithValueByLocator(text, locator, 1);
    }

    protected void selectDropdownWithValueByElement(String text, WebElement element, int numOfElementMoreThan) {
        seleniumUtil.selectDropdownWithValueByElement(text, element, numOfElementMoreThan);
    }

    protected void selectDropdownWithValueByElement(String text, WebElement element) {
        seleniumUtil.selectDropdownWithValueByElement(text, element, 1);
    }

    protected void scrollIntoElement(WebElement element) {
        seleniumUtil.scrollIntoElement(element);
    }

    protected void scrollElementToMiddle(WebElement element) {
        seleniumUtil.scrollElementToMiddle(element);
    }

    protected void scrollElementToBelowNavbarHeader(WebElement element) {
        seleniumUtil.scrollElementToBelowElement(element, getMobileNavbarHeader());
    }

    protected WebElement getMobileNavbarHeader() {
        return getDriver().findElement(By.className("navbar-header"));
    }

    protected void waitUntilElementScrolledToNavbarGetSpecificYOffset(WebElement element, int yOffset) {
        seleniumUtil.waitUntilElementsGetCloseWithSpecificYOffset(getMobileNavbarHeader(), element, yOffset);
    }

    protected void assertURL(String url) {
        seleniumUtil.assertURL(url);
    }

    protected void assertURLWithRegex(String regex) {
        seleniumUtil.assertURLWithRegex(regex);
    }

    protected void assertURLWithOptionalTrailingSlash(String url) {
        seleniumUtil.assertURLWithOptionalTrailingSlash(url);
    }

    protected boolean elementExist(By locator) {
        return seleniumUtil.elementExist(locator);
    }

    protected void verifyElementContent(Map<By, String> elementContentMap) {
        seleniumUtil.verifyElementContent(elementContentMap);
    }
}
