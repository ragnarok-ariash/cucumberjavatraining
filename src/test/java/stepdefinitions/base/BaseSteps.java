package stepdefinitions.base;

import enums.CountryType;
import enums.DisplayType;
import globalmanagers.GlobalManager;
import globalmanagers.URLSlugs;
import globalmanagers.WebDriverFactory;
import io.cucumber.java.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;
import pageobjects.HomePageView;
import pageobjects.MYHomePage;
import pageobjects.ph.PHHomePage;
import pageobjects.THHomePage;
import utils.SeleniumUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static stepdefinitions.base.BaseSteps.CTAType.CI_CTA;

@Slf4j
public class BaseSteps extends AbstractSteps {
    private PHHomePage phHomePage;
    private MYHomePage myHomePage;
    private THHomePage thHomePage;

    @Autowired
    public BaseSteps(GlobalManager globalManager, SeleniumUtil seleniumUtil) {
        this.globalManager = globalManager;
        this.seleniumUtil = seleniumUtil;
    }

    public enum CTAType {
        NONE,
        CI_CTA,
        CC_CTA,
        PL_CTA,
        GDP_CTA,
        TI_CTA,
        Partner_CTA
    }

    @ParameterType("(PL|CI|CC|TI|GDP|Partner)\\sCTA")
    public CTAType ctaBtn(String cta) {
        String[] ctas = cta.split("\\s");
        if (ctas.length > 0) {
            switch (ctas[0]) {
                case "PL":
                    return CTAType.PL_CTA;
                case "GDP":
                    return CTAType.GDP_CTA;
                case "CI":
                    return CI_CTA;
                case "CC":
                    return CTAType.CC_CTA;
                case "TI":
                    return CTAType.TI_CTA;
                case "Partner":
                    return CTAType.Partner_CTA;
            }
        }
        return CTAType.NONE;
    }

    @Before
    public void createDriver() {
        globalManager.driver = new WebDriverFactory().createDriver(globalManager.browser, globalManager.display);
        pageData.init();
        if (globalManager.display == DisplayType.DESKTOP)
            getDriver().manage().window().maximize();
    }

    @After
    public void destroyDriver(Scenario scenario) {
        destroyDriver(!scenario.isFailed());
    }

    @Given("Open landing page")
    public void openThePage() throws PendingException{
        navigateInto(globalManager.baseUrl);

        if (globalManager.country == CountryType.PH) {
            phHomePage = loadPageComponent(PHHomePage.class);
            handleSurveyPopup();
            handleCookieAlert();

        } else if (globalManager.country == CountryType.TH) {
            thHomePage = loadPageComponent(THHomePage.class);
            handleOneSignalPopup();

        } else if (globalManager.country == CountryType.MY)
            myHomePage = loadPageComponent(MYHomePage.class);
    }

    @When("Click on {ctaBtn} button")
    public void clickOnCTAButton(CTAType ctaType) {
        final Map<CTAType, String> ctaToSegmentMap = Map.ofEntries(
                Map.entry(CTAType.CI_CTA, URLSlugs.CI),
                Map.entry(CTAType.CC_CTA, URLSlugs.CC),
                Map.entry(CTAType.PL_CTA, URLSlugs.PL),
                Map.entry(CTAType.TI_CTA, URLSlugs.TI),
                Map.entry(CTAType.GDP_CTA, URLSlugs.GDP),
                Map.entry(CTAType.Partner_CTA, URLSlugs.PARTNER)
        );

        HomePageView homePageView;
        switch (globalManager.country) {
            case PH:
                homePageView = phHomePage;
                break;
            case MY:
                handleScammerWarningPopup();
                homePageView = myHomePage;
                break;
            case TH:
                homePageView = thHomePage;
                break;
            default:
                throw new IllegalStateException("Unexpected value for country: " + globalManager.country);
        }

        homePageView.clickOnCtaBtn(ctaType, getDriver());
        waitUntilFinishRedirectToURL(getBaseUrl() + ctaToSegmentMap.get(ctaType));
    }

    @Then("Close browser")
    public void closeBrowser() throws PendingException {
        List<String> tabHandles = new ArrayList<>(getDriver().getWindowHandles());
        destroyDriver(tabHandles.size() > 1);
    }

    private void handleSurveyPopup() {
        //try to close survey popup if exist
        try {
            By surveyContainerLocator = By.xpath("//div[contains(@class,'surveyContainer')]");
            waitUntilElementVisible(surveyContainerLocator);
            clickElementByLocatorUsingScript(By.xpath("//div[contains(@class,'surveyContainer')]//button[contains(@class,'openStateToggle')]"));
            seleniumUtil.waitFor(ExpectedConditions.attributeContains(surveyContainerLocator, "class", "minimized"));
        } catch (NoSuchElementException | TimeoutException ignored) {

        }
    }

    private void handleCookieAlert() {
        //try to close cookie policy (if exist) on mobile view since it could obscure element to be clicked thus lead to click interception
        if (globalManager.display == DisplayType.MOBILE) {
            try {
                waitUntilElementVisible(phHomePage.cookiePolicyWrapper);
                clickElement(phHomePage.cookieAcceptBtn);
                waitUntilElementGone(phHomePage.cookiePolicyWrapper);
            } catch (NoSuchElementException | TimeoutException ignored) {

            }
        }
    }

    private void handleOneSignalPopup() {
        try {
            waitUntilElementVisible(thHomePage.oneSignalPopup);
            if (elementExist(By.id("onesignal-popover-cancel-button"))) {
                thHomePage.oneSignalPopup.click();
            }
        } catch (NoSuchElementException | TimeoutException ignored) {

        }
    }

    private void handleScammerWarningPopup() {
        List<WebElement> popupScammer = getDriver().findElements(By.className("popup-overlay"));
        if (popupScammer.size() != 0) {
            if (myHomePage.isScammerWarningPopupDisplayed()) {
                myHomePage.closeScammerPopup();
                waitUntilElementGone(By.className("popup-overlay"));
            }
        }
    }

}
