package enums;

public enum CountryType {
    UNKNOWN,
    PH,
    MY,
    TH
}
