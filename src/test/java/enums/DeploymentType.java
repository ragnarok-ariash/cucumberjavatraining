package enums;

public enum DeploymentType {
    DEV,
    STAGING,
    PROD
}
